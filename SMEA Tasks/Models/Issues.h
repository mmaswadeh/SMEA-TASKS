//
//  Issues.h
//  PMO Dashboard
//
//  Created by Mohammad Maswadeh on 2/8/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseObject.h"
#import "High.h"
#import "Medium.h"
#import "Low.h"
#import "HMLModel.h"


/*typedef enum : NSUInteger {
    IssuesHigh = 1,
    IssuesMedium = 2,
    IssuesLow = 3
} IssuesStatus;

*/
@interface Issues :HMLModel

/*@property (nonatomic, strong) High *high;
@property (nonatomic, strong) Medium *medium;
@property (nonatomic, strong) Low *low;

@property (nonatomic, assign) NSInteger totalCount;

@property (nonatomic, strong) NSString *issueTitle;
@property (nonatomic, strong) NSString *assignTo;

@property (nonatomic, assign) NSInteger idProjects;

@property (nonatomic, assign) IssuesStatus statusNumber;

@property (nonatomic, strong) UIColor *issuesColor;

*/
@end
