//
//  Projects.m
//  PMO Dashboard
//
//  Created by Mohammad Maswadeh on 2/8/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import "Projects.h"

@implementation Projects

-(instancetype)initWithAttributes:(NSDictionary *)attributes
{
    if (self = [super initWithAttributes:attributes])
        
    {
        
        self.entityName=[self extractStringValue:attributes[@"EntityName"]];
         self.entityID = [self extractIntegerValue:attributes[@"EntityID"]];
         self.feelingId=[self extractIntegerValue:attributes[@"feelingId"]];
        self.PhaseStatus = [self extractStringValue:attributes[@"PhaseStatus"]];
        self.totalCount = [self extractIntegerValue:attributes[@"TotalCount"]];
        self.statusNumber = [self extractIntegerValue:attributes[@"StatusNumber"]];
       self.InstanceStatusNumber = [self extractIntegerValue:attributes[@"StatusNumber"]];
        self.phaseColor=[self statusColor:self.statusNumber];

        self.projectID = [self extractIntegerValue:attributes[@"ID"]];
        self.issuesCount = [self extractIntegerValue:attributes[@"IssuesCount"]];
        self.statusNumber = [self extractIntegerValue:attributes[@"StatusNumber"]];
        
        self.completedPercent = [self extractDoubleValue:attributes[@"CompletePercent"]];
        
        self.projectTitle = [self extractStringValue:attributes[@"title"]];
        self.projectPriority = [self extractStringValue:attributes[@"ProjectPriority"]];
        self.startDate = [self extractStringValue:attributes[@"startDate"]];
        self.finishDate = [self extractStringValue:attributes[@"endDate"]];
        
        self.phaseColor=[self statusColor:self.statusNumber];
        self.InstancephaseColor=[self statusInstanceColor:self.InstanceStatusNumber];
        
        /////
        
    
        self.projectManager = [self extractStringValue:attributes[@"ProjectManager"]];
        self.summary = [self extractStringValue:attributes[@"Summary"]];
        self.ownerPosition =[self extractStringValue:attributes[@"BusinessOwner"]];
        self.projectName = [self extractStringValue:attributes[@"ProjectName"]];
        self.assignto =[self extractStringValue:attributes[@"Assignto"]];
        self.jobPosition=[self extractStringValue:attributes[@"JopPosition"]];
        self.name=[self extractStringValue:attributes[@"Name"]];
        self.month=[self extractIntegerValue:attributes[@"Month"]];

        
        self.projectImage=[self extractUrlValue:attributes[@"Image"]];
        self.baseline=[self extractDoubleValue:attributes[@"BaseLine"]];
        self.actual=[self extractDoubleValue:attributes[@"Actual"] ];
        

    }
    
    return self;
}


- (UIColor*)statusColor:(ProgressStatus)status {
   return [[ColorHelper sharedManager] getColorWithStatus:status];}

- (UIColor*)statusInstanceColor:(InstancesStatus)status {
    return [[ColorHelper sharedManager] getColorOfInstancesWithStatus:status];}

@end
