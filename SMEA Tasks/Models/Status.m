//
//  Status.m
//  PMO Dashboard
//
//  Created by Mohammad Maswadeh on 2/8/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import "Status.h"

@implementation Status


-(instancetype)initWithAttributes:(NSDictionary *)attributes
{
    if (self = [super initWithAttributes:attributes])
        
    {
        self.statusNumber  = [self extractIntegerValue:attributes[@"StatusNumber"]];
        self.projectNumber = [self extractIntegerValue:attributes[@"projectNumber"]];
        
        self.percentage    = [self extractDoubleValue:attributes[@"projectNumber"]];
//        self.percentage    = [self extractDoubleValue:attributes[@"Percentage"]];
        self.projectBudget = [self extractDoubleValue:attributes[@"ProjectBudget"]];
        self.projectStatus = [self extractStringValue:attributes[@"ProjectStatus"]];
        self.projectColor=[self statusColor:self.statusNumber];
    }
    
    return self;
    
}


- (UIColor*)statusColor:(ProgressStatus)status {
  return [[ColorHelper sharedManager] getColorWithStatus:status];}

@end
