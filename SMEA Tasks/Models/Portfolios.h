//
//  Portfolios.h
//  PMO Dashboard
//
//  Created by Mohammad Maswadeh on 2/8/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseObject.h"
#import "Status.h"
#import "ColorHelper.h"





@interface Portfolios : BaseObject
@property (nonatomic, strong) ColorHelper *colorSettings;
@property (nonatomic, strong) NSArray *status;

@property (nonatomic, strong) NSString *propertyValue;
@property (nonatomic, strong) NSString *portfolioName;

@property (nonatomic, strong) NSString *portfoiloOwner;
@property (nonatomic, strong) NSString *statusClass;
@property (nonatomic, strong) NSString *lastUpdateDate;
@property (nonatomic, strong) NSString *token;

@property (nonatomic, assign) BOOL isSelected;
@property (nonatomic, assign) BOOL isSuccess;


@property (nonatomic, assign) NSInteger listPortfolioId;
@property (nonatomic, assign) NSInteger portfolioID;
@property (nonatomic, assign) NSInteger totalNumber;
@property (nonatomic, assign) NSInteger overAllStatus;
@property (nonatomic, assign) NSInteger projectNumber;
@property (nonatomic, assign) NSInteger commentType;
@property (nonatomic, assign) NSInteger relatedType;
@property (nonatomic, assign) NSInteger language;
@property (nonatomic, assign) NSInteger totalCounts;

@property (nonatomic, assign) ProgressStatus statusNumber;
@property (nonatomic, strong) UIColor *portfolioColor;

@property (nonatomic, assign) double portfolioBudget;

-(NSMutableArray *)extractStatus:(NSArray *)statusDictsArray;

@end




