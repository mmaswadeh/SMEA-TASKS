//
//  Portfolios.m
//  PMO Dashboard
//
//  Created by Mohammad Maswadeh on 2/8/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import "Portfolios.h"

@implementation Portfolios
@synthesize colorSettings;


-(instancetype)initWithAttributes:(NSDictionary *)attributes
{
    if (self = [super initWithAttributes:attributes]) {
        
        self.propertyValue = [self extractStringValue:attributes[@"PropertyValue"]];
        self.portfolioName = [self extractStringValue:attributes[@"PortfolioName"]];
        self.portfoiloOwner = [self extractStringValue:attributes[@"PortfolioOwner"]];
        self.statusClass = [self extractStringValue:attributes[@"StatusClass"]];
        self.lastUpdateDate = [self extractStringValue:attributes[@"LastUpdateDate"]];
        self.token = [self extractStringValue:attributes[@"Token"]];
        
        self.statusNumber  = [self extractIntegerValue:attributes[@"StatusNumber"]];
        self.portfolioID = [self extractIntegerValue:attributes[@"ID"]];
        self.listPortfolioId = [self extractIntegerValue:attributes[@"PortfolioID"]];
        self.totalNumber = [self extractIntegerValue:attributes[@"TotalNumber"]];
        self.overAllStatus = [self extractIntegerValue:attributes[@"OverAllStatus"]];
        self.projectNumber = [self extractIntegerValue:attributes[@"ProjectNumber"]];
        self.commentType = [self extractIntegerValue:attributes[@"CommentTypes"]];
        self.relatedType = [self extractIntegerValue:attributes[@"RelatedType"]];
        self.language = [self extractIntegerValue:attributes[@"Language"]];
        self.totalCounts = [self extractIntegerValue:attributes[@"TotalCounts"]];

        self.portfolioBudget = [self extractDoubleValue:attributes[@"PortfolioBudget"]];

        self.isSuccess = [self extractBooleanValue:attributes[@"IsSuccess"]];
        self.isSelected = [self extractBooleanValue:attributes[@"selected"]];
        
        self.portfolioColor=[self statusColor:self.statusNumber];

        
        // Status
        if (attributes[@"Status"] && attributes[@"Status"] != NSNull.null)
            self.status = [self extractStatus:attributes[@"Status"]];

        

    }
    
    return self;
    
}

- (UIColor*)statusColor:(ProgressStatus)status {
    
    return [[ColorHelper sharedManager] getColorWithStatus:status];
    
}


-(NSMutableArray *)extractStatus:(NSArray *)statusDictsArray {
    NSMutableArray *arrStatus = [[NSMutableArray alloc] init];
    
    for (NSDictionary *dictStatus in statusDictsArray) {
        Status *status = [[Status alloc] initWithAttributes:dictStatus];
        [arrStatus addObject:status];
    }
    
    return arrStatus;
}






@end
