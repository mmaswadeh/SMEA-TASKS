//
//  chartData.m
//  VRO-BTIT
//
//  Created by Blessed Tree IT on 8/17/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import "chartData.h"
#import "AVHexColor.h"
@implementation chartData


-(instancetype)initWithAttributes:(NSDictionary *)attributes
{
    if (self = [super initWithAttributes:attributes])
        
    {
    
        self.count = [self extractIntegerValue:attributes[@"Count"]];
       
        self.statusName=[self extractStringValue:attributes[@"statusName"]];
           self.statusColor=[self extractStringValue:attributes[@"statusColor"]];
        self.status = [self extractIntegerValue:attributes[@"status"]];
       
        self.phaseColor=[self statusInstanceColor:self.statusColor];
        self.shadowColor=[self statusInstanceColor:@"#FFFF0000"];

    }
    
    return self;
    
}
- (UIColor*)statusInstanceColor:(NSString *)status {
    
 
    return   [AVHexColor colorWithHexString:status];
   // return color;
}


@end
