//
//  chartData.h
//  VRO-BTIT
//
//  Created by Blessed Tree IT on 8/17/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseObject.h"
#import "ColorHelper.h"


@interface chartData :BaseObject
@property (nonatomic, strong) NSString * statusColor;
@property (nonatomic, strong) NSString * statusName;
@property (nonatomic, assign) NSInteger count;
@property (nonatomic, assign) NSInteger status;
@property (nonatomic, strong) UIColor *phaseColor;
@property (nonatomic, strong) UIColor *shadowColor;


@end
