//
//  Status.h
//  PMO Dashboard
//
//  Created by Mohammad Maswadeh on 2/8/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "AVHexColor.h"
#import "BaseObject.h"


#import "ColorHelper.h"



@interface Status : BaseObject

@property (nonatomic, assign) NSInteger projectNumber;

@property (nonatomic, assign) ProgressStatus statusNumber;

@property (nonatomic, assign) double projectBudget;
@property (nonatomic, assign) double percentage;

@property (nonatomic, strong) NSString *projectStatus;

@property (nonatomic, strong) UIColor *projectColor;


@end
