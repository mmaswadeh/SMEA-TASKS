//
//  HMLModel.m
//  MCI
//
//  Created by Blessed Tree IT on 5/22/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import "HMLModel.h"
#import "chartData.h"

@implementation HMLModel

-(instancetype)initWithAttributes:(NSDictionary *)attributes
{
    if (self = [super initWithAttributes:attributes])
    {
        
        self.totalCount = [self extractIntegerValue:attributes[@"TotalCount"]];
        
        self.hmlTitle = [self extractStringValue:attributes[@"Title"]];
        self.assignTo = [self extractStringValue:attributes[@"Assignto"]];
        self.hmlOwner = [self extractStringValue:attributes[@"Owner"]];
        self.statusNumber = [self extractIntegerValue:attributes[@"StatusNumber"]];
        
       // self.totalCount = [self extractIntegerValue:attributes[@"TotalCount"]];
        
        self.hmlColor=[self statusColor:self.statusNumber];
        
        self.hmlID=[self extractIntegerValue:attributes[@"ID"]];
        // Extract High
        
        self.high =[[High alloc]initWithAttributes:attributes[@"High"]];
        
        // Extract Medium
        
        self.medium =[[Medium alloc]initWithAttributes:attributes[@"Medium"]];
        
        
        // Extract Low
        
        self.low =[[Low alloc]initWithAttributes:attributes[@"Low"]];
        
        self.name=[self extractStringValue:attributes[@"name"]];
        
        self.total = [self extractIntegerValue:attributes[@"total"]];
        
        self.icon=[self extractStringValue:attributes[@"icon"]];
        
        
        if (attributes[@"chartData"] && attributes[@"chartData"] != NSNull.null)
            self.chartData = [self extractProjects:attributes[@"chartData"]];
        
        
    }
    
    return self;
    
    
}

-(NSMutableArray *)extractProjects:(NSArray *)projectsDictsArray {
    NSMutableArray *arrProjects = [[NSMutableArray alloc] init];
    
    for (NSDictionary *dictProjects in projectsDictsArray) {
        chartData  *projects = [[chartData alloc] initWithAttributes:dictProjects];
        [arrProjects addObject:projects];
    }
    
    return arrProjects;

}

- (UIColor*)statusColor:(HMLStatus)status {
   return [[ColorHelper sharedManager] getColorWithHMLStatus:status];
}

-(NSMutableArray *)extractIssuesHigh:(NSArray *)highDictsArray {
    NSMutableArray *arrHigh = [[NSMutableArray alloc] init];
    
    for (NSDictionary *dictHigh in highDictsArray) {
        High *high = [[High alloc] initWithAttributes:dictHigh];
        [arrHigh addObject:high];
    }
    
    return arrHigh;
}

-(NSMutableArray *)extractIssuesMedium:(NSArray *)mediumDictsArray {
    NSMutableArray *arrMedium = [[NSMutableArray alloc] init];
    
    for (NSDictionary *dictMedium in mediumDictsArray) {
        Medium *medium = [[Medium alloc] initWithAttributes:dictMedium];
        [arrMedium addObject:medium];
    }
    
    return arrMedium;
}

-(NSMutableArray *)extractIssuesLow:(NSArray *)lowDictsArray {
    NSMutableArray *arrLow = [[NSMutableArray alloc] init];
    
    for (NSDictionary *dictLow in lowDictsArray) {
        Low *low = [[Low alloc] initWithAttributes:dictLow];
        [arrLow addObject:low];
    }
    
    return arrLow;
}


@end
