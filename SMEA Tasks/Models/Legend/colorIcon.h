//
//  colorIcon.h
//  VRO-BTIT
//
//  Created by Blessed Tree IT on 8/20/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface colorIcon : UIView
@property (weak, nonatomic) IBOutlet UIView *colorBox;
@property (weak, nonatomic) IBOutlet UILabel *legnedTitle;

@property (weak, nonatomic) IBOutlet UILabel * countNo;
+ (id)customView;
@end
