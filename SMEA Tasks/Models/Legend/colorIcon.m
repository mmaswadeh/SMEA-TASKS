//
//  colorIcon.m
//  VRO-BTIT
//
//  Created by Blessed Tree IT on 8/20/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import "colorIcon.h"

@implementation colorIcon

+ (id)customView
{
    colorIcon *customView = [[[NSBundle mainBundle] loadNibNamed:@"colorIcon" owner:nil options:nil] lastObject];
    
    // make sure customView is not nil or the wrong class!
    if ([customView isKindOfClass:[colorIcon class]])
    {
        
        
        return customView;
        
        
    }
    else
        return nil;
}



@end
