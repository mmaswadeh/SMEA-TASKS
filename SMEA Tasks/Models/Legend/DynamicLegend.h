//
//  DynamicLegend.h
//  VRO-BTIT
//
//  Created by Blessed Tree IT on 8/20/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "colorIcon.h"

@interface DynamicLegend : UIView
@property (weak, nonatomic) IBOutlet UIView *viewLegned;

@property(strong,nonatomic)NSMutableArray * listOfStacks;
@property(strong,nonatomic) UIStackView *mainStackView;
+ (id)customView;
-(void)createViewWithArrayOfColorsAndNamesOfLegend:(NSArray *)arrayOfColorsAndNames;
@end
