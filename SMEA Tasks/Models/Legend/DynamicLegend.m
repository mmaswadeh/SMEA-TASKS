//
//  DynamicLegend.m
//  VRO-BTIT
//
//  Created by Blessed Tree IT on 8/20/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import "DynamicLegend.h"
#import "colorIcon.h"
#import "legendHelper.h"
#import "Dashboard.h"

@interface DynamicLegend(){

    
    
}
@end
@implementation DynamicLegend
@synthesize listOfStacks,mainStackView;
+ (id)customView
{
    DynamicLegend *customView = [[[NSBundle mainBundle] loadNibNamed:@"DynamicLegend" owner:nil options:nil] lastObject];

    // make sure customView is not nil or the wrong class!
    if ([customView isKindOfClass:[DynamicLegend class]]) {

        CGFloat screenwidth = [UIScreen mainScreen].bounds.size.width;
        customView.listOfStacks=[NSMutableArray new];
        customView.mainStackView = [[UIStackView alloc] init];
        
        return customView;
    }
    else
        return nil;
}

-(void)createViewWithArrayOfColorsAndNamesOfLegend:(NSArray *)arrayOfColorsAndNames{

    if (arrayOfColorsAndNames.count>4) {
        
        NSMutableArray *arrayOfArrays = [NSMutableArray array];
        
        NSUInteger  itemsRemaining = [arrayOfColorsAndNames count];
        int j = 0;
        
        while(itemsRemaining) {
            NSRange range = NSMakeRange(j, MIN(4, itemsRemaining));
            NSArray *subarray = [arrayOfColorsAndNames subarrayWithRange:range];
            [arrayOfArrays addObject:subarray];
            [self drawMultiLegendWithArray:subarray];
            itemsRemaining-=range.length;
            
            j+=range.length;
            
        }
       [self drawMultiStackViews:listOfStacks];

    }
    else {
    
    
        [self drawLegendWithArray:arrayOfColorsAndNames];
    
    }
}


-(void)drawLegendWithArray:(NSArray *)items{
    
        UIStackView *stackView = [[UIStackView alloc] init];
        
        stackView.axis = UILayoutConstraintAxisHorizontal;
        stackView.distribution = UIStackViewDistributionEqualSpacing;
        stackView.alignment = UIStackViewAlignmentCenter;
        stackView.spacing =10;
        stackView.backgroundColor = [UIColor blueColor];
    
        for (legendHelper * data in items) {
            colorIcon * view1 =[colorIcon customView];
            view1.colorBox.backgroundColor=data.color;
            view1.legnedTitle.text=data.textDescription;
            if ([data.countDataStr isEqualToString:@""]) {
                view1.countNo.text=[NSString stringWithFormat:@"%@",data.countDataStr];
            }
            else{
         view1.countNo.text=[NSString stringWithFormat:@"(%@)",data.countDataStr];
            }
            [stackView addArrangedSubview:view1];
        }
        
      stackView.translatesAutoresizingMaskIntoConstraints = false;
    
        [self addSubview:stackView];
                [stackView.centerXAnchor constraintEqualToAnchor:self.centerXAnchor].active = true;
        [stackView.centerYAnchor constraintEqualToAnchor:self.centerYAnchor].active = true;
    
}


-(void)drawMultiLegendWithArray:(NSArray *)items{

    UIStackView *stackView = [[UIStackView alloc] init];
    
    stackView.axis = UILayoutConstraintAxisHorizontal;
    stackView.distribution = UIStackViewDistributionEqualSpacing;
    stackView.alignment = UIStackViewAlignmentCenter;
    stackView.spacing = 10;
    
    for (legendHelper * data in items) {
        colorIcon * view1 =[colorIcon customView];
        view1.colorBox.backgroundColor=data.color;
        view1.legnedTitle.text=data.textDescription;
//        if ([data.countDataStr isEqualToString:@""]) {
//          //  view1.countNo.text=[NSString stringWithFormat:@"%@",data.countDataStr];
//        }
//        else{
//          //  view1.countNo.text=[NSString stringWithFormat:@"(%@)",data.countDataStr];
//        }
        [stackView addArrangedSubview:view1];
    }
    
    stackView.translatesAutoresizingMaskIntoConstraints = false;
    
    [listOfStacks addObject:stackView];

}
-(void)drawMultiStackViews:(NSArray *)list{

    mainStackView.axis = UILayoutConstraintAxisVertical;
    mainStackView.distribution = UIStackViewDistributionEqualSpacing;
    mainStackView.alignment = UIStackViewAlignmentCenter;
    mainStackView.spacing =30;
    mainStackView.backgroundColor=[UIColor yellowColor];
    
    
    
    for (UIStackView *views in list) {
         [mainStackView addArrangedSubview:views];
    }

    mainStackView.translatesAutoresizingMaskIntoConstraints = false;
    
    
    [self addSubview:mainStackView];
    
    
    //Layout for Stack View
    [mainStackView.centerXAnchor constraintEqualToAnchor:self.centerXAnchor].active = true;
    [mainStackView.centerYAnchor constraintEqualToAnchor:self.centerYAnchor].active = true;

}

@end
