//
//  legendHelper.h
//  VRO-BTIT
//
//  Created by Blessed Tree IT on 8/27/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface legendHelper : NSObject

+ (instancetype)dataItemWithColor:(UIColor*)color
                      description:(NSString *)description count:(NSString *)countNo  ;

+ (instancetype)setDataItemWithColor:(UIColor*)color
                      description:(NSString *)description count:(NSString *)countNo  ;

@property (nonatomic) UIColor  *color;
@property (nonatomic) NSString *textDescription;
@property (nonatomic) NSInteger countData;
@property (nonatomic) NSString *countDataStr;
@end
