//
//  legendHelper.m
//  VRO-BTIT
//
//  Created by Blessed Tree IT on 8/27/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import "legendHelper.h"

@implementation legendHelper


+ (instancetype)dataItemWithColor:(UIColor*)color
                      description:(NSString *)description count:(NSString *)countNo {

    legendHelper *item = [legendHelper new];
    item.textDescription = description;
    item.color  = color;
    item.countDataStr=countNo;
    return item;

  
}

+ (instancetype)setDataItemWithColor:(UIColor*)color
                         description:(NSString *)description count:(NSString *)countNo {
    legendHelper *item = [legendHelper dataItemWithColor:color description:description count:(NSString *)countNo ];
    item.textDescription = description;
    item.countDataStr=countNo;
    return item;
}


@end
