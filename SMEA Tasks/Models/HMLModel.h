//
//  HMLModel.h
//  MCI
//
//  Created by Blessed Tree IT on 5/22/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseObject.h"
#import "High.h"
#import "Medium.h"
#import "Low.h"
#import "ColorHelper.h"


@interface HMLModel : BaseObject

@property (nonatomic, strong) High *high;
@property (nonatomic, strong) Medium *medium;
@property (nonatomic, strong) Low *low;

@property (nonatomic, assign) NSInteger totalCount;
@property (nonatomic, assign) NSInteger hmlID;
@property (nonatomic, strong) NSString *hmlTitle;
@property (nonatomic, strong) NSString *assignTo;

@property (nonatomic, strong) NSString *hmlOwner;


@property (nonatomic, assign) HMLStatus statusNumber;

@property (nonatomic, strong) UIColor *hmlColor;

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *icon;
@property (nonatomic, strong) NSArray  *chartData;
@property (nonatomic, assign) NSInteger total;




@end
