//
//  High.h
//  PMO Dashboard
//
//  Created by Mohammad Maswadeh on 2/8/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "BaseObject.h"
#import "AVHexColor.h"
#import "ColorHelper.h"


@interface High : BaseObject

@property (nonatomic, assign) NSInteger count;
@property (nonatomic, assign) double percentage;

@property (nonatomic, assign) HMLStatus statusNumber;

@property (nonatomic, strong) UIColor *highColor;



@end
