//
//  PMOCircleChart.m
//  VRO-BTIT
//
//  Created by Blessed Tree IT on 7/27/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import "PMOCircleChart.h"

@implementation PMOCircleChart

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (id)initWithFrame:(CGRect)frame
              total:(NSNumber *)total
            current:(NSNumber *)current
          clockwise:(BOOL)clockwise
             shadow:(BOOL)hasBackgroundShadow
        shadowColor:(UIColor *)backgroundShadowColor
displayCountingLabel:(BOOL)displayCountingLabel
  overrideLineWidth:(NSNumber *)overrideLineWidth
{
    //1)
    overrideLineWidth = @3.0;
    
    self = [super initWithFrame:frame
                          total:total
                        current:current
                      clockwise:clockwise
                         shadow:shadow
                    shadowColor:backgroundShadowColor
           displayCountingLabel:displayCountingLabel
              overrideLineWidth:overrideLineWidth];

    //2)
    //super.lineWidth = @3.0;
    
    return self;
}

@end
