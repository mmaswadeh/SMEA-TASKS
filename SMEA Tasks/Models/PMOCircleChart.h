//
//  PMOCircleChart.h
//  VRO-BTIT
//
//  Created by Blessed Tree IT on 7/27/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import <PNChart/PNChart.h>

@interface PMOCircleChart : PNCircleChart
- (id)initWithFrame:(CGRect)frame
              total:(NSNumber *)total
            current:(NSNumber *)current
          clockwise:(BOOL)clockwise
             shadow:(BOOL)hasBackgroundShadow
        shadowColor:(UIColor *)backgroundShadowColor
displayCountingLabel:(BOOL)displayCountingLabel
  overrideLineWidth:(NSNumber *)overrideLineWidth;

@end
