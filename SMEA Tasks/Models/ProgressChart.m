//
//  ProgressChart.m
//  PMO-Product
//
//  Created by Blessed Tree IT on 5/7/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import "ProgressChart.h"
#import "PMOCircleChart.h"
#import "AVHexColor.h"

@implementation ProgressChart

+(id)progressDataWith:(double)anumber withProgressColor:(UIColor *)projectObject{
    ProgressChart *ProgressChartcustomView = [[[NSBundle mainBundle] loadNibNamed:@"ProgressChart" owner:nil options:nil] firstObject];
    
    // make sure customView is not nil or the wrong class!
    if ([ProgressChartcustomView isKindOfClass:[ProgressChart class]])
    {
      PMOCircleChart  *circleChartView;
        NSNumber * numberValue=[NSNumber numberWithDouble:anumber];
//    //  circleChartView = [[PMOCircleChart alloc] initWithFrame:CGRectMake(0.0,0.0,48.0,48.0)
//                                                              total:@100
//                                                            current:numberValue
//                                                          clockwise:YES];
//
        NSString *grayHex=@"#DCDCDC";
        UIColor *shadowGray =[AVHexColor colorWithHexString:grayHex];
        circleChartView =[[PMOCircleChart alloc]initWithFrame:CGRectMake(0.0,0.0,48.0,48.0) total:@100 current:numberValue clockwise:YES shadow:YES shadowColor:shadowGray displayCountingLabel:YES];
        
        circleChartView.backgroundColor = [UIColor clearColor];
        
        [circleChartView setStrokeColor:projectObject];
        [circleChartView strokeChart];
        
        [ProgressChartcustomView addSubview:circleChartView];

        
        return ProgressChartcustomView;
        
        
    }
    else
        return nil;
}




@end
