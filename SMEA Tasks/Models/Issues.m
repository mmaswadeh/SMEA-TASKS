//
//  Issues.m
//  PMO Dashboard
//
//  Created by Mohammad Maswadeh on 2/8/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import "Issues.h"

@implementation Issues

/*
-(instancetype)initWithAttributes:(NSDictionary *)attributes
{
    if (self = [super initWithAttributes:attributes])
    {
        
        self.totalCount = [self extractIntegerValue:attributes[@"TotalCount"]];
        
        self.issueTitle = [self extractStringValue:attributes[@"Title"]];
        self.assignTo = [self extractStringValue:attributes[@"Assignto"]];
        
        self.statusNumber = [self extractIntegerValue:attributes[@"StatusNumber"]];
        
        self.totalCount = [self extractIntegerValue:attributes[@"TotalCount"]];
        
        self.issuesColor=[self statusColor:self.statusNumber];

        self.idProjects=[self extractIntegerValue:attributes[@"ID"]];
        // Extract High
        
        self.high =[[High alloc]initWithAttributes:attributes[@"High"]];
        
        // Extract Medium
        
        self.medium =[[Medium alloc]initWithAttributes:attributes[@"Medium"]];


        // Extract Low
        
        self.low =[[Low alloc]initWithAttributes:attributes[@"Low"]];



    }
    
    return self;
    
    
}

- (UIColor*)statusColor:(IssuesStatus)status {
    UIColor* color;
    
    
    switch (status) {
        case IssuesHigh:
            color =[AVHexColor colorWithHexString: @"#ed4e5d"];
            break;
            
        case IssuesMedium:
            color = [AVHexColor colorWithHexString: @"#f8d15c"];
            break;
            
        case IssuesLow:
            color = [AVHexColor colorWithHexString: @"#92d050"];
            break;
            
        default:
            [UIColor clearColor];
            break;
    }
    
    return color;
}

-(NSMutableArray *)extractIssuesHigh:(NSArray *)highDictsArray {
    NSMutableArray *arrHigh = [[NSMutableArray alloc] init];
    
    for (NSDictionary *dictHigh in highDictsArray) {
        High *high = [[High alloc] initWithAttributes:dictHigh];
        [arrHigh addObject:high];
    }
    
    return arrHigh;
}

-(NSMutableArray *)extractIssuesMedium:(NSArray *)mediumDictsArray {
    NSMutableArray *arrMedium = [[NSMutableArray alloc] init];
    
    for (NSDictionary *dictMedium in mediumDictsArray) {
        Medium *medium = [[Medium alloc] initWithAttributes:dictMedium];
        [arrMedium addObject:medium];
    }
    
    return arrMedium;
}

-(NSMutableArray *)extractIssuesLow:(NSArray *)lowDictsArray {
    NSMutableArray *arrLow = [[NSMutableArray alloc] init];
    
    for (NSDictionary *dictLow in lowDictsArray) {
        Low *low = [[Low alloc] initWithAttributes:dictLow];
        [arrLow addObject:low];
    }
    
    return arrLow;
}
*/
@end
