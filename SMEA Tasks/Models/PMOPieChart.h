//
//  PMOPieChart.h
//  VRO-BTIT
//
//  Created by Blessed Tree IT on 7/27/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PNChartDelegate.h"
#import "PNChart.h"
@interface PMOPieChart : PNPieChart

- (void)recompute;


@end
