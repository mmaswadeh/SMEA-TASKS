//
//  Risks.m
//  PMO Dashboard
//
//  Created by Mohammad Maswadeh on 2/8/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import "Risks.h"

@implementation Risks
/*
-(instancetype)initWithAttributes:(NSDictionary *)attributes
{
    if (self = [super initWithAttributes:attributes])
    {
        
        self.riskTitle = [self extractStringValue:attributes[@"Title"]];
        self.riskOwner = [self extractStringValue:attributes[@"Owner"]];

        self.statusNumber = [self extractIntegerValue:attributes[@"StatusNumber"]];

        self.totalCount = [self extractIntegerValue:attributes[@"TotalCount"]];
        
        self.risksColor=[self statusColor:self.statusNumber];

                self.riskID=[self extractIntegerValue:attributes[@"ID"]];
        // Extract High
        
        self.high =[[High alloc]initWithAttributes:attributes[@"High"]];
        
        // Extract Medium
        
        self.medium =[[Medium alloc]initWithAttributes:attributes[@"Medium"]];
        
        
        // Extract Low
        
        self.low =[[Low alloc]initWithAttributes:attributes[@"Low"]];
        
        
        
    }
    
    return self;
    
    
}

- (UIColor*)statusColor:(RisksStatus)status {
    UIColor* color;
    
    
    switch (status) {
        case RisksHigh:
            color =[AVHexColor colorWithHexString: @"#ed4e5d"];
            break;
            
        case RisksMedium:
            color = [AVHexColor colorWithHexString: @"#f8d15c"];
            break;
            
        case RisksLow:
            color = [AVHexColor colorWithHexString: @"#92d050"];
            break;
            
        default:
            [UIColor clearColor];
            break;
    }
    
    return color;
}

-(NSMutableArray *)extractRisksHigh:(NSArray *)highDictsArray {
    NSMutableArray *arrHigh = [[NSMutableArray alloc] init];
    
    for (NSDictionary *dictHigh in highDictsArray) {
        High *high = [[High alloc] initWithAttributes:dictHigh];
        [arrHigh addObject:high];
    }
    
    return arrHigh;
}

-(NSMutableArray *)extractRisksMedium:(NSArray *)mediumDictsArray {
    NSMutableArray *arrMedium = [[NSMutableArray alloc] init];
    
    for (NSDictionary *dictMedium in mediumDictsArray) {
        Medium *medium = [[Medium alloc] initWithAttributes:dictMedium];
        [arrMedium addObject:medium];
    }
    
    return arrMedium;
}

-(NSMutableArray *)extractRisksLow:(NSArray *)lowDictsArray {
    NSMutableArray *arrLow = [[NSMutableArray alloc] init];
    
    for (NSDictionary *dictLow in lowDictsArray) {
        Low *low = [[Low alloc] initWithAttributes:dictLow];
        [arrLow addObject:low];
    }
    
    return arrLow;
}

*/


@end
