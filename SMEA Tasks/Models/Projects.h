//
//  Projects.h
//  PMO Dashboard
//
//  Created by Mohammad Maswadeh on 2/8/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "AVHexColor.h"
#import "BaseObject.h"
#import "ColorHelper.h"

@interface Projects : BaseObject


@property (nonatomic, strong) NSString *entityName;

@property (nonatomic, assign) NSInteger  entityID;
@property (nonatomic, strong) NSString *PhaseStatus;

@property (nonatomic, assign) NSInteger totalCount;

@property (nonatomic, assign) ProgressStatus statusNumber;
@property (nonatomic, assign) InstancesStatus InstanceStatusNumber;

@property (nonatomic, strong) UIColor *phaseColor;
@property (nonatomic, strong) UIColor *InstancephaseColor;
@property (nonatomic, strong) NSString *projectTitle;
@property (nonatomic, strong) NSString *projectPriority;
@property (nonatomic, strong) NSString *startDate;
@property (nonatomic, strong) NSString *finishDate;

@property (nonatomic, assign) NSInteger projectID;
@property (nonatomic, assign) NSInteger issuesCount;
@property (nonatomic, assign) double completedPercent;
@property (nonatomic, assign) NSInteger  feelingId;

//////////////

@property (nonatomic,strong)NSString *projectManager;

@property (nonatomic,strong)NSString *summary;
@property(nonatomic,strong)NSString *ownerPosition;
@property(nonatomic,strong)NSData *projectImage;

@property(nonatomic,strong)NSString *assignto;
@property(nonatomic,strong)NSString *projectName;
@property(nonatomic,strong)NSString *jobPosition;
@property(nonatomic,strong)NSString *name;

@property (nonatomic, assign) double baseline;
@property (nonatomic, assign) double actual;

@property (nonatomic, assign) NSInteger month;

@end
