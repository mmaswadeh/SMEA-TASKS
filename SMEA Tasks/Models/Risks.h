//
//  Risks.h
//  PMO Dashboard
//
//  Created by Mohammad Maswadeh on 2/8/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseObject.h"
#import "High.h"
#import "Medium.h"
#import "Low.h"
#import "AVHexColor.h"
#import "HMLModel.h"

/*typedef enum : NSUInteger {
    RisksHigh = 1,
    RisksMedium = 2,
    RisksLow = 3
} RisksStatus;

*/

@interface Risks : HMLModel

/*
@property (nonatomic, strong) High *high;
@property (nonatomic, strong) Medium *medium;
@property (nonatomic, strong) Low *low;

@property (nonatomic, assign) NSInteger totalCount;

@property (nonatomic, strong) NSString *riskTitle;
@property (nonatomic, strong) NSString *riskOwner;

@property (nonatomic, assign) NSInteger riskID;

@property (nonatomic, assign) RisksStatus statusNumber;

@property (nonatomic, strong) UIColor *risksColor;
*/
@end
