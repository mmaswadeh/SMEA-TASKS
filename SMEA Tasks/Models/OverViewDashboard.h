//
//  OverViewDashboard.h
//  VRO-BTIT
//
//  Created by Blessed Tree IT on 7/24/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "AVHexColor.h"
#import "BaseObject.h"
#import "ColorHelper.h"
#import "HMLModel.h"

@interface OverViewDashboard :  BaseObject

@property (nonatomic, assign) NSInteger objID;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, assign) NSInteger total;
@property (nonatomic, strong) NSString *icon;
@property (nonatomic, assign) NSInteger chart_type;
@property(nonatomic,strong)   NSArray * chart_data;
@property(nonatomic,strong)   NSArray * chart_list;


@end
