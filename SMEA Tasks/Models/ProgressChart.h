//
//  ProgressChart.h
//  PMO-Product
//
//  Created by Blessed Tree IT on 5/7/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PNChartDelegate.h"
#import "PNChart.h"
//#import "Projects.h"

@interface ProgressChart : UIView<PNChartDelegate>

@property(strong,nonatomic) NSNumber *anumber;
@property (strong, nonatomic) IBOutlet UIView *circleChart;
+(id)progressDataWith:(double)anumber withProgressColor:(UIColor *)projectObject;
@end
