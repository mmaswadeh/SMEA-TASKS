//
//  PMOPieChart.m
//  VRO-BTIT
//
//  Created by Blessed Tree IT on 7/27/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import "PMOPieChart.h"
@implementation PMOPieChart
- (void)recompute
{
    self.outerCircleRadius = CGRectGetWidth(self.bounds) / 2;
   // self.innerCircleRadius = CGRectGetWidth(self.bounds) / 6;
    self.innerCircleRadius = 0;
}
@end
