//
//  Medium.m
//  PMO Dashboard
//
//  Created by Mohammad Maswadeh on 2/8/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import "Medium.h"

@implementation Medium

-(instancetype)initWithAttributes:(NSDictionary *)attributes
{
    if (self = [super initWithAttributes:attributes])
    {
        self.count = [self extractIntegerValue:attributes[@"Count"]];
        
       self.percentage  = [self extractIntegerValue:attributes[@"Count"]];
       // self.percentage = [self extractDoubleValue:attributes[@"Percentage"]];
        self.statusNumber = [self extractIntegerValue:attributes[@"StatusNumber"]];
        self.mediumColor=[self statusColor:self.statusNumber];

    }
    
    return self;
    
}


- (UIColor*)statusColor:(HMLStatus)status {
   return [[ColorHelper sharedManager] getColorWithHMLStatus:status];}



@end
