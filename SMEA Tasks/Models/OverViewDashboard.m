//
//  OverViewDashboard.m
//  VRO-BTIT
//
//  Created by Blessed Tree IT on 7/24/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import "OverViewDashboard.h"
#import "Projects.h"
#import "chartData.h"
#import "StrategicObjectives.h"

@implementation OverViewDashboard
-(instancetype)initWithAttributes:(NSDictionary *)attributes
{
    if (self = [super initWithAttributes:attributes])
        
    {
        
        self.objID = [self extractIntegerValue:attributes[@"id"]];
        self.name = [self extractStringValue:attributes[@"name"]];

            self.total = [self extractIntegerValue:attributes[@"total"]];
            self.chart_type = [self extractIntegerValue:attributes[@"chart_type"]];

        // Projects
        if (attributes[@"chartData"] && attributes[@"chartData"] != NSNull.null)
            self.chart_data = [self extractProjects:attributes[@"chartData"]];
        
        if (attributes[@"chartList"] && attributes[@"chartList"] != NSNull.null)
            self.chart_list = [self extractChartList:attributes[@"chartList"]];
        

        

    }
    
    
    return self;
    
}

-(NSMutableArray *)extractProjects:(NSArray *)projectsDictsArray {
    NSMutableArray *arrProjects = [[NSMutableArray alloc] init];
    
    for (NSDictionary *dictProjects in projectsDictsArray) {
       chartData  *projects = [[chartData alloc] initWithAttributes:dictProjects];
        [arrProjects addObject:projects];
    }
    
    return arrProjects;
}

-(NSMutableArray *)extractChartList:(NSArray *)chartListDictsArray {
    NSMutableArray *arrList = [[NSMutableArray alloc] init];
    
    for (NSDictionary *dictList in chartListDictsArray) {
        StrategicObjectives  *list = [[StrategicObjectives alloc] initWithAttributes:dictList];
        [arrList addObject:list];
    }
    
    return arrList;
}


@end
