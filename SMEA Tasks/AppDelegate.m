//
//  AppDelegate.m
//  SMEA Tasks
//
//  Created by Mohammad Maswadeh on 3/6/18.
//  Copyright © 2018 BTIT. All rights reserved.
//

#import "AppDelegate.h"
#import "MainViewController.h"
#import "TasksResquest.h"
#import "Reachability.h"
#import "DBManager.h"

@interface AppDelegate ()

@property (nonatomic, strong) DBManager *dbManager;

@end


@implementation AppDelegate


NSUserDefaults *getProfile;
Reachability* reachability;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.dbManager = [[DBManager alloc] initWithDatabaseFilename:@"TasksDB.sqlite"];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleNetworkChange:) name:kReachabilityChangedNotification object:nil];
    
    reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    
    NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
    
    if(remoteHostStatus == NotReachable) {
        
        NSLog(@"no");
    }
    else if (remoteHostStatus == ReachableViaWiFi) {
        
        NSLog(@"wifi");
        [self loadData];
        
        
    }
    else if (remoteHostStatus == ReachableViaWWAN) {
        
        NSLog(@"cell");
        [self loadData];
        
    }
    
    getProfile= [NSUserDefaults standardUserDefaults];
    NSString *profileUser = [getProfile objectForKey:@"loggedUser"];
    
    [TasksResquest getAssigneeUserlistWithSuccess:^(NSMutableArray *usersList,NSMutableArray *usersID) {
        
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setObject:usersList forKey:@"assingeeNames"];
        [userDefaults setObject:usersID forKey:@"assingeeIds"];
        [userDefaults synchronize];
        
        
    } failure:^(NSError *error) {
        
        NSLog(@"Error = %@",error);
        
        
    }];
    [TasksResquest getAgenciesUserlistWithSuccess:^(NSMutableArray *usersList, NSMutableArray *usersID) {
        
        
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setObject:usersList forKey:@"agencyNames"];
        [userDefaults setObject:usersID forKey:@"agencyIds"];
        [userDefaults synchronize];
    } failure:^(NSError *error) {
        NSLog(@"Error = %@",error);
    }];
    
    
    [TasksResquest getClosureTypesWithSuccess:^(NSMutableArray *closureNames, NSMutableArray *closueIds) {
        
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setObject:closureNames forKey:@"closureNames"];
        [userDefaults setObject:closueIds forKey:@"closureId"];
        [userDefaults synchronize];
        
    } failure:^(NSError *error) {
        
    }];
    
    [TasksResquest getSourceslistWithSuccess:^(NSMutableArray *sourceList, NSMutableArray *sourceID) {

        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setObject:sourceList forKey:@"sourcesName"];
        [userDefaults setObject:sourceID forKey:@"sourcesIds"];
        [userDefaults synchronize];
        
    } failure:^(NSError *error) {
        
    }];
    
    if (profileUser == nil ) {
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"NavigationController"];
        [navigationController setViewControllers:@[[storyboard instantiateViewControllerWithIdentifier:@"LoginpageViewController"]]];
        UIWindow *window = UIApplication.sharedApplication.delegate.window;
        window.rootViewController = navigationController;
        [UIView transitionWithView:window
                          duration:0.3
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:nil
                        completion:nil];
        
        self.window.tintColor = [UIColor whiteColor];
        
        
    }
    else{
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"NavigationController"];
        
        [navigationController setViewControllers:@[[storyboard instantiateViewControllerWithIdentifier:@"MyTasksDashboardViewController"]]];
        
        MainViewController *mainViewController = [storyboard instantiateInitialViewController];
        mainViewController.rootViewController = navigationController;
        
        UIWindow *window = UIApplication.sharedApplication.delegate.window;
        window.rootViewController = mainViewController;
        [UIView transitionWithView:window
                          duration:0.3
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:nil
                        completion:nil];
        
        self.window.tintColor = [UIColor whiteColor];
        
        
    }
    
    return YES;
    
}

- (void)handleNetworkChange:(NSNotification *)notice
{
    
    NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
    
    if(remoteHostStatus == NotReachable) {
        
        NSLog(@"no");
    }
    else if (remoteHostStatus == ReachableViaWiFi) {
        
        NSLog(@"wifi");
        [self loadData];
    }
    else if (remoteHostStatus == ReachableViaWWAN){
        
        NSLog(@"cell");
        [self loadData];
        
        
    }
}


-(void)loadData{
    // Form the query.
    
    NSInteger count;
    
    NSString *query = @"select * from OfflineTasks";
    
    // Get the results.
    if (self.arrTaskInfo != nil) {
        self.arrTaskInfo = nil;
        
        
    }
    _offlineTasks = [[[self.dbManager loadDataFromDB:query] reverseObjectEnumerator] allObjects];
    
    int arrayLength = (int)_offlineTasks.count;
    count = _offlineTasks.count;
    
    NSLog(@"Array length =%d",arrayLength);
    NSLog(@"Count = %ld",(long)count);
    
    
    self.arrTaskInfo = [[NSArray alloc] initWithArray:_offlineTasks];
    
    if (count > 0) {
        
        [self uploadTasksAutomatically:arrayLength];
        
    }
    
}


-(void)uploadTasksAutomatically:(int)count

{
    
    for (int i=0; i < count; i++) {
        
        _offlineTaskId = [[[self.arrTaskInfo objectAtIndex:i] objectAtIndex:0]integerValue];
        _offlineTitle = [[self.arrTaskInfo objectAtIndex:i] objectAtIndex:1];
        _offlineDescription = [[self.arrTaskInfo objectAtIndex:i] objectAtIndex:2];
        _offlineUserId = [[[self.arrTaskInfo objectAtIndex:i] objectAtIndex:3]integerValue];
        _offlinePath = [[self.arrTaskInfo objectAtIndex:i] objectAtIndex:4];
        _offlineStartingDate = [[self.arrTaskInfo objectAtIndex:i] objectAtIndex:5];
        _offlineEndingDate = [[self.arrTaskInfo objectAtIndex:i] objectAtIndex:6];
        _offlineAssignee = [[self.arrTaskInfo objectAtIndex:i] objectAtIndex:7];
        
       [self uploadRequestWithComment:_offlineDescription andTitle:_offlineTitle andUserid:_offlineUserId andTaskId:_offlineTaskId andImagePath:_offlinePath andImageData:nil andStartingDate:_offlineStartingDate andEndingDate:_offlineEndingDate];
    }
    
}

-(void)uploadRequestWithComment:(NSString*)comment andTitle:(NSString*)title andUserid:(NSInteger)userId andTaskId:(NSInteger)taskId andImagePath:(NSString *)imagePath andImageData:(NSData*)imageData andStartingDate:(NSString *)startDate andEndingDate:(NSString *)endDate

{

    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:
                      [NSString stringWithFormat:@"%@",imagePath]];


    NSError* error = nil;
    NSData* data = [NSData dataWithContentsOfFile:path  options:0 error:&error];

    NSLog(@"Data read from %@ with error: %@", imagePath, error);


    //    [TasksResquest addTaskWithComment:comment andTitle:title andUserid:userId andTaskId:0 andImagePath:imagePath andImageData:data andStartingDate:startDate andEndingDate:endDate andAgencyid:1 success:^(NSDictionary *comments) {
    
    
    
[TasksResquest addTaskWithComment:comment andTitle:title andAssigneeid:userId andTaskId:0  andImagePath:imagePath  andImageData:data  andStartingDate:startDate  andEndingDate:endDate  andAgencyid:1  andUserid:1  andSourceId:1  andMeetingNumber:1   success:^(NSDictionary *comments) {

        [self DeleteRecord:taskId];


    } failure:^(NSError *error) {

        NSLog(@"Error = %@",error);

    }];


}

-(void)DeleteRecord:(NSInteger )taskId
{
    // Delete the selected record.
    // Find the record ID.
    self.dbManager = [[DBManager alloc] initWithDatabaseFilename:@"TasksDB.sqlite"];
    
    // Prepare the query.
    NSString *query = [NSString stringWithFormat:@"delete from OfflineTasks where TaskId=%ld", (long)taskId];
    
    // Execute the query.
    [self.dbManager executeQuery:query];
    
    // Reload the table view.
    
    
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
