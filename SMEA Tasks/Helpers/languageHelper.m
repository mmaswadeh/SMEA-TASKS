//
//  languageHelper.m
//  MCI
//
//  Created by Blessed Tree IT on 4/27/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import "languageHelper.h"

@implementation languageHelper
@synthesize languageSet;
- (id)init {
    if (self = [super init]) {
        languageSet = [[NSString alloc] init];
    }
    return self;
}

-(NSString*)checkLanguages{
 
        
        NSArray *languageArray = [NSLocale preferredLanguages];
        NSString *language = [languageArray objectAtIndex:0];
        
        
        NSArray * indexofDash = [language componentsSeparatedByString:@"-"];
        NSString *languageWithoutRegionCode =[indexofDash objectAtIndex:0];
        
        
        if([languageWithoutRegionCode isEqualToString:@"en"])
        {
            languageSet=languageWithoutRegionCode;
        }
        else  if([languageWithoutRegionCode isEqualToString:@"ar"])
        {
           languageSet=languageWithoutRegionCode;
        }

    return languageSet;
 
    
    
    

    //   return checkLanguagesString;
}

//NSArray *languageArray = [NSLocale preferredLanguages];
//NSString *language = [languageArray objectAtIndex:0];
//
//
//NSArray * indexofDash = [language componentsSeparatedByString:@"-"];
//NSString *languageWithoutRegionCode =[indexofDash objectAtIndex:0];
//
//
//if([languageWithoutRegionCode isEqualToString:@"en"])
//{
//    LangID=2;
//}
//else  if([languageWithoutRegionCode isEqualToString:@"ar"])
//{
//    LangID=1;
//}
//else
//{ LangID=2;
//}


@end
