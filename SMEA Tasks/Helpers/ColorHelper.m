//
//  ColorHelper.m
//  MCI
//
//  Created by Blessed Tree IT on 5/22/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import "ColorHelper.h"
#import <Foundation/Foundation.h>
static ColorHelper *colorHelper;
@implementation ColorHelper
@synthesize colorSettings;

- (id)init {
    if (self = [super init]) {
        self.off_white = @"#FEFEFE";
        self.pink=@"#E91E63";
        self.indigo=@"#3F51B5";
       self.orange=@"#FF8C50";
       self.red=@"#E64A19";
       self.green=@"#8BC34A";
       self.yellow=@"#FFEB3B";
        
       self.lightgrey=@"#C8C8C8";
       self.darkBlue=@"#303F9F";
       self.blue=@"#3F51B5";
       self.grey_300=@"#E0E0E0";
       self.black_semi_transparent=@"#B2000000";
       self.background=@"#e5e5e5";
       self.half_black=@"#808080";
       self.blue_semi_transparent=@"#805677fc";
       self.light_blue=@"#3499e0";
       self.line_baseline=@"#ed4e5d";
       self.line_actual=@"#006600";
       self.indicator_overdue=@"#000";
       self.indicator_delayed=@"#ed4e5d";
       self.indicator_delayed_recoverable=@"#f8d15c";
       self.indicator_on_track=@"#92d050";
       self.indicator_ahead_of_plan=@"#3499e0";
       self.indicator_delivered=@"#006600";
       self.indicator_not_started=@"#7f7f7f";
       self.indicator_no_baseline=@"#d3d3d3";
       self.indicator_high=@"#ed4e5d";
       self.indicator_medium=@"#f8d15c";
       self.indicator_low=@"#92d050";
       self.indicatorColor=@"#40000000";
       self.rippleColor=@"#30000000";
       self.textColor=@"#616161";
       self.titleTextColor=@"#424242";
       self.headerTextColor=@"#212121";
        self.nwExpectedDate = @"#9C27B0";
        
        self.overdue=@"#FF2B00";
        self.delayed_recoverable=@"#FFD455";
        self.on_track=@"#008000";
        self.delivered=@"#007FFF";
        self.onHold=@"#AA0080";
        
        
        self.canceled=@"#F44336";
        self.no_baseline=@"#9E9E9E";
        
        self.Critical=@"#F7412D";
        self.OnTime=@"#47B04B";
        self.NotStarted=@"#999999";
        self.Late=@"#FFDE00";
        
        self.completed=@"#448AFF";
        self.notSet=@"#B0BEC5";
    
    
    }
    return self;
}
+ (id)sharedManager
{
    
    if (colorHelper == nil)
    {
        colorHelper = [[ColorHelper alloc] init];
        
    }
    
    return colorHelper;
}


-(UIColor *)getColorWithHMLStatus:(NSInteger)HMLstatusNumber{


    switch (HMLstatusNumber) {
        case eHigh:
           return [AVHexColor colorWithHexString:self.indicator_high];
            break;
            
        case eMedium:
           return [AVHexColor colorWithHexString: self.indicator_medium];
            break;
            
        case eLow:
           return[AVHexColor colorWithHexString: self.indicator_low];
            break;
            
        default:
            return [UIColor clearColor];
            break;
    }
    

}
-(UIColor *)getColorWithStatus:(NSInteger)statusNumber {
    
    
    switch (statusNumber) {
        case Delivered:
            return [AVHexColor colorWithHexString:self.delivered];
            break;
            
        case OnTrack:
            return [AVHexColor colorWithHexString: self.on_track];
            break;
            
        case DelayedRecoverable:
            return [AVHexColor colorWithHexString: self.delayed_recoverable];
            break;
            
        case Overdue:
            return [AVHexColor colorWithHexString: self.overdue];
            break;
            
        case onHold:
            return [AVHexColor colorWithHexString: self.onHold];
            break;
              default:
            return [UIColor clearColor];
            break;
    }
    
    
}
-(UIColor *)getColorOfInstancesWithStatus:(NSInteger)statusNumber {
    
    
    switch (statusNumber) {
   
            
        case IOnTrack:
            return [AVHexColor colorWithHexString: self.on_track];
            break;
            
        case IDelayedRecoverable:
            return [AVHexColor colorWithHexString: self.delayed_recoverable];
            break;
            
        case IOverdue:
            return [AVHexColor colorWithHexString: self.overdue];
            break;
            
    
        default:
            return [UIColor clearColor];
            break;
    }
    
    
}

-(UIColor *)getColorWithDefaultStatus:(NSInteger)statusNumber {
    
    
    switch (statusNumber) {
        case eOverdue:
            return [AVHexColor colorWithHexString:self.indicator_overdue];
            break;
            
        case eDelayed:
            return [AVHexColor colorWithHexString: self.indicator_delayed];
            break;
            
        case eDelayedRecoverable:
            return [AVHexColor colorWithHexString: self.indicator_delayed_recoverable];
            break;
            
        case eOnTrack:
            return [AVHexColor colorWithHexString: self.indicator_on_track];
            break;
            
        case eAheadofPlan:
            return [AVHexColor colorWithHexString: self.indicator_ahead_of_plan];
            break;
            
        case eDelivered:
            return [AVHexColor colorWithHexString: self.indicator_delivered];
            break;
            
        case eNotStarted:
            return  [AVHexColor colorWithHexString: self.indicator_not_started];
            break;
            
        case eNoBaseline:
            return  [AVHexColor colorWithHexString: self.indicator_no_baseline];
            break;
            
        default:
            return [UIColor clearColor];
            break;
    }
    
    
}

-(UIColor *)getColorOfTasksWithStatus:(NSInteger)statusNumber
{
    switch (statusNumber) {
            
            
            
        case TaskStatus_New:
            return [AVHexColor colorWithHexString: self.onHold];
            break;
            
        case TaskStatus_InProgress:
            return [AVHexColor colorWithHexString: self.canceled];
            break;
            
        case TaskStatus_Comp:
            return [AVHexColor colorWithHexString: self.no_baseline];
            break;
            
        case TaskStatus_NotSet:
            return [AVHexColor colorWithHexString: self.notSet];
            break;
            
        case TaskStatus_OnTrack:
            return [AVHexColor colorWithHexString: self.on_track];
            break;
            
        case TaskStatus_Late:
            return [AVHexColor colorWithHexString: self.Late];
            break;
            
        case TaskStatus_Critical:
            return [AVHexColor colorWithHexString: self.Critical];
            break;
            
        case TaskStatus_Completed:
            return [AVHexColor colorWithHexString: self.completed];
            break;
            
            case TaskStatus_NewExpectedDate:
            return [AVHexColor colorWithHexString:self.nwExpectedDate];
            break;
            
        default:
            return [UIColor clearColor];
            break;
    }
    
}


@end
