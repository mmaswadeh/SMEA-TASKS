//
//  ColorHelper.h
//  MCI
//
//  Created by Blessed Tree IT on 5/22/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "AVHexColor.h"

typedef enum : NSUInteger {
    Delivered = 1,
    OnTrack = 2,
    DelayedRecoverable = 3,
    Overdue = 4,
    onHold = 5,
} ProgressStatus;

typedef enum : NSUInteger {
    
    IOnTrack = 1,
    IDelayedRecoverable = 2,
    IOverdue = 3
   
} InstancesStatus;


typedef enum : NSUInteger {
   eHigh = 1,
   eMedium = 2,
   eLow = 3
} HMLStatus;


typedef enum : NSUInteger {
    eOverdue = 1,
    eDelayed = 2,
    eDelayedRecoverable = 3,
    eOnTrack = 4,
    eAheadofPlan = 5,
    eDelivered = 6,
    eNotStarted = 7,
    eNoBaseline = 8
} DefaultStatus;

typedef enum : NSUInteger {
    
    
    TaskStatus_New = 90,
    TaskStatus_InProgress = 91,
    TaskStatus_Comp = 92,
    TaskStatus_NotSet = 610,
    TaskStatus_OnTrack = 611,
    TaskStatus_Late = 612,
    TaskStatus_Critical = 613,
    TaskStatus_Completed = 614,
    TaskStatus_NewExpectedDate = 649
} taskStatus;

@interface ColorHelper : NSObject


@property(weak, nonatomic) NSString *off_white;
@property(weak, nonatomic) NSString *pink;
@property(weak, nonatomic) NSString *indigo;
@property(weak, nonatomic) NSString *orange;
@property(weak, nonatomic) NSString *red;
@property(weak, nonatomic) NSString *green;
@property(weak, nonatomic) NSString *yellow;
@property(weak, nonatomic) NSString *lightgrey;
@property(weak, nonatomic) NSString *darkBlue;
@property(weak, nonatomic) NSString *blue;
@property(weak, nonatomic) NSString *grey_300;
@property(weak, nonatomic) NSString *black_semi_transparent;
@property(weak, nonatomic) NSString *background;
@property(weak, nonatomic) NSString *half_black;
@property(weak, nonatomic) NSString *blue_semi_transparent;
@property(weak, nonatomic) NSString *light_blue;
@property(weak, nonatomic) NSString *line_baseline;
@property(weak, nonatomic) NSString *line_actual;
@property(weak, nonatomic) NSString *indicator_overdue;
@property(weak, nonatomic) NSString *indicator_delayed;
@property(weak, nonatomic) NSString *indicator_delayed_recoverable;
@property(weak, nonatomic) NSString *indicator_on_track;
@property(weak, nonatomic) NSString *indicator_ahead_of_plan;
@property(weak, nonatomic) NSString *indicator_delivered;
@property(weak, nonatomic) NSString *indicator_not_started;
@property(weak, nonatomic) NSString *indicator_no_baseline;
@property(weak, nonatomic) NSString *indicator_high;
@property(weak, nonatomic) NSString *indicator_medium;
@property(weak, nonatomic) NSString *indicator_low;
@property(weak, nonatomic) NSString *indicatorColor;
@property(weak, nonatomic) NSString *rippleColor;
@property(weak, nonatomic) NSString *textColor;
@property(weak, nonatomic) NSString *titleTextColor;
@property(weak, nonatomic) NSString *headerTextColor;
@property(weak, nonatomic) NSString  *onHold;

@property(weak, nonatomic) NSString  *canceled;
@property(weak, nonatomic) NSString  *no_baseline;

@property(weak, nonatomic) NSString *overdue;
@property(weak, nonatomic) NSString *delayed_recoverable;
@property(weak, nonatomic) NSString *on_track;
@property(weak, nonatomic) NSString *delivered;
@property(weak,nonatomic)  NSString * AheadofPlan;

@property(weak,nonatomic)  NSString * OnTime;
@property(weak,nonatomic)  NSString * Late;
@property(weak,nonatomic)  NSString * Critical;
@property(weak,nonatomic)  NSString * NotStarted;
@property(weak,nonatomic)  NSString * nwExpectedDate;

@property(weak,nonatomic)  NSString * completed;
@property(weak,nonatomic)  NSString * notSet;


@property (nonatomic, strong) ColorHelper *colorSettings;

+ (id)sharedManager;
-(UIColor *)getColorWithDefaultStatus:(NSInteger)statusNumber;
-(UIColor *)getColorWithStatus:(NSInteger)statusNumber;
-(UIColor *)getColorWithHMLStatus:(NSInteger)HMLstatusNumber;
-(UIColor *)getColorOfInstancesWithStatus:(NSInteger)statusNumber;
-(UIColor *)getColorOfTasksWithStatus:(NSInteger)statusNumber;

@end

