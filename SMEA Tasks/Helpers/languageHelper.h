//
//  languageHelper.h
//  MCI
//
//  Created by Blessed Tree IT on 4/27/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface languageHelper : NSObject
@property (nonatomic, retain) NSString *languageSet;

-(NSString*)checkLanguages;
@end
