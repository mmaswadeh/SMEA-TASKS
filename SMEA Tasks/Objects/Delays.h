//
//  Delays.h
//  MCI-Tasks
//
//  Created by Blessed Tree IT on 1/14/18.
//  Copyright © 2018 BTIT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseObject.h"
@interface Delays : BaseObject
@property (nonatomic, assign) NSInteger Id;
@property (nonatomic, strong) NSString *NewEndDate;
@property (nonatomic, strong) NSString *Justification;
@property (nonatomic, assign) NSInteger TaskId;


@end
