//
//  Users.h
//  VRO-BTIT
//
//  Created by Mohammad Maswadeh on 9/28/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseObject.h"

@interface Users : BaseObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *userName;
@property (nonatomic, assign) NSInteger  sourceId;
@property (nonatomic, strong) NSString * sourceName;
@property (nonatomic, assign) NSInteger userId;
@property (nonatomic, assign) NSInteger agencyId;


@end

