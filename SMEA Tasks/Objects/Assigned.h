//
//  Assigned.h
//  Dashboard-Core
//
//  Created by Blessed Tree IT on 6/8/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseObject.h"
@interface Assigned : BaseObject


@property (nonatomic, assign) NSInteger      ProfileID;
@property (nonatomic, strong) NSString     * FirstName;
@property (nonatomic, strong) NSString     * SecondName;
@property (nonatomic, strong) NSString     * ThirdName;
@property (nonatomic, strong) NSString     * LastName;
@property (nonatomic, strong) NSString     * Email;
@property (nonatomic, strong) NSString     * JobTitle;
@property (nonatomic, strong) NSString     * MobileNumber;
@property (nonatomic, assign) NSInteger      UserId;
@property (nonatomic, strong) NSString     * ImageURL;
@property (nonatomic, strong) NSString     * FullName;
@property (nonatomic, strong) NSString     * UserName;
@end
