//
//  Tasks.m
//  Dashboard-Core
//
//  Created by Blessed Tree IT on 6/8/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import "Tasks.h"
#import "ColorHelper.h"
#import "Delays.h"


@implementation Tasks
-(instancetype)initWithAttributes:(NSDictionary *)attributes
{
    if (self = [super initWithAttributes:attributes])
        
    {

        self.taskID = [self extractIntegerValue:attributes[@"id"]];
        self.Description = [self extractStringValue:attributes[@"Description"]];
        self.DueDate = [self extractStringValue:attributes[@"DueDate"]];
        
        self.StartDate = [self extractStringValue:attributes[@"StartDate"]];
        self.Effort = [self extractStringValue:attributes[@"Effort"]];
        
        self.Priority = [self extractIntegerValue:attributes[@"Priority"]];
        self.Status = [self extractIntegerValue:attributes[@"Status"]];
        self.Progress = [self extractIntegerValue:attributes[@"Progress"]];
        self.CommentsCount = [self extractIntegerValue:attributes[@"CommentsCount"]];
        
     
        self.title= [self extractStringValue:attributes[@"Title"]];
        self.comments = [self extractStringValue:attributes[@"Comments"]];
        self.taskDescription = [self extractStringValue:attributes[@"Comments"]];
        
        self.assigneeUserName = [self extractStringValue:attributes[@"AssigneeName"]];
        self.filesUrl = [self extractStringValue:attributes[@"FilesUrl"]];
        self.startingDate = [self extractStringValue:attributes[@"StartingDate"]];
        
        if ([self.startingDate isEqualToString:@""]) {
            self.startingDate = NSLocalizedString(@"NoStartingDate", nil);
            
        }
        self.endingDate = [self extractStringValue:attributes[@"EndingDate"]];
        
        if ([self.endingDate isEqualToString:@""]) {
            self.endingDate = NSLocalizedString(@"NoEndingDate", nil);;
            
        }
        self.taskId  = [self extractIntegerValue:attributes[@"TaskId"]];
        self.taskStatus  = [self extractIntegerValue:attributes[@"TaskProgressStatus"]];
        self.TaskPerfomanceStatus= [self extractIntegerValue:attributes[@"TaskPerfomanceStatus"]];
        
        [self checkStatusName];
        
        self.AssigneeUserId  = [self extractIntegerValue:attributes[@"AssigneeUserId"]];
        
        
        self.completePercentage = [self extractIntegerValue:attributes[@"CompletePercentage"]];
        
        self.taskColor=[self statusInstanceColor:self.taskStatus];
        
        
          if (attributes[@"Delays"] && attributes[@"Delays"] != NSNull.null)
              self.Delays = [self extractDelays:attributes[@"Delays"]];
              
       
        if (attributes[@"Agency"] && attributes[@"Agency"] != NSNull.null)
            self.Agency = [self extractAgency:attributes[@"Agency"]];
        
          self.ClouserMessage = [self extractStringValue:attributes[@"ClouserMessage"]];
          self.Clousertype    = [self extractStringValue:attributes[@"ClosureType"]];
        self.Sources=[self extractAgency:attributes[@"Source"]];
        self.MeetingNumber= [self extractStringValue:attributes[@"MeetingNumber"]];
    }
    return self;
    
}

-(NSMutableArray *)extractDelays:(NSArray *)dalaysArr{
    
 
    NSMutableArray *arrDelays = [[NSMutableArray alloc] init];
    
    for (NSDictionary *dictDelays in dalaysArr) {
        Delays * items =[[Delays alloc]initWithAttributes:dictDelays];
        [arrDelays addObject:items];
    }
    
    return arrDelays;
    
    
}
-(Users *)extractAgency:(NSDictionary *)agencyDic{
    
    Users * items= [[Users alloc]initWithAttributes:agencyDic];
    return items;
    
    
}
- (UIColor*)statusInstanceColor:(taskStatus)status {
    return [[ColorHelper sharedManager] getColorOfTasksWithStatus:status];
}


-(void)checkStatusName
{
    if (self.taskStatus == 610) {
        
        self.statusText = NSLocalizedString(@"NotSet", nil);
    }
    
    else if (self.taskStatus == 611) {
        
        self.statusText = NSLocalizedString(@"OnTrack", nil);
        
    }
    
    else if (self.taskStatus == 612) {
        
        self.statusText = NSLocalizedString(@"Late", nil);
        
        
    }
    else if (self.taskStatus == 613){
        
        self.statusText = NSLocalizedString(@"Overdue", nil);
        
    }
    else if (self.taskStatus == 614){
        
        self.statusText = NSLocalizedString(@"Completed", nil);
        
    }
    
    else if (self.taskStatus == 649){
        
        self.statusText = NSLocalizedString(@"NewExpectedDate", nil);
        
    }
    
}


@end
