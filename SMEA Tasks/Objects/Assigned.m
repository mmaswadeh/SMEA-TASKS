//
//  Assigned.m
//  Dashboard-Core
//
//  Created by Blessed Tree IT on 6/8/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import "Assigned.h"

@implementation Assigned

-(instancetype)initWithAttributes:(NSDictionary *)attributes
{
    if (self = [super initWithAttributes:attributes])
        
    {
        
        
        
        self.ProfileID        = [self extractIntegerValue:attributes[@"id"]];
        self.FirstName        = [self extractStringValue:attributes[@"FirstName"]];
        self.SecondName       = [self extractStringValue:attributes[@"SecondName"]];
        
        self.ThirdName        = [self extractStringValue:attributes[@"ThirdName"]];
        self.LastName         = [self extractStringValue:attributes[@"LastName"]];
        self.Email            = [self extractStringValue:attributes[@"Email"]];
        
        self.JobTitle         = [self extractStringValue:attributes[@"JobTitle"]];
        
        self.MobileNumber     = [self extractStringValue:attributes[@"MobileNumbe"]];
        self.UserId           = [self extractIntegerValue:attributes[@"UserId"]];
    
        self.ImageURL         = [self extractStringValue:attributes[@"ImageURL"]];
        self.FullName         = [self extractStringValue:attributes[@"FullName"]];
        self.UserName         = [self extractStringValue:attributes[@"UserName"]];
        
        
        
        
        
    }
    return self;
    
}




@end
