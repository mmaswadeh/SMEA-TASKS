//
//  Dashboard.h
//  MCI-Tasks
//
//  Created by Blessed Tree IT on 1/14/18.
//  Copyright © 2018 BTIT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseObject.h"
#import <UIKit/UIKit.h>
#import "ColorHelper.h"

@interface Dashboard : BaseObject

@property (nonatomic, assign) NSInteger     StatusLkpId;
@property (nonatomic, strong) NSString     *Status;
@property (nonatomic, strong) NSString     *StatusColor;
@property (nonatomic, assign) NSInteger     Count;
@property (nonatomic, strong) UIColor      *colorIs;
@property (nonatomic, strong) NSString     *statusText;
@property (nonatomic, assign) NSInteger     agencyId;
@property (nonatomic, strong) NSString     *agencyName;
@property (nonatomic, strong) NSArray      * taskStatuses;

@end
