//
//  profileInfo.m
//  MCI
//
//  Created by Blessed Tree IT on 4/6/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import "profileInfo.h"

@implementation profileInfo
-(instancetype)initWithAttributes:(NSDictionary *)attributes
{
    if (self = [super initWithAttributes:attributes]) {
        


self.FirstName= [self extractStringValue:attributes[@"FirstName"]];
        
self.Email = [self extractStringValue:attributes[@"Email"]];
self.LoginName = [self extractStringValue:attributes[@"LoginName"]];

self.profileId  = [self extractIntegerValue:attributes[@"ID"]];
    
    
    }
return self;
}
@end
