//
//  Closure.m
//  MCI-Tasks
//
//  Created by Mohammad Maswadeh on 1/25/18.
//  Copyright © 2018 BTIT. All rights reserved.
//

#import "Closure.h"

@implementation Closure

-(instancetype)initWithAttributes:(NSDictionary *)attributes
{
    if (self = [super initWithAttributes:attributes]) {
        
        self.closureId  = [self extractIntegerValue:attributes[@"Id"]];
        self.closureName = [self extractStringValue:attributes[@"Name"]];

    }
    
    return self;
}
@end
