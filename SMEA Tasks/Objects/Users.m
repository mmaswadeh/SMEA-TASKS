//
//  Users.m
//  VRO-BTIT
//
//  Created by Mohammad Maswadeh on 9/28/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import "Users.h"

@implementation Users

-(instancetype)initWithAttributes:(NSDictionary *)attributes
{
    if (self = [super initWithAttributes:attributes]) {
        
        self.name = [self extractStringValue:attributes[@"Name"]];
        self.userName = [self extractStringValue:attributes[@"UserName"]];
        self.userId  = [self extractIntegerValue:attributes[@"UserId"]];
        self.agencyId  = [self extractIntegerValue:attributes[@"Id"]];
        
        self.sourceName = [self extractStringValue:attributes[@"Name"]];
        self.sourceId   = [self extractIntegerValue:attributes[@"Id"]];

    }
    
    return self;
}


@end
