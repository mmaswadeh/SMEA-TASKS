//
//  Delays.m
//  MCI-Tasks
//
//  Created by Blessed Tree IT on 1/14/18.
//  Copyright © 2018 BTIT. All rights reserved.
//

#import "Delays.h"

@implementation Delays
-(instancetype)initWithAttributes:(NSDictionary *)attributes
{
    if (self = [super initWithAttributes:attributes]) {
     
        self.Id  = [self extractIntegerValue:attributes[@"Id"]];
        self.NewEndDate = [self extractStringValue:attributes[@"NewEndDate"]];
        self.Justification = [self extractStringValue:attributes[@"Justification"]];
        self.TaskId  = [self extractIntegerValue:attributes[@"TaskId"]];
        
        
    }
    
    return self;
}

@end
