//
//  serialNumber.h
//  PMO Dashboard
//
//  Created by Blessed Tree IT on 2/19/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface serialNumber : UIDevice
@property (nonatomic, readonly) NSString *serialNumber;

@end
