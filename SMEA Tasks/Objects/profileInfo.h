//
//  profileInfo.h
//  MCI
//
//  Created by Blessed Tree IT on 4/6/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseObject.h"
@interface profileInfo : BaseObject
@property (nonatomic, assign) NSInteger profileId;
@property (nonatomic, strong) NSString *FirstName;
@property (nonatomic, strong) NSString *Email;
@property (nonatomic, strong) NSString *LoginName;

/*"": "",
 "id": "",
 "email": "",
 "IsSuccess": "false",
 "error": "The user name or password is incorrect.",*/


@end
