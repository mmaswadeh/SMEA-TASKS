//
//  Error.m
//  PMO Dashboard
//
//  Created by Mohammad Maswadeh on 2/8/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import "Errors.h"

@implementation Errors


-(instancetype)initWithAttributes:(NSDictionary *)attributes
{
    if (self = [super initWithAttributes:attributes]) {
        self.errorCode = [self extractIntegerValue:attributes[@"Code"]];
        self.errorDescription = [self extractStringValue:attributes[@"Description"]];
    }
    
    return self;
}



@end
