//
//  Closure.h
//  MCI-Tasks
//
//  Created by Mohammad Maswadeh on 1/25/18.
//  Copyright © 2018 BTIT. All rights reserved.
//

#import "BaseObject.h"

@interface Closure : BaseObject

@property (nonatomic, assign) NSInteger closureId;
@property (nonatomic, strong) NSString *closureName;

@end
