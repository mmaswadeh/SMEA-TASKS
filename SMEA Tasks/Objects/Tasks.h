//
//  Tasks.h
//  Dashboard-Core
//
//  Created by Blessed Tree IT on 6/8/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "BaseObject.h"
#import "Users.h"

@interface Tasks : BaseObject

@property (nonatomic, assign) NSInteger      taskID;
@property (nonatomic, strong) NSDictionary * AssignedBy;
@property (nonatomic, strong) NSDictionary * AssignedTo;
@property (nonatomic, strong) NSString     * Description;
@property (nonatomic, strong) NSString     * DueDate;
@property (nonatomic, assign) NSInteger      Priority;
@property (nonatomic, strong) NSString     * StartDate;
@property (nonatomic, assign) NSInteger      Status;
@property (nonatomic, strong) NSString     * Effort;
@property (nonatomic, strong) NSDictionary * CreatedBy;
@property (nonatomic, strong) NSString     * DelegateTo;
@property (nonatomic, assign) NSInteger      Progress;
@property (nonatomic, assign) NSInteger      CommentsCount;
@property (nonatomic, strong) NSArray      * Attachments;
@property (nonatomic, strong) NSArray      * SuggestionsComplaints;
@property (nonatomic, strong) NSArray      * ConcernedPeople;

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *comments;
@property (nonatomic, strong) NSString *assigneeUserName;
@property (nonatomic, strong) NSString *filesUrl;
@property (nonatomic, strong) NSString *startingDate;
@property (nonatomic, strong) NSString *endingDate;
@property (nonatomic, strong) NSString *taskDescription;
@property (nonatomic, strong) NSString *statusText;

@property (nonatomic, assign) NSInteger taskId;
@property (nonatomic, assign) NSInteger completePercentage;
@property (nonatomic, assign) NSInteger taskStatus;
@property (nonatomic, assign) NSInteger TaskPerfomanceStatus;
@property (nonatomic, assign) NSInteger AssigneeUserId;
@property (nonatomic, strong) Users    *Agency;
@property (nonatomic, strong) Users    *Sources;
@property (nonatomic, strong) NSArray  *Delays;
@property (nonatomic, strong) UIColor  *taskColor;

@property (nonatomic, strong) NSString *Clousertype;
@property (nonatomic, strong) NSString *ClouserMessage;
@property (nonatomic, strong) NSString *MeetingNumber;

@end
