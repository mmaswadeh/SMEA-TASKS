//
//  Dashboard.m
//  MCI-Tasks
//
//  Created by Blessed Tree IT on 1/14/18.
//  Copyright © 2018 BTIT. All rights reserved.
//

#import "Dashboard.h"
#import "AVHexColor.h"
@implementation Dashboard

-(instancetype)initWithAttributes:(NSDictionary *)attributes
{
    if (self = [super initWithAttributes:attributes])
        
    {
    
        self.StatusLkpId       = [self extractIntegerValue:attributes[@"StatusLkpId"]];
        self.Status            = [self extractStatusName:[self extractIntegerValue:attributes[@"StatusLkpId"]]];
        self.agencyId          = [self extractIntegerValue:attributes[@"AgencyId"]];
        self.agencyName        = [self extractStringValue:attributes[@"AgencyName"]];
        self.taskStatuses      = [self extractTaskStatusesList:attributes[@"TaskStatuses"]];
        self.Count             = [self extractIntegerValue:attributes[@"Count"]];
        self.StatusColor       = [self extractStringValue:attributes[@"StatusColor"]];
        self.colorIs           = [self statusColor:self.StatusLkpId];
        [self checkStatusName];
        
    }
    
    return self;
    
}

-(NSString *)extractStatusName:(NSInteger)status {
    
    NSString *statusName;
    if (status == 651) {
        
        statusName = NSLocalizedString(@"New", nil);
    }
    
    else if (status == 652) {
        
        statusName = NSLocalizedString(@"open", nil);
        
    }
    
    else if (status == 653) {
        
        statusName = NSLocalizedString(@"Completed", nil);
        
        
    }
    else if (status == 654){
        
        statusName = NSLocalizedString(@"Closed and considered as Goal", nil);
        
    }
    else if (status == 655){
        
        statusName = NSLocalizedString(@"Closed and considered as Program", nil);
        
    }
    
    else if (status == 656){
        
        statusName = NSLocalizedString(@"Closed and considered as Initiative", nil);
        
    }
    else if (status == 657){
        
        statusName = NSLocalizedString(@"Closed and considered as Project", nil);
        
    }
    else if (status == 658){
        
        statusName = NSLocalizedString(@"Closed and considered as Case Study", nil);
        
    }
    
    else if (status == 659){
        
        statusName = NSLocalizedString(@"canceled", nil);
        
    }
    
    
    return statusName;
}


- (UIColor*)statusColor:(taskStatus)status {
    return [[ColorHelper sharedManager] getColorOfTasksWithStatus:status];
}

-(void)checkStatusName
{
    if (self.StatusLkpId == 610) {
        
        self.statusText = NSLocalizedString(@"NotSet", nil);
    }
    
    else if (self.StatusLkpId == 611) {
        
        self.statusText = NSLocalizedString(@"OnTrack", nil);
        
    }
    
    else if (self.StatusLkpId == 612) {
        
        self.statusText = NSLocalizedString(@"Late", nil);
        
        
    }
    else if (self.StatusLkpId == 613){
        
        self.statusText = NSLocalizedString(@"Critical", nil);
        
    }
    else if (self.StatusLkpId == 614){
        
        self.statusText = NSLocalizedString(@"Completed", nil);
        
    }
    
    else if (self.StatusLkpId == 649){
        
        self.statusText = NSLocalizedString(@"NewExpectedDate", nil);
        
    }
    
}

-(NSMutableArray * )extractTaskStatusesList:(NSArray *)TaskStatusesArr{
    NSMutableArray *taskStatuses =[NSMutableArray new];
    for (NSDictionary *taskStatusesDic in TaskStatusesArr) {
        
        Dashboard * items =[[Dashboard alloc]initWithAttributes:taskStatusesDic];
        [taskStatuses addObject:items];
    }
    return taskStatuses;
    
}

@end
