//
//  loginRequest.h
//  PMO Dashboard
//
//  Created by Blessed Tree IT on 2/20/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "NetworkOperationManager.h"
#import "Constants.h"
#import "profileInfo.h"
@interface loginRequest : NSObject


+(void)postLoginUsernamewith:(NSString *)username Passwordwith:(NSString *)password SerialNumberwith:(NSString *)serialNumber plateformwith:(NSInteger)ios notificationTokenwith:(NSString *)tokendevice languagesIdwith:(NSInteger)languageId success:(void(^)(NSString  *emailStr,NSString * surenameStr,NSString * isSuccessStr,NSString*errorStr,NSInteger * userIdIn ,NSString *loginToken))success failure:(void (^)(NSError *))failure;


+(void)newLoginWith:(NSString *)username Passwordwith:(NSString *)password Grantwith:(NSString *)grant success:(void(^)(NSString  *emailStr,NSString * surenameStr,NSString * isSuccessStr,NSString*errorStr,NSString* userIdIn,NSString *accessToken ,NSString *tokenType))success failure:(void (^)(NSError *))failure;

@end
