 //
//  loginRequest.m
//  PMO Dashboard
//
//  Created by Blessed Tree IT on 2/20/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import "loginRequest.h"

@implementation loginRequest


+(void)postLoginUsernamewith:(NSString *)username Passwordwith:(NSString *)password SerialNumberwith:(NSString *)serialNumber plateformwith:(NSInteger)ios notificationTokenwith:(NSString *)tokendevice languagesIdwith:(NSInteger)languageId success:(void (^)(NSString  *emailStr,NSString * surenameStr,NSString * isSuccessStr,NSString*errorStr,NSInteger * userIdIn,NSString *loginToken))success failure:(void (^)(NSError *))failure{
    
    NSString *path = POST_LOGIN_USER;
    NSDictionary *paramslogin =[[NSDictionary alloc]init];
    
    
    paramslogin= @{@"username": username,
                   @"password" : password ,
                   @"grant_type":serialNumber};
    
    
    
    NSLog(@"data :%@",paramslogin);
    [[NetworkOperationManager sharedObjectLogin]POST:path
                                     parameters: paramslogin
                                       progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                                           
                                               NSDictionary *response = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
                                               if(success) {
                                                   
                                                   //token
                                                   NSString *loginToken=(NSString*)response[@"access_token"];
                                                   NSString * sureName=(NSString *)response[@"surname"];
                                                   NSString * useerId=(NSString *)response[@"id"];
                                                   NSString * email=(NSString *)response[@"email"];
                                                   NSString * IsSuccess=(NSString *)response[@"IsSuccess"];
                                                   NSString * error=(NSString *)response[@"error"];
                                                   NSUserDefaults*loginTokenSaved=[NSUserDefaults standardUserDefaults];
                                                   NSNumber * userIDNum=[NSNumber numberWithInteger:[useerId integerValue]];
                                                   NSInteger idIs=[useerId integerValue];
                                                    [loginTokenSaved setObject:loginToken forKey:@"token"];
                                                    [loginTokenSaved setObject:userIDNum forKey:@"UserId"];
                                                   
                                               
                                                   
                                                
                                                   success(sureName,email,IsSuccess,error,&idIs,loginToken);
                                               
                                           }
                                           
                                       }
                                        failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                           if(failure) {
                                               failure(error);
                                               
                                               NSLog(@"error = %@",error);
                                           }
                                           
                                       }];
    
}

+(void)newLoginWith:(NSString *)username Passwordwith:(NSString *)password Grantwith:(NSString *)grant success:(void(^)(NSString  *emailStr,NSString * surenameStr,NSString * isSuccessStr,NSString*errorStr,NSString *userIdIn,NSString *accessToken ,NSString *tokenType))success failure:(void (^)(NSError *))failure
{
    NSString *path = POST_LOGIN_USER;
    
    NSString *granttype=@"password";
    


    NSDictionary* paramslogin= @{
                   @"username": username,
                   @"password" : password,@"grant_type":granttype
                   };
    
   [[NetworkOperationManager sharedObjectLogin]POST:path parameters:paramslogin progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {

        NSDictionary *response = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        if(success) {

            //token
            NSString *accessToken=(NSString*)response[@"access_token"];
            NSString *tokenType=(NSString*)response[@"token_type"];

            NSString *fullToken = [NSString stringWithFormat:@"%@ %@",tokenType ,accessToken];
            NSUserDefaults*loginTokenSaved=[NSUserDefaults standardUserDefaults];
            [loginTokenSaved setObject:fullToken forKey:@"token"];
            NSString * sureName=(NSString *)response[@"surname"];
            NSString * useerId=(NSString *)response[@"id"];
            NSInteger loginUserId = (NSInteger)response[@"id"];
            NSString * email=(NSString *)response[@"email"];
            NSString * IsSuccess=(NSString *)response[@"IsSuccess"];
            NSString * error=(NSString *)response[@"error"];
            NSNumber * userIDNum=[NSNumber numberWithInteger:[useerId integerValue]];
            NSInteger idIs=[useerId integerValue];
         
            [loginTokenSaved setObject:userIDNum forKey:@"UserId"];
            [loginTokenSaved setObject:sureName forKey:@"FirstName"];

            success(sureName,email,IsSuccess,error,useerId,accessToken,tokenType);
        //    success(accessToken,tokenType);
        }

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {

        if(failure) {
            failure(error);

            NSLog(@"error = %@",error);
        }

    }];
//
}

@end
