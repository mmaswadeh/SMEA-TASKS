//
//  TasksResquest.m
//  VRO-BTIT
//
//  Created by Mohammad Maswadeh on 9/28/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import "TasksResquest.h"
#import "Tasks.h"
#import "Users.h"
#import "Closure.h"

@implementation TasksResquest

NSURL *thePath;
NSString *theFile;

+(void)getTasksWithSuccess:(void (^)(NSMutableArray *tasksList))success failure:(void (^)(NSError *))failure
{
    NSString *path = GET_TASKS;
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params = nil;
    
    
    [[NetworkOperationManager sharedObjectTasks]GET:path parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSDictionary *data=[NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        
        if (success)
        {
            NSMutableArray *tasksData=[NSMutableArray new];
            NSArray *arrTasks = (NSArray *)data[@"Tasks"];
            for (NSDictionary *tasksDict in arrTasks)
            {
                
                
                Tasks *TasksObj=[[Tasks alloc]initWithAttributes:tasksDict];
                [tasksData addObject:TasksObj];
                
            }
            
            success(tasksData);
            
            
        }
        
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        if (failure)
            
            failure([[ErrorHandler sharedHandler] handleHTTPResponseWithError:error domain:@"GetTasksList"]);
        
    }];
    
}
+(void)getAgenciesUserlistWithSuccess:(void (^)(NSMutableArray *usersList,NSMutableArray *usersID))success failure:(void (^)(NSError *))failure{
    NSString *path = GET_AGGENCIES;
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params = nil;
    
    [[NetworkOperationManager sharedObjectTasks]GET:path parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSDictionary *data=[NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        
        if (success)
        {
            NSMutableArray *usersData=[NSMutableArray new];
            NSMutableArray *usersIds=[NSMutableArray new];
            
            NSArray *arrUsers = (NSArray *)data[@"Agencies"];
            for (NSDictionary *usersDict in arrUsers)
            {
                
                
                Users *UsersObj=[[Users alloc]initWithAttributes:usersDict];
                [usersData addObject:UsersObj.name];
                [usersIds addObject:@(UsersObj.agencyId
                 )];
                
                
            }
            
            success(usersData,usersIds);
            
            
        }
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        if (failure)
            
            failure([[ErrorHandler sharedHandler] handleHTTPResponseWithError:error domain:@"GetUsersList"]);
        
        
    }];
    
    
    
}

+(void)getSourceslistWithSuccess:(void (^)(NSMutableArray *sourceList,NSMutableArray *sourceID))success failure:(void (^)(NSError *))failure {
    
    NSString *path = GET_TASK_SOURCES;
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params = nil;
    
    [[NetworkOperationManager sharedObjectTasks]GET:path parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSDictionary *data=[NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        
        if (success)
        {
            NSMutableArray *sourcesData=[NSMutableArray new];
            NSMutableArray *sourcesIds=[NSMutableArray new];
            
            NSArray *arrSources = (NSArray *)data[@"TaskSources"];
            for (NSDictionary *sourcesDict in arrSources)
            {
                
                
                Users *UsersObj=[[Users alloc]initWithAttributes:sourcesDict];
                [sourcesData addObject:UsersObj.name];
                [sourcesIds addObject:@(UsersObj.agencyId)];
                
                
            }
            
            success(sourcesData,sourcesIds);
            
            
        }
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        if (failure)
            
            failure([[ErrorHandler sharedHandler] handleHTTPResponseWithError:error domain:@"GetSourcesList"]);
        
        
    }];
    
    
}
+(void)getAssigneeUserlistWithSuccess:(void (^)(NSMutableArray *usersList,NSMutableArray *usersID))success failure:(void (^)(NSError *))failure
{
    NSString *path = GET_ASSIGNEE;
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params = nil;
    
    [[NetworkOperationManager sharedObjectTasks]GET:path parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSDictionary *data=[NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        
        if (success)
        {
            NSMutableArray *usersData=[NSMutableArray new];
            NSMutableArray *usersIds=[NSMutableArray new];
            
            NSArray *arrUsers = (NSArray *)data[@"Users"];
            for (NSDictionary *usersDict in arrUsers)
            {
                
                
                Users *UsersObj=[[Users alloc]initWithAttributes:usersDict];
                [usersData addObject:UsersObj.name];
                [usersIds addObject:@(UsersObj.userId)];
                
                
            }
            
            success(usersData,usersIds);
            
            
        }
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        if (failure)
            
            failure([[ErrorHandler sharedHandler] handleHTTPResponseWithError:error domain:@"GetUsersList"]);
        
        
    }];
    
    
    
}

+(void)deleteTasksWithId: (NSInteger)taskId success:(void (^)(NSDictionary *comments))success failure:(void (^)(NSError *error))failure
{
    NSString *path = DELETE_TASK;
    NSDictionary *params = [[NSMutableDictionary alloc] init];
    params = nil;
    
    params = @{@"id" : @(taskId)};
    
    
    
    [[NetworkOperationManager sharedObjectTasks]GET:path parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSDictionary *data=[NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        
        if (success)
        {
            success(data);
            
        }
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        if (failure)
            
            failure([[ErrorHandler sharedHandler] handleHTTPResponseWithError:error domain:@"GetUsersList"]);
        NSLog(@"Error = %@",error);
        
        
    }];
    
    
    
    
    
}
+(void)addTaskWithComment:(NSString*)comment andTitle:(NSString*)title andAssigneeid:(NSInteger)assigneId andTaskId:(NSInteger)taskId andImagePath:(NSString *)imagePath andImageData:(NSData*)imageData andStartingDate:(NSString *)startDate andEndingDate:(NSString *)endDate andAgencyid:(NSInteger)agencyId andUserid:(NSInteger)userId andSourceId:(NSInteger)sourceId andMeetingNumber:(NSInteger)meetingNumber success:(void (^)(NSDictionary *comments))success failure:(void (^)(NSError *error))failure {
    
    NSString *path = ADD_TASK;
    thePath = [NSURL URLWithString:imagePath];
    theFile = [thePath path];
    
    if ([startDate isEqualToString:@"No Starting Date"]) {
        
        startDate = @"";
        
    }
    
    if ([endDate isEqualToString:@"No Ending Date"]) {
        
        endDate = @"";
        
    }
    NSDictionary *params = [[NSMutableDictionary alloc] init];
    /*TaskId:104
     Comments:yes correct
     Title:sojoudUp
     //TaskStatusLkpId:
     StartingDate:31/11/2018
     EndingDate:28/12/2018
     AssigneeUserId:3
     AgencyId:2
     userId:1*/
    
    if (agencyId ==0) {
        params = @{@"Comments" :comment,
                   @"Title" : title,
                   @"TaskId":@(taskId),
                   @"AssigneeUserId" : @(assigneId),
                   @"StartingDate" : startDate,
                   @"EndingDate" : endDate,
                   @"userId":@(userId),
                   @"TaskSourceId":@(sourceId),
                   @"MeetingNumber":@(meetingNumber)};
    }
    else{
    params = @{@"Comments" :comment,
               @"Title" : title,
               @"TaskId":@(taskId),
               @"AssigneeUserId" : @(assigneId),
               @"AgencyId" : @(agencyId),
               @"StartingDate" : startDate,
               @"EndingDate" : endDate,
               @"userId":@(userId),
               @"TaskSourceId":@(sourceId),
               @"MeetingNumber":@(meetingNumber)};
    }
    
    NSLog(@"params %@",params);
    [[NetworkOperationManager sharedObjectTasks]POST:path parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {

            if (imageData) {
                [formData appendPartWithFileData:imageData name:@"ScreenShot" fileName:@"photo.jpg" mimeType:@"image/jpeg"];
                
        }
        
        
    }
     
    progress:nil
     
    success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
     
     {
         
         NSDictionary *data=[NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
         
         if (success)
         {
             NSLog(@"Data = %@",data);
             success(data);
             
         }
         else
             
             NSLog(@"not success %@",data);
     }
     
    failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error)
     
     {
         if (failure)
             
             failure([[ErrorHandler sharedHandler] handleHTTPResponseWithError:error domain:@"Add Task"]);
         
     }];
}

+(void)editProgressAndClosureWithTaskId:(NSInteger)taskId andProgress:(NSString*)progress andUserid:(NSInteger)userId andClosureType:(NSString *)closureType andTranferTargetName:(NSString *)tranferTargetName success:(void (^)(NSDictionary *response))success failure:(void (^)(NSError *error))failure {

    NSString *path = SAVE_MY_TASK;
    NSDictionary *params = [[NSMutableDictionary alloc] init];
    
    
    params = @{@"TaskId" :@(taskId),
               @"Progress" : progress,
               @"userId" : @(userId),
               @"ClosureType" : closureType,
               @"TranferTargetName" : tranferTargetName};
    
    [[NetworkOperationManager sharedObjectTasks]POST:path parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSDictionary *response = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        if(success) {
            success(response);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        if(failure) {
            failure(error);
            
            NSLog(@"error = %@",error);
        }
        
    }];
}

+(void)saveTaskDelayWithTaskId:(NSInteger)taskId andJustification:(NSString*)justification andUserid:(NSInteger)userId andNewEndDate:(NSString *)newEndDate success:(void (^)(NSDictionary *response))success failure:(void (^)(NSError *error))failure {
    
    NSString *path = SAVE_TASK_DELAY;
    NSDictionary *params = [[NSMutableDictionary alloc] init];
    params = @{@"TaskId" :@(taskId),
               @"Justification" : justification,
               @"userId" : @(userId),
               @"NewEndDate" : newEndDate};
    
    [[NetworkOperationManager sharedObjectTasks]POST:path parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSDictionary *response = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        if(success) {
            success(response);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        if(failure) {
            failure(error);
            
            NSLog(@"error = %@",error);
        }
        
    }];
}

+(void)updateTaskDelayWithTaskId:(NSInteger)taskId andJustification:(NSString*)justification andUserid:(NSInteger)userId success:(void (^)(NSDictionary *response))success failure:(void (^)(NSError *error))failure {
    
    NSString *path = UPDATE_TASK_DELAY;
    NSDictionary *params = [[NSMutableDictionary alloc] init];
    params = @{@"Id" :@(taskId),
               @"Justification" : justification,
               @"userId" : @(userId)};
    
    [[NetworkOperationManager sharedObjectTasks]POST:path parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSDictionary *response = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        if(success) {
            success(response);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        if(failure) {
            failure(error);
            
            NSLog(@"error = %@",error);
        }
        
    }];
}

+(void)getAllTasksWithUserId:(NSInteger)userId Success:(void (^)(NSMutableArray *assinedByMe ,NSMutableArray *assinedToMe))success failure:(void (^)(NSError *))failure {

    NSString *path = MY_SWIFT_TASK;
    NSDictionary *params = [[NSDictionary alloc] init];
    params = @{@"id" :@(userId)};
    
    
    [[NetworkOperationManager sharedObjectTasks]GET:path parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSDictionary *data=[NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        
        if (success)
        {
            NSMutableArray *byMeData=[NSMutableArray new];
            NSArray *arrByMe = (NSArray *)data[@"AssinedByMeTasks"];
            for (NSDictionary *byMeDict in arrByMe)
            {
                
                
                Tasks *TasksObj=[[Tasks alloc]initWithAttributes:byMeDict];
                [byMeData addObject:TasksObj];
                
            }
            
            NSMutableArray *toMeData=[NSMutableArray new];
            NSArray *arrToMe = (NSArray *)data[@"AssinedToMeTasks"];
            for (NSDictionary *toMeDict in arrToMe)
            {
                
                
                Tasks *TasksOb=[[Tasks alloc]initWithAttributes:toMeDict];
                [toMeData addObject:TasksOb];
               
            }
            
            success(byMeData,toMeData);
   
        }
        

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        if (failure)
            
            failure([[ErrorHandler sharedHandler] handleHTTPResponseWithError:error domain:@"GetTasksList"]);
        
    }];
    
}
+(void)getClosureTypesWithSuccess:(void (^)(NSMutableArray *closureNames, NSMutableArray *closueIds))success failure:(void (^)(NSError *))failure {

    NSString *path = GET_CLOSURE_TYPES;
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params = nil;
    
    
    [[NetworkOperationManager sharedObjectTasks]GET:path parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSDictionary *data=[NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        
        if (success)
        {
            NSMutableArray *closureData=[NSMutableArray new];
            NSMutableArray *closureIds=[NSMutableArray new];
            
            NSArray *arrUsers = (NSArray *)data[@"Agencies"];
            for (NSDictionary *usersDict in arrUsers)
            {
                
                
                Closure *closureObj=[[Closure alloc]initWithAttributes:usersDict];
                [closureData addObject:closureObj.closureName];
                [closureIds addObject:@(closureObj.closureId)];
                
                
            }
            
            success(closureData,closureIds);

        }
        


    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        if (failure)
            
            failure([[ErrorHandler sharedHandler] handleHTTPResponseWithError:error domain:@"GetTasksList"]);
        
    }];
}

+(void)deleteDelayWithId: (NSInteger)delayId success:(void (^)(NSDictionary *response))success failure:(void (^)(NSError *error))failure {
    
    NSString *path = DELETE_TASK_DELAY;
    NSDictionary *params = [[NSDictionary alloc] init];
    params = @{@"id" :@(delayId)};

    
    [[NetworkOperationManager sharedObjectTasks]GET:path parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSDictionary *data=[NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        
        if (success)
        {
      
            
            success(data);
            
        }
        
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        if (failure)
            
            failure([[ErrorHandler sharedHandler] handleHTTPResponseWithError:error domain:@"GetTasksList"]);
        
    }];
}

+(void)deleteTaskImageWithId: (NSInteger)taskId success:(void (^)(NSDictionary *comments))success failure:(void (^)(NSError *error))failure {
    
    NSString *path = DELETE_TASK_IMAGE;
    NSDictionary *params = [[NSMutableDictionary alloc] init];
    params = nil;
    
    params = @{@"id" : @(taskId)};
    
    
    
    [[NetworkOperationManager sharedObjectTasks]GET:path parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSDictionary *data=[NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        
        if (success)
        {
            success(data);
            
        }
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        if (failure)
            
            failure([[ErrorHandler sharedHandler] handleHTTPResponseWithError:error domain:@"DeleteImage"]);
        NSLog(@"Error = %@",error);
        
        
    }];
    
}
@end

