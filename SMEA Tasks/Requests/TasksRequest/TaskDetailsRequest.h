//
//  TaskDetailsRequest.h
//  VRO-BTIT
//
//  Created by Blessed Tree IT on 6/13/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NetworkOperationManager.h"
#import "Constants.h"
#import "Tasks.h"
@interface TaskDetailsRequest : NSObject
+(void)getTaskDetailsWithID:(NSInteger)TaskID sucess:(void(^)(Tasks*Tasks))success  failure:(void (^)(NSError *))failure;
@end
