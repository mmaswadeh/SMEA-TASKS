//
//  TaskDetailsRequest.m
//  VRO-BTIT
//
//  Created by Blessed Tree IT on 6/13/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import "TaskDetailsRequest.h"

@implementation TaskDetailsRequest
+(void)getTaskDetailsWithID:(NSInteger)TaskID sucess:(void(^)(Tasks*Tasks))success  failure:(void (^)(NSError *))failure{
    NSString *path = GET_TASKS_DETAILS;
    
    NSDictionary *params = @{@"id" : @(TaskID)};
    
    
    [[NetworkOperationManager sharedObjectTasks] GET:path
                                     parameters:params
                                       progress:nil
                                        success:^(NSURLSessionDataTask * _Nonnull sessionTask, id  _Nullable responseObject) {
                                            
                                            
                                            NSDictionary *data=[NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
                                            
                                            
                                            if (success)
                                            {
                                                
                                                
                                                
                                                // Tasks
                                                
                                        
                                                NSDictionary *arrtasks = (NSDictionary *)data[@"Task"];
                                              
                                                    Tasks *tasksObj=[[Tasks alloc]initWithAttributes:arrtasks];
                                               
                                                
                                                
                                                success(tasksObj);
                                                
                                            }
                                            
                                            
                                            
                                            
                                            
                                        }
     
                                        failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                            
                                            if (failure)
                                                
                                                failure([[ErrorHandler sharedHandler] handleHTTPResponseWithError:error domain:@"GetTasksDetails"]);
                                            
                                        }];
    
}

@end
