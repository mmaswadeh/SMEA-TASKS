//
//  TasksResquest.h
//  VRO-BTIT
//
//  Created by Mohammad Maswadeh on 9/28/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"
#import "NetworkOperationManager.h"

@interface TasksResquest : NSObject

+(void)getTasksWithSuccess:(void (^)(NSMutableArray *tasksList))success failure:(void (^)(NSError *))failure;
+(void)getAssigneeUserlistWithSuccess:(void (^)(NSMutableArray *usersList,NSMutableArray *usersID))success failure:(void (^)(NSError *))failure;

+(void)getSourceslistWithSuccess:(void (^)(NSMutableArray *sourceList,NSMutableArray *sourceID))success failure:(void (^)(NSError *))failure;


+(void)getAgenciesUserlistWithSuccess:(void (^)(NSMutableArray *usersList,NSMutableArray *usersID))success failure:(void (^)(NSError *))failure;


+(void)addTaskWithComment:(NSString*)comment andTitle:(NSString*)title andAssigneeid:(NSInteger)assigneId andTaskId:(NSInteger)taskId andImagePath:(NSString *)imagePath andImageData:(NSData*)imageData andStartingDate:(NSString *)startDate andEndingDate:(NSString *)endDate andAgencyid:(NSInteger)agencyId andUserid:(NSInteger)userId andSourceId:(NSInteger)sourceId andMeetingNumber:(NSInteger)meetingNumber success:(void (^)(NSDictionary *comments))success failure:(void (^)(NSError *error))failure;


+(void)deleteTasksWithId: (NSInteger)taskId success:(void (^)(NSDictionary *comments))success failure:(void (^)(NSError *error))failure;

+(void)editProgressAndClosureWithTaskId:(NSInteger)taskId andProgress:(NSString*)progress andUserid:(NSInteger)userId andClosureType:(NSString *)closureType andTranferTargetName:(NSString *)tranferTargetName success:(void (^)(NSDictionary *response))success failure:(void (^)(NSError *error))failure;

+(void)saveTaskDelayWithTaskId:(NSInteger)taskId andJustification:(NSString*)justification andUserid:(NSInteger)userId andNewEndDate:(NSString* )newEndDate success:(void (^)(NSDictionary *response))success failure:(void (^)(NSError *error))failure;

+(void)updateTaskDelayWithTaskId:(NSInteger)taskId andJustification:(NSString*)justification andUserid:(NSInteger)userId success:(void (^)(NSDictionary *response))success failure:(void (^)(NSError *error))failure;

+(void)getAllTasksWithUserId:(NSInteger)userId Success:(void (^)(NSMutableArray *assinedByMe ,NSMutableArray *assinedToMe))success failure:(void (^)(NSError *))failure;



+(void)getClosureTypesWithSuccess:(void (^)(NSMutableArray *closureNames, NSMutableArray *closueIds))success failure:(void (^)(NSError *))failure;

+(void)deleteDelayWithId: (NSInteger)delayId success:(void (^)(NSDictionary *response))success failure:(void (^)(NSError *error))failure;

+(void)deleteTaskImageWithId: (NSInteger)taskId success:(void (^)(NSDictionary *comments))success failure:(void (^)(NSError *error))failure;

@end
