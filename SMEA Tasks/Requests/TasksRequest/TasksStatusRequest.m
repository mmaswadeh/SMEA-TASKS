//
//  TasksStatusRequest.m
//  MCI-Tasks
//
//  Created by Blessed Tree IT on 1/14/18.
//  Copyright © 2018 BTIT. All rights reserved.
//

#import "TasksStatusRequest.h"
#import "Dashboard.h"

@implementation TasksStatusRequest


+(void)getTasksDashboardWithUserId:(NSInteger)userId Success:(void(^)(NSMutableArray * toMeChartData,NSMutableArray * byMeChartData, NSMutableArray *toMeStastus , NSMutableArray *byMeStatus,NSMutableArray *ProgressStatusByAgencyToMe,NSMutableArray *ProgressStatusByAgencyByMe,NSMutableArray *ProgressStatusBySourceByMe ,NSMutableArray *ProgressStatusBySourceToMe))success failure:(void(^)(NSError * error))failure {
    NSString *path = GET_MY_TASK_STATUS;
    NSDictionary *params = [[NSMutableDictionary alloc] init];
    params = @{
               @"id" : @(userId)
              };
    
    [[NetworkOperationManager sharedObjectTasks] GET:path
                                     parameters:params
                                       progress:nil
                                        success:^(NSURLSessionDataTask * _Nonnull sessionTask, id  _Nullable responseObject) {
                                            
                                            
                                            NSDictionary *data=[NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
                                            
                                            
                                            if (success)
                                            {
                                                                                                
                                                // Tasks
                                                
                                                NSMutableArray *toMeTasksData=[NSMutableArray new];
                                                NSArray *arrtoMeTasks = (NSArray *)data[@"TasksToMeStatus"];
                                                for (NSDictionary *toMeTasksDict in arrtoMeTasks)
                                                {
                                                    Dashboard *tasksObj=[[Dashboard alloc]initWithAttributes:toMeTasksDict];
                                                    [toMeTasksData addObject:tasksObj];
                                                    
                                                }
                                                
                                                NSMutableArray *tasksData=[NSMutableArray new];
                                                NSArray *arrtasks = (NSArray *)data[@"TasksByMeStatus"];
                                                for (NSDictionary *tasksDict in arrtasks)
                                                {
                                                    Dashboard *tasksObj=[[Dashboard alloc]initWithAttributes:tasksDict];
                                                    [tasksData addObject:tasksObj];
                                                    
                                                }
                                                
                                                NSMutableArray *toMeStatusData=[NSMutableArray new];
                                                NSArray *arrToMeStatus = (NSArray *)data[@"PerformanceToMeStatus"];
                                                for (NSDictionary *toMeStatusDict in arrToMeStatus)
                                                {
                                                    Dashboard *ToMeStatusObj=[[Dashboard alloc]initWithAttributes:toMeStatusDict];
                                                    [toMeStatusData addObject:ToMeStatusObj];
                                                    
                                                }
                                                
                                                NSMutableArray *byMeStatusData=[NSMutableArray new];
                                                NSArray *arrByMeStatus = (NSArray *)data[@"PerformanceByMeStatus"];
                                                for (NSDictionary *byMeStatusDict in arrByMeStatus)
                                                {
                                                    Dashboard *byMeStatusObj=[[Dashboard alloc]initWithAttributes:byMeStatusDict];
                                                    [byMeStatusData addObject:byMeStatusObj];
                                                    
                                                }
                                                // by me Agency
                                                NSMutableArray *ProgressStatusByAgencyToMeData=[NSMutableArray new];
                                                NSArray *arrProgressStatusByAgencyToMe = (NSArray *)data[@"ProgressStatusByAgencyByMe"];
                                                for (NSDictionary *ProgressStatusByAgencyToMeDict in arrProgressStatusByAgencyToMe)
                                                {
                                                    Dashboard *ProgressStatusByAgencyByMeDataObj=[[Dashboard alloc]initWithAttributes:ProgressStatusByAgencyToMeDict];
                                                    [ProgressStatusByAgencyToMeData addObject:ProgressStatusByAgencyByMeDataObj];
                                                    
                                                }
                                                
                                                // to me Agency
                                                NSMutableArray *ProgressStatusByAgencyByMeData=[NSMutableArray new];
                                                NSArray *arrProgressStatusByAgencyByMe = (NSArray *)data[@"ProgressStatusByAgencyToMe"];
                                                for (NSDictionary *ProgressStatusByAgencyByMeDict in arrProgressStatusByAgencyByMe)
                                                {
                                                    Dashboard *ProgressStatusByAgencyByMeDataObj=[[Dashboard alloc]initWithAttributes:ProgressStatusByAgencyByMeDict];
                                                    [ProgressStatusByAgencyByMeData addObject:ProgressStatusByAgencyByMeDataObj];
                                                    
                                                }
                                                
                                                // to me Source
                                                NSMutableArray *ProgressStatusBySourceToMeData=[NSMutableArray new];
                                                NSArray *arrProgressStatusBySourceToMe = (NSArray *)data[@"ProgressStatusBySourceToMe"];
                                                for (NSDictionary *ProgressStatusBySourceToMeDict in arrProgressStatusBySourceToMe)
                                                {
                                                    Dashboard *ProgressStatusBySourceByMeDataObj=[[Dashboard alloc]initWithAttributes:ProgressStatusBySourceToMeDict];
                                                    [ProgressStatusBySourceToMeData addObject:ProgressStatusBySourceByMeDataObj];
                                                    
                                                }
                                                
                                                // by me Source
                                                NSMutableArray *ProgressStatusByASourceByMeData=[NSMutableArray new];
                                                NSArray *arrProgressStatusBySourceByMe = (NSArray *)data[@"ProgressStatusBySourceByMe"];
                                                for (NSDictionary *ProgressStatusBySourceByMeDict in arrProgressStatusBySourceByMe)
                                                {
                                                    Dashboard *ProgressStatusBySourceByMeDataObj=[[Dashboard alloc]initWithAttributes:ProgressStatusBySourceByMeDict];
                                                    [ProgressStatusByASourceByMeData addObject:ProgressStatusBySourceByMeDataObj];
                                                    
                                                }
                                                
                                                
                                                success(toMeTasksData,tasksData,toMeStatusData,byMeStatusData,ProgressStatusByAgencyToMeData,ProgressStatusByAgencyByMeData,ProgressStatusByASourceByMeData,ProgressStatusBySourceToMeData);
                                                
                                            }
      
                                            
                                        }
     
                                        failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                            
                                            if (failure)
                                                
                                                failure([[ErrorHandler sharedHandler] handleHTTPResponseWithError:error domain:@"GetTasksDetails"]);
                                            
                                        }];
    
}

+(void)getAllTasksDashboardWithUserId:(NSInteger)userId Success:(void(^)(NSMutableArray * taskStatus,NSMutableArray * performanceStatus, NSMutableArray *progressStatusByAgency))success failure:(void(^)(NSError * error))failure {
    
    NSString *path = GET_AGENCY_TASK_STATUS;
    NSDictionary *params = [[NSMutableDictionary alloc] init];
    params = @{
               @"id" : @(userId)
               };
    
    [[NetworkOperationManager sharedObjectTasks] GET:path
                                          parameters:params
                                            progress:nil
                                             success:^(NSURLSessionDataTask * _Nonnull sessionTask, id  _Nullable responseObject) {
                                                 
                                                 
                                                 NSDictionary *data=[NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
                                                 
                                                 
                                                 if (success)
                                                 {
                                                     
                                                     // Tasks
                                                     
                                                     NSMutableArray *agencyTaskStatus=[NSMutableArray new];
                                                     NSArray *arrtoMeTasks = (NSArray *)data[@"TaskStatus"];
                                                     for (NSDictionary *toMeTasksDict in arrtoMeTasks)
                                                     {
                                                         Dashboard *tasksObj=[[Dashboard alloc]initWithAttributes:toMeTasksDict];
                                                         [agencyTaskStatus addObject:tasksObj];
                                                         
                                                     }
                                                     
                                                     NSMutableArray *agencyPerformanceStatus=[NSMutableArray new];
                                                     NSArray *arrtasks = (NSArray *)data[@"PerformanceStatus"];
                                                     for (NSDictionary *tasksDict in arrtasks)
                                                     {
                                                         Dashboard *tasksObj=[[Dashboard alloc]initWithAttributes:tasksDict];
                                                         [agencyPerformanceStatus addObject:tasksObj];
                                                         
                                                     }
                                                     
                                                     NSMutableArray *agencyProgressStatus=[NSMutableArray new];
                                                     NSArray *arrToMeStatus = (NSArray *)data[@"ProgressStatusByAgency"];
                                                     for (NSDictionary *toMeStatusDict in arrToMeStatus)
                                                     {
                                                         Dashboard *ToMeStatusObj=[[Dashboard alloc]initWithAttributes:toMeStatusDict];
                                                         [agencyProgressStatus addObject:ToMeStatusObj];
                                                         
                                                     }
                                                     
                                                     success(agencyTaskStatus,agencyPerformanceStatus,agencyProgressStatus);
                                                     
                                                 }
                                                 
                                                 
                                             }
     
                                             failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                                 
                                                 if (failure)
                                                     
                                                     failure([[ErrorHandler sharedHandler] handleHTTPResponseWithError:error domain:@"GetTasksDetails"]);
                                                 
                                             }];

    
}
@end
