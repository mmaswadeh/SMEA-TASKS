//
//  TasksStatusRequest.h
//  MCI-Tasks
//
//  Created by Blessed Tree IT on 1/14/18.
//  Copyright © 2018 BTIT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NetworkOperationManager.h"
#import "Constants.h"
@interface TasksStatusRequest : NSObject
//x+(void)getTaskDetailsWithID:(NSInteger)TaskID sucess:(void(^)(NSMutableArray*Tasks))success  failure:(void (^)(NSError *))failure;

+(void)getTasksDashboardWithUserId:(NSInteger)userId Success:(void(^)(NSMutableArray * toMeChartData,NSMutableArray * byMeChartData, NSMutableArray *toMeStastus , NSMutableArray *byMeStatus,NSMutableArray *ProgressStatusByAgencyToMe,NSMutableArray *ProgressStatusByAgencyByMe, NSMutableArray *ProgressStatusBySourceByMe ,NSMutableArray *ProgressStatusBySourceToMe))success failure:(void(^)(NSError * error))failure;

+(void)getAllTasksDashboardWithUserId:(NSInteger)userId Success:(void(^)(NSMutableArray * taskStatus,NSMutableArray * performanceStatus, NSMutableArray *progressStatusByAgency))success failure:(void(^)(NSError * error))failure;


@end
