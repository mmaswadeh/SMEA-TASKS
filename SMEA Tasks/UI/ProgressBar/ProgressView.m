//
//  ProgressView.m
//  MOH-BTIT-IOS-1
//
//  Created by Blessed Tree IT on 11/29/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import "ProgressView.h"

@implementation ProgressView
+(id)customView{
    ProgressView *customView = [[[NSBundle mainBundle] loadNibNamed:@"ProgressView" owner:nil options:nil] lastObject];
    
    // make sure customView is not nil or the wrong class!
    if ([customView isKindOfClass:[ProgressView class]])
    {
        
   [customView.progressBar setTransform:CGAffineTransformMakeScale(1.0, 2.0)];
        
        return customView;
        
        
    }
    else
        return nil;
}
@end
