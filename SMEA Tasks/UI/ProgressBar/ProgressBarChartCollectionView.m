//
//  ProgressBarChartCollectionView.m
//  MOH-BTIT-IOS-1
//
//  Created by Blessed Tree IT on 11/29/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import "ProgressBarChartCollectionView.h"
#import "ProgressView.h"

#import "ProgressBarCell.h"
#import "StrategicObjectives.h"
#import "DetailsCollectionViewCell.h"
#import "chartData.h"
#import "legendHelper.h"
#import "PieChartVIew.h"
#import "DynamicLegend.h"
#import "Dashboard.h"
#import "MainViewController.h"


#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))
#define IS_ZOOMED (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)

#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)
#define IS_IPHONE_X (IS_IPHONE && SCREEN_MAX_LENGTH == 812.0)
@interface ProgressBarChartCollectionView(){
    
    
    NSArray *progressDataArr;
    NSArray *pieDataArr;
    
}
@end

@implementation ProgressBarChartCollectionView
+(id)createProgessStackView:(NSArray *)dataItems withTotla:(NSInteger) total{
    
    
    ProgressBarChartCollectionView *customView = [[[NSBundle mainBundle] loadNibNamed:@"ProgressBarChartCollectionView" owner:nil options:nil] lastObject];
    
    // make sure customView is not nil or the wrong class!
    if ([customView isKindOfClass:[ProgressBarChartCollectionView class]])
    {
        
        customView.containerViewProgress.hidden=false;
        [customView drawPrgressData:dataItems withTotal:total];
        
        return customView;
        
        
    }
    else
        return nil;
    
}
+ (id)createCollectionWith:(NSArray *) data{
    
    
    ProgressBarChartCollectionView *customView = [[[NSBundle mainBundle] loadNibNamed:@"ProgressBarChartCollectionView" owner:nil options:nil] lastObject];
    
    // make sure customView is not nil or the wrong class!
    if ([customView isKindOfClass:[ProgressBarChartCollectionView class]])
    {
        customView.SwitchChartSegment.selectedSegmentIndex=0;
        customView.dataArr=[NSArray arrayWithArray:data];
        
          customView.containerViewProgress.hidden=true;
        [customView customCollectionPie:data];
        return customView;
        
        
    }
    else
        return nil;
}
+ (id)createCollectionWithoutButton:(NSArray *) data{
  
    ProgressBarChartCollectionView *customView = [[[NSBundle mainBundle] loadNibNamed:@"ProgressBarChartCollectionView" owner:nil options:nil] lastObject];
    // make sure customView is not nil or the wrong class!
    if ([customView isKindOfClass:[ProgressBarChartCollectionView class]]) {
        
        customView.dataArr=[NSArray arrayWithArray:data];
        customView.containerViewProgress.hidden=true;
        customView.SwitchChartSegment.selectedSegmentIndex=0;

        [customView customCollectionwithoutButton:data];
        return customView;
    }
    else
        return nil;
}
-(void)drawPrgressData:(NSArray *)data withTotal:(NSInteger)total {
    
    _leftArrowImage.hidden = YES;
    _rightArrowImage.hidden = YES;
    for (int x=0; x<data.count; x++) {
        chartData * obj=[data objectAtIndex:x];
        
        ProgressView * progressBar =[ProgressView customView];
        // progressBar.progressBar.progress=obj.count;
        
        if (obj.count) {
            
            float x = (float)obj.count/(float)total;
            NSLog(@"float = %f",x);
            progressBar.progressBar.progress=x;
            
        }
        
        else
        {
            progressBar.progressBar.progress=obj.count;
            
        }
        progressBar.progressBar.progressTintColor=obj.phaseColor;
        progressBar.title.text=obj.statusName ;
        progressBar.amount.text=[NSString stringWithFormat:@"%ld",obj.count];
        [self.ProgressStackView addArrangedSubview:progressBar];
        
    }
}
-(void)customCollectionwithoutButton : (NSArray *)items {

    NSString * language = [[NSString alloc]init];
    
    _languageSettings = [[languageHelper alloc]init];
    language= [_languageSettings checkLanguages];
    if ([language isEqualToString:@"ar"]) {
        _rightArrowImage.image = [UIImage imageNamed:@"leftArrow"];
        _leftArrowImage.image = [UIImage imageNamed:@"rightArrow"];
    }
    
    else{
        
    }
    
    self.allCollection.tag=2;
    [self.allCollection registerClass:[ProgressBarCell class] forCellWithReuseIdentifier:@"DetailsCollectionViewCell"];
    self.allCollection.delegate = self;
    self.allCollection.dataSource = self;
    [self.allCollection reloadData];
}

-(void)customCollection : (NSArray *)items {
    
    NSString * language = [[NSString alloc]init];
    
    _languageSettings = [[languageHelper alloc]init];
    language= [_languageSettings checkLanguages];
    if ([language isEqualToString:@"ar"]) {
        _rightArrowImage.image = [UIImage imageNamed:@"leftArrow"];
        _leftArrowImage.image = [UIImage imageNamed:@"rightArrow"];
    }
    
    else{
        
    }
        
    
    self.allCollection.tag=0;
    [self.allCollection registerClass:[ProgressBarCell class] forCellWithReuseIdentifier:@"ProgressBarCell"];
    self.allCollection.delegate = self;
    self.allCollection.dataSource = self;
    [self.allCollection reloadData];
    
    
}

-(void)customCollectionPie : (NSArray *)items {
    
    NSString * language = [[NSString alloc]init];
    
    _languageSettings = [[languageHelper alloc]init];
    language= [_languageSettings checkLanguages];
    if ([language isEqualToString:@"ar"]) {
        _rightArrowImage.image = [UIImage imageNamed:@"leftArrow"];
        _leftArrowImage.image = [UIImage imageNamed:@"rightArrow"];
    }
    
    else{
        
    }
    
    
    self.allCollection.tag=2;
    [self.allCollection registerClass:[DetailsCollectionViewCell class] forCellWithReuseIdentifier:@"DetailsCollectionViewCell"];
    self.allCollection.delegate = self;
    self.allCollection.dataSource = self;
    [self.allCollection reloadData];
    
    
}
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    
    return  1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataArr.count;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *maincell;

    if (self.allCollection.tag ==0) {
        
    UICollectionViewCell* cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"ProgressBarCell" forIndexPath:indexPath];
     StrategicObjectives *entityObj=[self.dataArr objectAtIndex:indexPath.row];
       // ProgressBarCell  * view=[ProgressBarCell customView:entityObj.chart_data];
        ProgressBarCell  * view=[ProgressBarCell customView:entityObj.chart_data total:entityObj.total];
        view.nameOfEntity.text=entityObj.name;
        view.detailsBtn.tag = indexPath.row;
        [view.detailsBtn addTarget:self action:@selector(collectionViewCellButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
        [cell addSubview:view];
        maincell=cell;
   }
    if (self.allCollection.tag ==1) {
         UICollectionViewCell* cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"ProgressBarCell" forIndexPath:indexPath];

        StrategicObjectives *entityObj=[self.dataArr objectAtIndex:indexPath.row];
        ProgressBarCell  * view=[ProgressBarCell customView:entityObj.chart_data];
        view.nameOfEntity.text=entityObj.name;
        view.detailsBtn.hidden=true;
        
      
        [cell addSubview:view];
        maincell=cell;
 
        
    }
    if (self.allCollection.tag ==2) {
        UICollectionViewCell* cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"DetailsCollectionViewCell" forIndexPath:indexPath];
        
        Dashboard *entityObj=[self.dataArr objectAtIndex:indexPath.row];
         NSMutableArray *items=[NSMutableArray new];
        for (Dashboard *statusObj in entityObj.taskStatuses) {
            
            [items addObject:[legendHelper dataItemWithColor:statusObj.colorIs description:statusObj.statusText count:[NSString stringWithFormat:@"%li",(long)statusObj.Count]]];
        }
        ProgressBarCell  * view=[ProgressBarCell PieCustom];
        PieChartVIew * pieChartData=[PieChartVIew PMOPieChart:entityObj.taskStatuses];
                DynamicLegend *legendViewCus=[DynamicLegend customView];
                [legendViewCus createViewWithArrayOfColorsAndNamesOfLegend:items];
      
        pieChartData.PieDelegate=self;
        view.nameOfEntity.text=entityObj.agencyName;
        view.detailsBtn.tag = indexPath.row;
        [view.detailsBtn addTarget:self action:@selector(collectionViewPieCellButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [view.viewLegend addSubview:legendViewCus];
        [view.chartView addSubview:pieChartData];
        [cell addSubview:view];

     maincell=cell;
        
        
        
    }
    return maincell;
    
}
#pragma mark - UICollectionViewFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat picDimension = self.allCollection.frame.size.width;
    return CGSizeMake(picDimension, picDimension);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    
        return UIEdgeInsetsMake(0, 0, 0, 0);
}
-(IBAction)collectionViewPieCellButtonPressed:(UIButton *)button {
    
    StrategicObjectives *portfObj=[self.dataArr objectAtIndex:button.tag];
    
    if (self.delegate) {
        [self.delegate didPress:portfObj.objID :portfObj.name];
    }
}
- (IBAction)collectionViewCellButtonPressed:(UIButton *)button {
    
    StrategicObjectives *portfObj=[self.dataArr objectAtIndex:button.tag];
    
    if (self.delegate) {
        [self.delegate didPress:portfObj.objID :portfObj.name];
    }
}




- (IBAction)changeOfSegment:(UISegmentedControl *)sender {
    
    if (sender.selectedSegmentIndex == 0) {
        [self customCollectionPie:self.dataArr];
        
    }
    
    else if (sender.selectedSegmentIndex == 1) {
           [self customCollection:self.dataArr];
    }
    
    
}


-(void)didClickOnPieChartWithId:(NSInteger)statusId {
    
    //  Dashboard *statusObj =[byMeDataItems objectAtIndex:statusId];
    //  NSInteger pieStatusId =statusId;
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setInteger:statusId forKey:@"myTasksPieFilterId"];
    
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"NavigationController"];
    
    [navigationController setViewControllers:@[[storyboard instantiateViewControllerWithIdentifier:@"TasksHolderViewController"]]];
    
    //  [[NetworkOperationManager sharedObjectLogin]setPmoBool:YES];
    MainViewController *mainViewController = [storyboard instantiateInitialViewController];
    mainViewController.rootViewController = navigationController;
    
    UIWindow *window = UIApplication.sharedApplication.delegate.window;
    window.rootViewController = mainViewController;
    
    [UIView transitionWithView:window
                      duration:0.3
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:nil
                    completion:nil];
    
    
}

@end
