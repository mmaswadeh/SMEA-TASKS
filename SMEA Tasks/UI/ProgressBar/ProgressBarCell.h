//
//  ProgressBarCell.h
//  MOH-BTIT-IOS-1
//
//  Created by Blessed Tree IT on 11/29/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProgressBarCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIStackView *stackView;
@property (weak, nonatomic) IBOutlet UILabel *nameOfEntity;
@property (weak, nonatomic) IBOutlet UIButton *detailsBtn;
@property (weak, nonatomic) IBOutlet UIView *chartView;
@property (weak, nonatomic) IBOutlet UIView *viewLegend;
+(id)customView:(NSArray *)dataArr;
+(id)customView:(NSArray *)dataArr total:(NSInteger)totalCount;
+(id)PieCustom;
@end
