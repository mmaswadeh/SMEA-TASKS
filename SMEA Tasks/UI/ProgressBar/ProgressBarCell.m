
//
//  ProgressBarCell.m
//  MOH-BTIT-IOS-1
//
//  Created by Blessed Tree IT on 11/29/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import "ProgressBarCell.h"
#import "ProgressView.h"
#import "chartData.h"

@implementation ProgressBarCell
//
+(id)customView:(NSArray *)dataArr{
    ProgressBarCell *customView = [[[NSBundle mainBundle] loadNibNamed:@"ProgressBarCell" owner:nil options:nil] lastObject];
    
    // make sure customView is not nil or the wrong class!
    if ([customView isKindOfClass:[ProgressBarCell class]])
    {
        [customView.detailsBtn setTitle:NSLocalizedString(@"GoToEntity", nil) forState:UIControlStateNormal];
        [customView drawStackView:dataArr];
        
        return customView;
        
        
    }
    else
        return nil;
}
+(id)PieCustom{
    
    ProgressBarCell *customView = [[[NSBundle mainBundle] loadNibNamed:@"ProgressBarCell" owner:nil options:nil] lastObject];
    
    // make sure customView is not nil or the wrong class!
    if ([customView isKindOfClass:[ProgressBarCell class]])
    {
        [customView.detailsBtn setTitle:NSLocalizedString(@"GoToEntity", nil) forState:UIControlStateNormal];
        
       
        
        return customView;
        
        
    }
    else
        return nil;
}
    


+(id)customView:(NSArray *)dataArr total:(NSInteger)totalCount
{
    ProgressBarCell *customView = [[[NSBundle mainBundle] loadNibNamed:@"ProgressBarCell" owner:nil options:nil] lastObject];
    
    // make sure customView is not nil or the wrong class!
    if ([customView isKindOfClass:[ProgressBarCell class]])
    {
        [customView.detailsBtn setTitle:NSLocalizedString(@"GoToEntity", nil) forState:UIControlStateNormal];

        [customView drawStackView:dataArr total:totalCount];
        
        return customView;
        
        
    }
    else
        return nil;
}


-(void)drawStackView:(NSArray *)dataItems total:(NSInteger)totalCount{
    
    for (int x=0; x<dataItems.count; x++) {
        chartData * obj=[dataItems objectAtIndex:x];
        ProgressView * progressBar =[ProgressView customView];
        
        NSLog(@"Count = %ld",(long)obj.count);
        NSLog(@"Total Count = %ld",totalCount);
        
        
        if (obj.count) {
            
            float x = (float)obj.count/(float)totalCount;
            NSLog(@"float = %f",x);
            progressBar.progressBar.progress=x;

        }
        
        else
        {
            progressBar.progressBar.progress=obj.count;

        }
        progressBar.progressBar.progressTintColor=obj.phaseColor;
       // progressBar.progressBar.trackTintColor = obj.shadowColor;
        progressBar.title.text=obj.statusName ;
        progressBar.amount.text=[NSString stringWithFormat:@"%ld",obj.count];
        [self.stackView addArrangedSubview:progressBar];
}
    
}

-(void)drawStackView:(NSArray *)dataItems{
   
    
    for (int x=0; x<dataItems.count; x++) {
        chartData * obj=[dataItems objectAtIndex:x];
         ProgressView * progressBar =[ProgressView customView];
        
        NSLog(@"Count = %ld",(long)obj.count);
        progressBar.progressBar.progress=obj.count;
        progressBar.progressBar.progressTintColor=obj.phaseColor;
       // progressBar.progressBar.trackTintColor = obj.shadowColor;
        progressBar.title.text=obj.statusName ;
        progressBar.amount.text=[NSString stringWithFormat:@"%ld",obj.count];
         [self.stackView addArrangedSubview:progressBar];
        
    }
// 
//    self.stackView.translatesAutoresizingMaskIntoConstraints = false;
//    
//  
//    [self.stackView.centerXAnchor constraintEqualToAnchor:self.centerXAnchor].active = true;
//    [self.stackView.centerYAnchor constraintEqualToAnchor:self.centerYAnchor].active = true;
        
   
    
}
@end
