//
//  DetailsCollectionViewCell.m
//  Dashboard-Product
//
//  Created by Blessed Tree IT on 5/9/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import "DetailsCollectionViewCell.h"

@implementation DetailsCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

    
    + (id)customView
    {
        DetailsCollectionViewCell *customView = [[[NSBundle mainBundle] loadNibNamed:@"DetailsCollectionViewCell" owner:nil options:nil] lastObject];
        
        // make sure customView is not nil or the wrong class!
        if ([customView isKindOfClass:[DetailsCollectionViewCell class]])
        {
            
            
            NSString * language = [[NSString alloc]init];
            
            customView.languageSettings = [[languageHelper alloc]init];
            language= [customView.languageSettings checkLanguages];
            
            if ([language isEqualToString:@"ar"]) {
                
              
                customView.rightArrow.transform=CGAffineTransformMakeScale(-1,1);
                customView.LeftArrow.transform=CGAffineTransformMakeScale(-1,1);
                     [customView.goToPortfolio setTitle:NSLocalizedString(@"GoToEntity", nil) forState:UIControlStateNormal];
                
            }
            return customView;
            
            
        }
        else
        return nil;
    }
@end
