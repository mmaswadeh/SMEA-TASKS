//
//  DetailsCollectionViewCell.h
//  Dashboard-Product
//
//  Created by Blessed Tree IT on 5/9/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "languageHelper.h"
@interface DetailsCollectionViewCell : UICollectionViewCell
    @property (weak, nonatomic) IBOutlet UIView *collectionViewCell;
    @property (weak, nonatomic) IBOutlet UIView *chartView;
       @property (weak, nonatomic) IBOutlet UIView *viewLegend;
    
    @property (weak, nonatomic) IBOutlet UILabel *portfoiloName;
    @property (weak, nonatomic) IBOutlet UIButton *goToPortfolio;
    
    @property (strong, nonatomic) IBOutlet UIImageView *rightArrow;
    @property (strong, nonatomic) IBOutlet UIImageView *LeftArrow;




@property (nonatomic, strong) languageHelper * languageSettings;

 + (id)customView;
@end
