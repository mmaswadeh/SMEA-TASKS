//
//  ProgressView.h
//  MOH-BTIT-IOS-1
//
//  Created by Blessed Tree IT on 11/29/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProgressView : UIView
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UIProgressView *progressBar;
@property (weak, nonatomic) IBOutlet UILabel *amount;
+(id)customView;
@end
