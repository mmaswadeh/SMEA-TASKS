//
//  ProgressBarChartCollectionView.h
//  MOH-BTIT-IOS-1
//
//  Created by Blessed Tree IT on 11/29/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "chartData.h"
#import "languageHelper.h"
#import "PieChartVIew.h"

@protocol allEntityCellDelegate <NSObject>

@optional

- (void)didPress:(NSInteger)entityID :(NSString*)entityName;

@end

@interface ProgressBarChartCollectionView : UIView<UICollectionViewDelegate,UICollectionViewDataSource,PNChartDelegate,PerformancePieChartDelegate>
+(id)createProgessStackView:(NSArray *)dataItems  withTotla:(NSInteger) total;
+ (id)createCollectionWithoutButton:(NSArray *) data;
+ (id)createCollectionWith:(NSArray *) data;
@property (weak, nonatomic) IBOutlet UICollectionView *allCollection;
@property (nonatomic,strong ) NSArray * dataArr;
@property (weak, nonatomic) id <allEntityCellDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIImageView *rightArrowImage;
@property (weak, nonatomic) IBOutlet UIImageView *leftArrowImage;
@property (nonatomic, strong) languageHelper * languageSettings;

@property (weak, nonatomic) IBOutlet UIStackView *ProgressStackView;
@property (weak, nonatomic) IBOutlet UILabel *headerTitle;
@property (weak, nonatomic) IBOutlet UIView *containerViewProgress;
@property (weak, nonatomic) IBOutlet UISegmentedControl *SwitchChartSegment;

- (IBAction)changeOfSegment:(UISegmentedControl *)sender;

@end
