//
//  CellDashboardView2.h
//  Dashboard-Product
//
//  Created by Blessed Tree IT on 5/8/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "languageHelper.h"

@interface CellDashboardView2 : UIView
@property (weak, nonatomic) IBOutlet UILabel *headerLabe2;
@property (weak, nonatomic) IBOutlet UIView *viewPieChartVC;
@property (weak, nonatomic) IBOutlet UIView *viewLegend;
@property (nonatomic, strong) languageHelper * languageSettings;
+ (id)customView;
-(void)checkLanguages;
@end
