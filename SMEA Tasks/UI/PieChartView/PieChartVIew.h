//
//  PieChartVIew.h
//  PMO-Product
//
//  Created by Blessed Tree IT on 5/7/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "languageHelper.h"
#import "Portfolios.h"
#import "Issues.h"
#import "Risks.h"
#import "HMLModel.h"
#import "PMOPieChart.h"
#import "StrategicObjectives.h"
#import "chartData.h"
#import "PNChartDelegate.h"
#import "PNChart.h"

@protocol PerformancePieChartDelegate <NSObject>
@optional

-(void)didClickOnPieChartWithId:(NSInteger)statusId;


@end


@interface PieChartVIew : UIView <PNChartDelegate> {
    
    id <PerformancePieChartDelegate> pieDelegate;
}

@property (strong, nonatomic) id <PerformancePieChartDelegate> PieDelegate;


@property (nonatomic) PMOPieChart * pieChart;
@property (weak, nonatomic) IBOutlet UIView *chartView;
    
@property (nonatomic, strong) languageHelper *languageSettings;

+ (id)SinglePieChart:(NSArray*)chartData;
+ (id)PortfolioChart:(Portfolios*)portfoliosObjects;
//+ (id)StrategicObjectivesChart:(chartData*)objectiveObjects;

//+ (id)IssuesChart:(Issues*)IssuesObjects;
//+ (id)RisksChart:(Risks*)RisksObjects;
+ (id)VROPieChart:(NSArray*)VROObj;
+ (id)PMOPieChart:(NSArray*)VROObj;

-(void)checkLanguages;
@end
