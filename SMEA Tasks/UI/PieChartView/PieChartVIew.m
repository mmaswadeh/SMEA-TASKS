//
//  PieChartVIew.m
//  PMO-Product
//
//  Created by Blessed Tree IT on 5/7/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import "PieChartVIew.h"
#import "NODataFoundView.h"
#import "PNPieChart.h"
#import "chartData.h"
#import "StrategicObjectives.h"
#import "PNChart.h"
#import "PNPieChartDataItem.h"
#import "Dashboard.h"
#import "PNChartDelegate.h"
@interface PieChartVIew ()
{
NSArray *itemsCustom;
}
@end
@implementation PieChartVIew
@synthesize pieChart;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setDefaults];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        
        
        [self setDefaults];
    }
    return self;
}

- (void)setDefaults
{
    
    [self.chartView clearsContextBeforeDrawing];
    // self.backgroundColor = [UIColor whiteColor];
    //    self.opaque = YES;
    self.pieChart.delegate = self;
    
    
}

+ (id)SinglePieChart:(NSArray*)chartData{
    
    PMOPieChart * pieChart;
    NSMutableArray *items=[NSMutableArray new];
    PieChartVIew *PieChartVC = [[[NSBundle mainBundle] loadNibNamed:@"PieChartVIew" owner:nil options:nil] lastObject];
    
    // make sure customView is not nil or the wrong class!
    if ([PieChartVC isKindOfClass:[PieChartVIew class]])
    {
        // Status *portfoliosObj=[portfoliosArr objectAtIndex:x];
        
        
        
        for (Status *statusObj in chartData) {
            if (statusObj.percentage>0) {
                
               [items addObject:[PNPieChartDataItem dataItemWithValue:statusObj.percentage color:statusObj.projectColor]];
                
                
            }
            
            pieChart = [[PMOPieChart alloc] initWithFrame:CGRectMake(0 ,0, 250, 250) items:items];
            pieChart.showAbsoluteValues = YES;
            pieChart.showOnlyValues = NO;
            
            
            
            [PieChartVC addSubview:pieChart];
            
            
            
        }
        
        
        
        
        
        
        
        if([items count]==0)
        {
            
            NODataFoundView * noView=[NODataFoundView customView];
            [PieChartVC addSubview:noView];
            
            noView.translatesAutoresizingMaskIntoConstraints = NO;
            
            NSLayoutConstraint *xCenterConstraint = [NSLayoutConstraint constraintWithItem:noView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:PieChartVC attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0];
            
            NSLayoutConstraint *top =[NSLayoutConstraint
                                      constraintWithItem:noView
                                      attribute:NSLayoutAttributeTop
                                      relatedBy:NSLayoutRelationEqual
                                      toItem:PieChartVC
                                      attribute:NSLayoutAttributeTop
                                      multiplier:1.0f
                                      constant:0.f];
            
            
            [PieChartVC addConstraint:xCenterConstraint];
            [PieChartVC addConstraint:top];
            
        }
        return PieChartVC;
        
        
    }
    else
        return nil;
    
    
}
+ (id)PortfolioChart:(Portfolios*)portfoliosObjects{
    
    PMOPieChart * pieChart;
  NSMutableArray *items=[NSMutableArray new];
    PieChartVIew *PieChartVC = [[[NSBundle mainBundle] loadNibNamed:@"PieChartVIew" owner:nil options:nil] lastObject];
    
    // make sure customView is not nil or the wrong class!
    if ([PieChartVC isKindOfClass:[PieChartVIew class]])
    {
       // Status *portfoliosObj=[portfoliosArr objectAtIndex:x];
   
    
        
        for (Status *statusObj in portfoliosObjects.status) {
            if (statusObj.percentage>0) {
                
                [items addObject:[PNPieChartDataItem dataItemWithValue:statusObj.percentage color:statusObj.projectColor]];
                
                
            }
            
           pieChart = [[PMOPieChart alloc] initWithFrame:CGRectMake(0 ,0, 200, 200) items:items];
            pieChart.showAbsoluteValues = YES;
            pieChart.showOnlyValues = NO;
            
            
            
            [PieChartVC addSubview:pieChart];
            

        }
        

         if([items count]==0){
            NODataFoundView * noView=[NODataFoundView customView];
                        [PieChartVC addSubview:noView];
             
             noView.translatesAutoresizingMaskIntoConstraints = NO;
             
             NSLayoutConstraint *xCenterConstraint = [NSLayoutConstraint constraintWithItem:noView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:PieChartVC attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0];
             
             NSLayoutConstraint *top =[NSLayoutConstraint
                                       constraintWithItem:noView
                                       attribute:NSLayoutAttributeTop
                                       relatedBy:NSLayoutRelationEqual
                                       toItem:PieChartVC
                                       attribute:NSLayoutAttributeTop
                                       multiplier:1.0f
                                       constant:0.f];
             
             
             [PieChartVC addConstraint:xCenterConstraint];
             [PieChartVC addConstraint:top];
        
        }
        return PieChartVC;
        
        
    }
    else
        return nil;


}

+ (id)VROPieChart:(NSArray*)VROObj{
    
    PMOPieChart * pieChart;
    NSMutableArray *items=[NSMutableArray new];
    PieChartVIew *PieChartVC = [[[NSBundle mainBundle] loadNibNamed:@"PieChartVIew" owner:nil options:nil] lastObject];
    
    // make sure customView is not nil or the wrong class!
    if ([PieChartVC isKindOfClass:[PieChartVIew class]])
    {
        // Status *portfoliosObj=[portfoliosArr objectAtIndex:x];
        
        for (chartData *statusObj in VROObj) {
            
            
            if (statusObj.count>0) {
                
                [items addObject:[PNPieChartDataItem dataItemWithValue:statusObj.count color:statusObj.phaseColor]];
                
                
            }
            
            pieChart = [[PMOPieChart alloc] initWithFrame:CGRectMake(0 ,0, 250, 250) items:items];
            pieChart.shouldHighlightSectorOnTouch=false;
            pieChart.showAbsoluteValues = YES;
            pieChart.showOnlyValues = NO;
            
            
            
            [PieChartVC addSubview:pieChart];
            
            
            
        }
        

        if([items count]==0)
        {
            
            NODataFoundView * noView=[NODataFoundView customView];
            [PieChartVC addSubview:noView];
            
            noView.translatesAutoresizingMaskIntoConstraints = NO;
            
            NSLayoutConstraint *xCenterConstraint = [NSLayoutConstraint constraintWithItem:noView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:PieChartVC attribute:NSLayoutAttributeCenterXWithinMargins multiplier:1 constant:0];
            
            NSLayoutConstraint *top =[NSLayoutConstraint
                                      constraintWithItem:noView
                                      attribute:NSLayoutAttributeTop
                                      relatedBy:NSLayoutRelationEqual
                                      toItem:PieChartVC
                                      attribute:NSLayoutAttributeCenterYWithinMargins
                                      multiplier:1.0f
                                      constant:1.f];
            
            
            [PieChartVC addConstraint:xCenterConstraint];
            [PieChartVC addConstraint:top];
            
        }
        return PieChartVC;
        
        
    }
    else
        return nil;
    
    
}


+ (id)PMOPieChart:(NSArray*)VROObj{
    
    NSMutableArray *items=[NSMutableArray new];
    PieChartVIew *PieChartVC = [[[NSBundle mainBundle] loadNibNamed:@"PieChartVIew" owner:nil options:nil] lastObject];
    [PieChartVC setUserInteractionEnabled:YES];

    // make sure customView is not nil or the wrong class!
    if ([PieChartVC isKindOfClass:[PieChartVIew class]])
    {
        // Status *portfoliosObj=[portfoliosArr objectAtIndex:x];

        for (Dashboard *statusObj in VROObj) {
            if (statusObj.Count>0) {
                
                [items addObject:[PNPieChartDataItem dataItemWithValue:statusObj.Count color:statusObj.colorIs statusID:statusObj.StatusLkpId]];
                
                //dataItemWithValue:statusObj.Count color:statusObj.colorIs ]];
                
                
            }
            PMOPieChart * pieChart = [[PMOPieChart alloc] initWithFrame:CGRectMake(0 ,0, 200, 200) items:items];
            pieChart.showAbsoluteValues = YES;
            pieChart.shouldHighlightSectorOnTouch=YES;
            pieChart.showOnlyValues = NO;
            pieChart.hideValues=false;
            [PieChartVC addSubview:pieChart];
            
            [PieChartVC drawPieChart:items with:PieChartVC];
            
            
        }
                
        if([items count]==0)
        {
          [PieChartVC setUserInteractionEnabled:NO];
            NODataFoundView * noView=[NODataFoundView customView];
            [PieChartVC addSubview:noView];
            
            noView.translatesAutoresizingMaskIntoConstraints = NO;
            
            NSLayoutConstraint *xCenterConstraint = [NSLayoutConstraint constraintWithItem:noView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:PieChartVC attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0];
            
            NSLayoutConstraint *top =[NSLayoutConstraint
                                      constraintWithItem:noView
                                      attribute:NSLayoutAttributeTop
                                      relatedBy:NSLayoutRelationEqual
                                      toItem:PieChartVC
                                      attribute:NSLayoutAttributeTop
                                      multiplier:1.0f
                                      constant:0.f];
            
            
            [PieChartVC addConstraint:xCenterConstraint];
            [PieChartVC addConstraint:top];
            
        }
        return PieChartVC;
        
        
    }
    else
        return nil;
    
    
}

-(void)drawPieChart:(NSArray*)items with:(PieChartVIew*)PieChartVC
{
    PMOPieChart * pieChart = [[PMOPieChart alloc] initWithFrame:CGRectMake(0 ,0, 200, 200) items:items];
    pieChart.showAbsoluteValues = YES;
    pieChart.shouldHighlightSectorOnTouch=YES;
    pieChart.showOnlyValues = NO;
    pieChart.hideValues=false;
    pieChart.delegate = self;
    itemsCustom = [NSArray arrayWithArray:items];
    [PieChartVC addSubview:pieChart];
    
    
}


-(void)userClickedOnPieIndexItem:(NSInteger)pieIndex
{
    
    PNPieChartDataItem *statusObj =[itemsCustom objectAtIndex:pieIndex];

    [self.PieDelegate didClickOnPieChartWithId:statusObj.statusID];
        NSLog(@"did click");
}

-(void)checkLanguages{
    
        NSString * language = [[NSString alloc]init];
    
        _languageSettings = [[languageHelper alloc]init];
        language= [_languageSettings checkLanguages];
    
        if ([language isEqualToString:@"ar"]) {
            [self.pieChart  setTransform:CGAffineTransformMakeScale(-1, 1)];
            
        }
        
    }

    

@end
