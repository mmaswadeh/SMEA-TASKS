//
//  NODataFoundView.h
//  Dashboard-Core
//
//  Created by Blessed Tree IT on 5/10/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NODataFoundView : UIView
    @property (weak, nonatomic) IBOutlet UILabel *noDataLabel;
    
+ (id)customView;
@end
