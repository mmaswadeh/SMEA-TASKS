//
//  NODataFoundView.m
//  Dashboard-Core
//
//  Created by Blessed Tree IT on 5/10/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import "NODataFoundView.h"

@implementation NODataFoundView

+ (id)customView
    {
        NODataFoundView *customView = [[[NSBundle mainBundle] loadNibNamed:@"NODataFoundView" owner:nil options:nil] lastObject];
        
        // make sure customView is not nil or the wrong class!
        if ([customView isKindOfClass:[NODataFoundView class]])
        {
            customView.noDataLabel.text=NSLocalizedString(@"noDataFound", nil);
            
            return customView;
            
            
        }
        else
        return nil;
    }

@end
