//
//  AgencyCollectionView.m
//  MCI-Tasks
//
//  Created by Mohammad Maswadeh on 2/26/18.
//  Copyright © 2018 BTIT. All rights reserved.
//

#import "AgencyCollectionView.h"
#import "StrategicObjectives.h"
#import "ProgressBarCell.h"
#import "AgencyCustomCell.h"
#import "Dashboard.h"
#import "legendHelper.h"
#import "DynamicLegend.h"
#import "PieChartVIew.h"
#import "MainViewController.h"

@interface AgencyCollectionView()<PerformancePieChartDelegate>

@end

@implementation AgencyCollectionView

+(id)createCollectionWithData:(NSArray *) data ScreenName:(NSString*)name {

    AgencyCollectionView *customView = [[[NSBundle mainBundle] loadNibNamed:@"AgencyCollectionView" owner:nil options:nil] lastObject];
    // make sure customView is not nil or the wrong class!
    if ([customView isKindOfClass:[AgencyCollectionView class]]) {
        
        customView.agenciesDataArray = [NSArray arrayWithArray:data];
        customView.screenName = name;
        [customView DrawCollectionWithData:data];
        
        return customView;
    }
    else
        return nil;
    
    return nil;
}

-(void)DrawCollectionWithData:(NSArray *)collectionData {
    
    
    NSString * language = [[NSString alloc]init];
    
    _languageSettings = [[languageHelper alloc]init];
    language= [_languageSettings checkLanguages];
    if ([language isEqualToString:@"ar"]) {
        _rightArrowImage.image = [UIImage imageNamed:@"leftArrow"];
        _leftArrowImage.image = [UIImage imageNamed:@"rightArrow"];
    }
    
    else{
        
    }
    
    NSMutableArray *items=[NSMutableArray new];

    if ([collectionData count]>0) {
        
        Dashboard *entityObj=[collectionData objectAtIndex:0];
        
        for (Dashboard *statusObj in entityObj.taskStatuses) {
            
            [items addObject:[legendHelper dataItemWithColor:statusObj.colorIs description:statusObj.statusText count:[NSString stringWithFormat:@"%li",(long)statusObj.Count]]];
        }
        
    }
    
    
    DynamicLegend *legendViewCus=[DynamicLegend customView];
    [legendViewCus createViewWithArrayOfColorsAndNamesOfLegend:items];
    
    
    legendViewCus.center = CGPointMake(self.legendView.frame.size.width  / 2,
                                       self.legendView.frame.size.height / 2);
    
    [self.legendView addSubview:legendViewCus];
    
    [self.agencyCollection registerClass:[AgencyCustomCell class] forCellWithReuseIdentifier:@"CustomCell"];
    self.agencyCollection.delegate = self;
    self.agencyCollection.dataSource = self;
    [self.agencyCollection reloadData];
    
}
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    
    return  1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.agenciesDataArray.count;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell* cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"CustomCell" forIndexPath:indexPath];
    
    Dashboard *entityObj=[self.agenciesDataArray objectAtIndex:indexPath.row];
    NSMutableArray *items=[NSMutableArray new];

    for (Dashboard *statusObj in entityObj.taskStatuses) {
        
        [items addObject:[legendHelper dataItemWithColor:statusObj.colorIs description:statusObj.statusText count:[NSString stringWithFormat:@"%li",(long)statusObj.Count]]];
    }
     
    AgencyCustomCell  * view=[AgencyCustomCell customView:self.agenciesDataArray];
    PieChartVIew * pieChartData=[PieChartVIew PMOPieChart:entityObj.taskStatuses];
    [view.agencyLabel setText:entityObj.agencyName];

    DynamicLegend *legendViewCus=[DynamicLegend customView];
    [legendViewCus createViewWithArrayOfColorsAndNamesOfLegend:items];
    
    if ([_screenName isEqualToString:@"AllTasksScreen"]) {
    }
    else {
        pieChartData.PieDelegate = self;
        
    }
    
    
    if (indexPath.row > 0) {
        
        Dashboard *entityObj=[self.agenciesDataArray objectAtIndex:indexPath.row-1];
        _agencyId =entityObj.agencyId;
        
    }
    else
    {
        _agencyId =entityObj.agencyId;
        
    }
    
    
    [view.chartView addSubview:pieChartData];
    
 
    [cell addSubview:view];

  //  cell.translatesAutoresizingMaskIntoConstraints = NO;
    
 //  [self changeViewConstraints:view WithCell:cell];
    return cell;
    
}

-(void)didClickOnPieChartWithId:(NSInteger)statusId {
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setInteger:statusId forKey:_screenName];
    [userDefaults setInteger:_agencyId forKey:@"agencyFilteredId"];

    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"NavigationController"];
    
    [navigationController setViewControllers:@[[storyboard instantiateViewControllerWithIdentifier:@"TasksHolderViewController"]]];
    
    //  [[NetworkOperationManager sharedObjectLogin]setPmoBool:YES];
    MainViewController *mainViewController = [storyboard instantiateInitialViewController];
    mainViewController.rootViewController = navigationController;
    
    UIWindow *window = UIApplication.sharedApplication.delegate.window;
    window.rootViewController = mainViewController;
    
    [UIView transitionWithView:window
                      duration:0.3
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:nil
                    completion:nil];
    
    
}
#pragma mark - UICollectionViewFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat picDimension = self.agencyCollection.frame.size.width;
    return CGSizeMake(picDimension, picDimension);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {

    return UIEdgeInsetsMake(0, 0, 0, 0);
}

-(void)changeViewConstraints:(UIView*)view WithCell:(UICollectionViewCell*)cell
{
    
    //Trailing
    NSLayoutConstraint *trailing =[NSLayoutConstraint
                                   constraintWithItem:view
                                   attribute:NSLayoutAttributeTrailing
                                   relatedBy:NSLayoutRelationEqual
                                   toItem:cell
                                   attribute:NSLayoutAttributeTrailing
                                   multiplier:1.0f
                                   constant:0.f];
    
    //Leading
    
    NSLayoutConstraint *leading = [NSLayoutConstraint
                                   constraintWithItem:view
                                   attribute:NSLayoutAttributeLeading
                                   relatedBy:NSLayoutRelationEqual
                                   toItem:cell
                                   attribute:NSLayoutAttributeLeading
                                   multiplier:1.0f
                                   constant:0.f];
    
    //Bottom
    NSLayoutConstraint *bottom =[NSLayoutConstraint
                                 constraintWithItem:view
                                 attribute:NSLayoutAttributeBottom
                                 relatedBy:NSLayoutRelationEqual
                                 toItem:cell
                                 attribute:NSLayoutAttributeBottom
                                 multiplier:1.0f
                                 constant:0.f];
    
    NSLayoutConstraint *top =[NSLayoutConstraint
                              constraintWithItem:view
                              attribute:NSLayoutAttributeTop
                              relatedBy:NSLayoutRelationEqual
                              toItem:cell
                              attribute:NSLayoutAttributeTop
                              multiplier:1.0f
                              constant:0.f];
    
    //Height to be fixed for SubView same as AdHeight
    NSLayoutConstraint *height = [NSLayoutConstraint
                                  constraintWithItem:view
                                  attribute:NSLayoutAttributeHeight
                                  relatedBy:NSLayoutRelationEqual
                                  toItem:nil
                                  attribute:NSLayoutAttributeNotAnAttribute
                                  multiplier:0
                                  constant:650.f];
    
    [cell addConstraint:trailing];
    [cell addConstraint:bottom];
    [cell addConstraint:top];
    [cell addConstraint:leading];
    [cell addConstraint:height];
    
    
}



@end
