//
//  AgencyCollectionView.h
//  MCI-Tasks
//
//  Created by Mohammad Maswadeh on 2/26/18.
//  Copyright © 2018 BTIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "languageHelper.h"

@interface AgencyCollectionView : UIView <UICollectionViewDelegate,UICollectionViewDataSource>

@property (weak, nonatomic) IBOutlet UIView *legendView;
@property (nonatomic, strong) NSString * screenName;
@property(nonatomic,assign)   NSInteger  agencyId;

@property (weak, nonatomic) IBOutlet UICollectionView *agencyCollection;
@property (weak, nonatomic) IBOutlet UILabel *agencyNameLbl;
@property (nonatomic,strong) NSArray *agenciesDataArray;

+(id)createCollectionWithData:(NSArray *) data ScreenName:(NSString*)name;
@property (weak, nonatomic) IBOutlet UILabel *headerLabel;
@property (weak, nonatomic) IBOutlet UIImageView *rightArrowImage;
@property (weak, nonatomic) IBOutlet UIImageView *leftArrowImage;
@property (nonatomic, strong) languageHelper * languageSettings;

@end
