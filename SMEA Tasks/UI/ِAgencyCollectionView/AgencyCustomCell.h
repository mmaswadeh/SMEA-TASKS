//
//  AgencyCustomCell.h
//  MCI-Tasks
//
//  Created by Mohammad Maswadeh on 2/26/18.
//  Copyright © 2018 BTIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AgencyCustomCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIView *chartView;
@property (weak, nonatomic) IBOutlet UILabel *agencyLabel;
+(id)customView:(NSArray *)dataArr;
+(id)PieCustom;

@end
