//
//  AgencyCustomCell.m
//  MCI-Tasks
//
//  Created by Mohammad Maswadeh on 2/26/18.
//  Copyright © 2018 BTIT. All rights reserved.
//

#import "AgencyCustomCell.h"

@implementation AgencyCustomCell

- (void)awakeFromNib {
    [super awakeFromNib];

    // Initialization code
}

+(id)customView:(NSArray *)dataArr{
    AgencyCustomCell *customView = [[[NSBundle mainBundle] loadNibNamed:@"AgencyCustomCell" owner:nil options:nil] lastObject];
    
    // make sure customView is not nil or the wrong class!
    if ([customView isKindOfClass:[AgencyCustomCell class]])
    {        
        return customView;
        
        
    }
    else
        return nil;
}

+(id)PieCustom{
    
    AgencyCustomCell *customView = [[[NSBundle mainBundle] loadNibNamed:@"AgencyCustomCell" owner:nil options:nil] lastObject];
    
    // make sure customView is not nil or the wrong class!
    if ([customView isKindOfClass:[AgencyCustomCell class]])
    {
        return customView;
        
        
    }
    else
        return nil;
}

@end
