//
//  HeaderCollection.h
//  MOH-BTIT-IOS-1
//
//  Created by Blessed Tree IT on 11/28/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HeaderCollCell.h"
@interface HeaderCollection : UIView<UICollectionViewDelegate,UICollectionViewDataSource>

+ (id)createCollectionWith:(NSMutableArray *) data;
+ (id)createCollectionWithNames:(NSArray*)names :(NSMutableArray *) data;

@property (weak, nonatomic) IBOutlet UICollectionView *allCollection;
@property (nonatomic,strong ) NSArray * dataArr;
@property (nonatomic,strong ) NSArray * namesArr;

@end
