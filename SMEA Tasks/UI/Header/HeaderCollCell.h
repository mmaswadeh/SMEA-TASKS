//
//  HeaderCollCell.h
//  MOH-BTIT-IOS-1
//
//  Created by Blessed Tree IT on 11/28/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HeaderCollCell : UICollectionViewCell
+ (id)customViewName:(NSString *)nameIs count:(NSString *)countIs;
@property (weak, nonatomic) IBOutlet UILabel *countOf;
@property (weak, nonatomic) IBOutlet UILabel *nameOf;

@end
