//
//  HeaderCollection.m
//  MOH-BTIT-IOS-1
//
//  Created by Blessed Tree IT on 11/28/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import "HeaderCollection.h"
//#import "IssuesViewController.h"
//#import "RisksViewController.h"
//#import "AllProjectsViewController.h"
#import "MainViewController.h"
#import "DashboardCollectionCell.h"

@implementation HeaderCollection
+ (id)createCollectionWith:(NSMutableArray *) data{
    HeaderCollection *customView = [[[NSBundle mainBundle] loadNibNamed:@"HeaderCollection" owner:nil options:nil] lastObject];
    
    // make sure customView is not nil or the wrong class!
    if ([customView isKindOfClass:[HeaderCollection class]])
    {
        customView.dataArr=[NSArray arrayWithArray:data];
 
        [customView customCollection];
        return customView;
        
        
    }
    else
        return nil;
}

+ (id)createCollectionWithNames:(NSArray*)names :(NSMutableArray *) data
{
    HeaderCollection *customView = [[[NSBundle mainBundle] loadNibNamed:@"HeaderCollection" owner:nil options:nil] lastObject];
    
    // make sure customView is not nil or the wrong class!
    if ([customView isKindOfClass:[HeaderCollection class]])
    {
        customView.dataArr=[NSArray arrayWithArray:data];
        customView.namesArr= [NSArray arrayWithArray:names];
        [customView customCollection];
        return customView;
        
    }
    else
        return nil;
    
}
-(void)customCollection{
    
    [self.allCollection registerClass:[HeaderCollCell class] forCellWithReuseIdentifier:@"HeaderCollCell"];
    self.allCollection.delegate = self;
    self.allCollection.dataSource = self;
    [self.allCollection reloadData];
    
    
}
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{

    return  1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [_namesArr count];
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
//    HeaderCollCell* cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"HeaderCollCell" forIndexPath:indexPath];
//        cell.nameOf.text = [_namesArr objectAtIndex:indexPath.row];
//    cell.countOf.text = [_dataArr objectAtIndex:indexPath.row];
//
//    return cell;
    
    DashboardCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"DashboardCollectionCell" forIndexPath:indexPath];
    
    cell.cellNumnber.text = [NSString stringWithFormat:@"%ld",(long)[_dataArr objectAtIndex:indexPath.row]];
    cell.cellName.text = [_namesArr objectAtIndex:indexPath.row];
    return cell;
    
    
    
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat picDimension = self.allCollection.frame.size.height ;
    return CGSizeMake(picDimension, picDimension);
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    

//
    if (indexPath.row == 0  && indexPath.section==0 ){
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"NavigationController"];
        
        [navigationController setViewControllers:@[[storyboard instantiateViewControllerWithIdentifier:@"RisksViewController"]]];
        
        [[NetworkOperationManager sharedObjectLogin]setPmoBool:NO];
        MainViewController *mainViewController = [storyboard instantiateInitialViewController];
        mainViewController.rootViewController = navigationController;
        
        UIWindow *window = UIApplication.sharedApplication.delegate.window;
        window.rootViewController = mainViewController;
        
        [UIView transitionWithView:window
                          duration:0.3
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:nil
                        completion:nil];
      
    }

     if (indexPath.row == 1  && indexPath.section==0 ){

//        IssuesViewController
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"NavigationController"];
        
        [navigationController setViewControllers:@[[storyboard instantiateViewControllerWithIdentifier:@"IssuesViewController"]]];
        
        [[NetworkOperationManager sharedObjectLogin]setPmoBool:NO];
        MainViewController *mainViewController = [storyboard instantiateInitialViewController];
        mainViewController.rootViewController = navigationController;
        
        UIWindow *window = UIApplication.sharedApplication.delegate.window;
        window.rootViewController = mainViewController;
        
        [UIView transitionWithView:window
                          duration:0.3
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:nil
                        completion:nil];
    }

 if (indexPath.row == 2  && indexPath.section==0 ){

       // AllProjectsViewController
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"NavigationController"];
        
        [navigationController setViewControllers:@[[storyboard instantiateViewControllerWithIdentifier:@"AllProjectsViewController"]]];
        
        [[NetworkOperationManager sharedObjectLogin]setPmoBool:NO];
        MainViewController *mainViewController = [storyboard instantiateInitialViewController];
        mainViewController.rootViewController = navigationController;
        
        UIWindow *window = UIApplication.sharedApplication.delegate.window;
        window.rootViewController = mainViewController;
        
        [UIView transitionWithView:window
                          duration:0.3
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:nil
                        completion:nil];
    }

}



@end
