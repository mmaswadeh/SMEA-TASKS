//
//  HeaderCollCell.m
//  MOH-BTIT-IOS-1
//
//  Created by Blessed Tree IT on 11/28/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import "HeaderCollCell.h"

@implementation HeaderCollCell
+ (id)customViewName:(NSString *)nameIs count:(NSString *)countIs
{
    HeaderCollCell *customView = [[[NSBundle mainBundle] loadNibNamed:@"HeaderCollCell" owner:nil options:nil] lastObject];
    
    // make sure customView is not nil or the wrong class!
    if ([customView isKindOfClass:[HeaderCollCell class]])
    {
        
        customView.nameOf.text=nameIs;
        customView.countOf.text=countIs;
        
        return customView;
        
        
    }
    else
        return nil;
}


@end
