//
//  UpdateDelayPopup.h
//  MCI-Tasks
//
//  Created by Mohammad Maswadeh on 1/27/18.
//  Copyright © 2018 BTIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SZTextView.h"
#import <LGAlertView/LGAlertView.h>
#import "PopupView.h"

//@import PopupKit;

@protocol UpdateDelayPopupDelegate <NSObject>
@optional

-(void)updateDelayDelegateWithJustification:(NSString *)justification;
-(void)deleteDelayWithTaskId:(NSString *)taskId;

@end
@interface UpdateDelayPopup : UIView {
    
    id <UpdateDelayPopupDelegate> delegate;
}

@property (strong, nonatomic) id <UpdateDelayPopupDelegate> delegate;


@end
