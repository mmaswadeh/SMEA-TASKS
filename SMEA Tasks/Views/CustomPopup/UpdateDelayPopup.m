//
//  UpdateDelayPopup.m
//  MCI-Tasks
//
//  Created by Mohammad Maswadeh on 1/27/18.
//  Copyright © 2018 BTIT. All rights reserved.
//

#import "UpdateDelayPopup.h"


@interface UpdateDelayPopup () <UIPopoverControllerDelegate,UITextFieldDelegate,LGAlertViewDelegate>

@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UITextField *delayDate;
@property (weak, nonatomic) IBOutlet SZTextView *justificationText;

@property (weak, nonatomic) IBOutlet UIButton *saveBtn;
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;

@end


@implementation UpdateDelayPopup
@synthesize delegate;

-(instancetype)initWithCoder:(NSCoder *)aDecoder {
    
    self = [super initWithCoder:aDecoder];
    if (self) {
        
        [self customInit];
    }
    
    return self;
}


-(instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        
        [self customInit];
    }
    
    return self;
}

-(void)customInit {
    
    [[NSBundle mainBundle] loadNibNamed:@"UpdateDelayPopup" owner:self options:nil];
     [self.saveBtn setTitle:NSLocalizedString(@"Save", nil) forState:UIControlStateNormal];
     [self.deleteBtn setTitle:NSLocalizedString(@"delete", nil) forState:UIControlStateNormal];
     [self.cancelBtn setTitle:NSLocalizedString(@"cancel", nil) forState:UIControlStateNormal];
    self.delayDate.delegate = self;
    self.layer.cornerRadius = 10;
    self.clipsToBounds = YES;
    [self addSubview:self.contentView];
    self.contentView.frame = self.bounds;
    
}


- (IBAction)submitAction:(id)sender {
    
    [self.delegate updateDelayDelegateWithJustification:_justificationText.text];
    if ([sender isKindOfClass:[UIView class]]) {
        [(UIView*)sender dismissPresentingPopup];
    }
    
}

- (IBAction)deleteAction:(id)sender {
    
    [self.delegate deleteDelayWithTaskId:@""];
    if ([sender isKindOfClass:[UIView class]]) {
        [(UIView*)sender dismissPresentingPopup];
    }
    
}

- (IBAction)cancelAction:(id)sender {
    
    if ([sender isKindOfClass:[UIView class]]) {
        [(UIView*)sender dismissPresentingPopup];
    }
    
}


@end
