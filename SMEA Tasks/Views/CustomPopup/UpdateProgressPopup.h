//
//  UpdateProgressPopup.h
//  MCI-Tasks
//
//  Created by Mohammad Maswadeh on 1/23/18.
//  Copyright © 2018 BTIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SZTextView.h"
#import <LGAlertView/LGAlertView.h>
#import "PopupView.h"

//@import PopupKit;
@protocol UpdateProgressPopupDelegate <NSObject>
@optional
-(void)updateProgressDelegateWithValue:(NSString*)value andClosureType:(NSInteger)closureType andTarget:(NSString*)target;
-(NSInteger)getProgressIndex;

@end

@interface UpdateProgressPopup : UIView {
    
    id <UpdateProgressPopupDelegate> delegate;
}

@property (strong, nonatomic) id <UpdateProgressPopupDelegate> delegate;


@end
