//
//  UpdateProgressPopup.m
//  MCI-Tasks
//
//  Created by Mohammad Maswadeh on 1/23/18.
//  Copyright © 2018 BTIT. All rights reserved.
//

#import "UpdateProgressPopup.h"

@interface UpdateProgressPopup () <UIPopoverControllerDelegate,UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource,LGAlertViewDelegate,UITextViewDelegate>

@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIPickerView *progressPicker;
@property (weak, nonatomic) IBOutlet UITextField *closureType;
@property (weak, nonatomic) IBOutlet SZTextView *targetName;

@property (weak, nonatomic) IBOutlet UIButton *saveBtn;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property (nonatomic, strong) NSArray *closureNamesArr;
@property (nonatomic, strong) NSArray *closureIdsArr;
@property (nonatomic, assign) NSInteger closureId;

@property (nonatomic, strong) NSArray *uneditableTextFields;
@property (weak, nonatomic) IBOutlet UILabel *titleProgress;

@end

@implementation UpdateProgressPopup
@synthesize delegate;

-(instancetype)initWithCoder:(NSCoder *)aDecoder {
    
    self = [super initWithCoder:aDecoder];
    if (self) {
        
        [self customInit];
    }
    
    return self;
}


-(instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        
        [self customInit];
    }
    
    return self;
}

-(void)customInit {
    
    [[NSBundle mainBundle] loadNibNamed:@"UpdateProgressPopup" owner:self options:nil];
    _targetName.placeholder=NSLocalizedString(@"targetName", nil);
    _closureType.placeholder=NSLocalizedString(@"closuretype", nil);
    _titleProgress.text=NSLocalizedString(@"titleprogress", nil);;
    [_saveBtn setTitle:NSLocalizedString(@"Update", nil)   forState:UIControlStateNormal];
    [_cancelBtn setTitle:NSLocalizedString(@"cancel", nil) forState:UIControlStateNormal];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    _closureNamesArr = [userDefaults objectForKey:@"closureNames"];
    _closureIdsArr = [userDefaults objectForKey:@"closureId"];
    


    self.uneditableTextFields = @[self.closureType];
    self.closureType.delegate = self;
    
    self.progressPicker.delegate = self;
    self.progressPicker.dataSource = self;
    self.layer.cornerRadius = 10;
    self.clipsToBounds = YES;
    [self addSubview:self.contentView];
    self.contentView.frame = self.bounds;
    
}

//-(BOOL)textViewShouldBeginEditing:(SZTextView *)textView{
//    
// 
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
//    return  true;}
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    BOOL shouldEdit = ![self.uneditableTextFields containsObject:textField];
    if (textField == self.closureType){
        
        [self addClosureType];
   

    }
    return shouldEdit;
}


-(void)addClosureType{
    
    
    LGAlertView *alertView = [[LGAlertView alloc] initWithTitle:NSLocalizedString(@"Closure", nil)
                                                        message:NSLocalizedString(@"Please choose a closure type", nil)
                                                          style:LGAlertViewStyleAlert
                                                   buttonTitles:_closureNamesArr
                                              cancelButtonTitle:NSLocalizedString(@"cancel", nil)
                                         destructiveButtonTitle:nil
                                                       delegate:self];
    
    
    alertView.windowLevel = LGAlertViewWindowLevelBelowStatusBar;
    [alertView showAnimated:YES completionHandler:nil];
}


- (void)alertView:(LGAlertView *)alertView clickedButtonAtIndex:(NSUInteger)index title:(nullable NSString *)title {
    NSLog(@"action {title: %@, index: %lu}", title, (long unsigned)index);
    
        _closureType.text = title;
        _closureId = [[_closureIdsArr objectAtIndex:index] integerValue];
    if (_closureId==640) {
        [_targetName setUserInteractionEnabled:NO];
    }
    else{
        
        
         [_targetName setUserInteractionEnabled:YES];
    }
    
    
}

- (void)alertViewCancelled:(LGAlertView *)alertView {
    NSLog(@"cancel");
}

- (void)alertViewDestructed:(LGAlertView *)alertView {
    NSLog(@"destructive");
}

- (IBAction)submitAction:(id)sender {
    
    NSLog(@"submit");
    
    NSString *value = [NSString stringWithFormat:@"%ld",(long)[_progressPicker selectedRowInComponent:0]+1];
    if ([_closureType isEnabled] && [_targetName isUserInteractionEnabled]) {
        
        [self.delegate updateProgressDelegateWithValue:value andClosureType:_closureId andTarget:_targetName.text];

    }
    
    else   if ([_closureType isEnabled] ) {
        
        [self.delegate updateProgressDelegateWithValue:value andClosureType:_closureId andTarget:_targetName.text];
        
    }
    else {
        
        [self.delegate updateProgressDelegateWithValue:value andClosureType:-1 andTarget:@""];

    }
    
    if ([sender isKindOfClass:[UIView class]]) {
        [(UIView*)sender dismissPresentingPopup];
    }
    
}

- (IBAction)cancelAction:(id)sender {
    
    NSLog(@"cancel");
    if ([sender isKindOfClass:[UIView class]]) {
        [(UIView*)sender dismissPresentingPopup];
    }
    
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(nonnull UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return 100;
}


- (NSString *)pickerView:(UIPickerView *)thePickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    NSLog(@"%@",[NSString stringWithFormat:@"%ld %@",(long)row+1,@"%"]);
    return [NSString stringWithFormat:@"%ld %@",(long)row+1,@"%"];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    if (row == 99) {
        [_closureType setEnabled:YES];
       // [_targetName setUserInteractionEnabled:YES];
    }
    
    else {
        
        [_closureType setEnabled:NO];
        [_targetName setUserInteractionEnabled:NO];
    }
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    if (_closureId==640) {
       [_targetName setUserInteractionEnabled:YES];
    }
    
    
    return YES;
}
//-(BOOL)textViewShouldEndEditing:(SZTextView *)textView{
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
//
//    [self endEditing:YES];
//    return YES;
//}
//

//- (void)keyboardDidShow:(NSNotification *)notification
//{
//    CGRect screenRect = [[UIScreen mainScreen] bounds];
//    CGFloat screenWidth = screenRect.size.width;
//    CGFloat screenHeight = screenRect.size.height;
//
//    // Assign new frame to your view
//    [self setFrame:CGRectMake(screenRect.origin.x,screenRect.origin.y-20,screenWidth,screenHeight)]; //here taken -110 for example i.e. your view will be scrolled to -110. change its value according to your requirement.
//
//}
//
//-(void)keyboardDidHide:(NSNotification *)notification
//{
//    CGRect screenRect = [[UIScreen mainScreen] bounds];
//    CGFloat screenWidth = screenRect.size.width;
//    CGFloat screenHeight = screenRect.size.height;
//
//    [self setFrame:CGRectMake(screenRect.origin.x,screenRect.origin.y,screenWidth,screenHeight)];
//}

@end
