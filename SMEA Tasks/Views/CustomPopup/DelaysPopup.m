//
//  DelaysPopup.m
//  MCI-Tasks
//
//  Created by Mohammad Maswadeh on 1/24/18.
//  Copyright © 2018 BTIT. All rights reserved.
//

#import "DelaysPopup.h"
#import "MyTasksDetailsViewController.h"

@interface DelaysPopup () <UIPopoverControllerDelegate,UITextFieldDelegate,LGAlertViewDelegate,MyTaskDetailsViewDelegate>

@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UITextField *delayDate;
@property (weak, nonatomic) IBOutlet SZTextView *justificationText;

@property (weak, nonatomic) IBOutlet UIButton *saveBtn;
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property (strong, nonatomic) IBOutlet UIDatePicker *endDatePicker;
@property (nonatomic, strong) NSString *lastDate;

@property (nonatomic, strong) NSArray *uneditableTextFields;

@end

@implementation DelaysPopup
@synthesize delegate;



-(instancetype)initWithCoder:(NSCoder *)aDecoder {
    
    self = [super initWithCoder:aDecoder];
    if (self) {
        
        [self customInit];
    }
    
    return self;
}


-(instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        
        [self customInit];
    }
    
    return self;
}


-(void)customInit {
    
    [[NSBundle mainBundle] loadNibNamed:@"DelaysPopup" owner:self options:nil];
    [self.saveBtn setTitle:NSLocalizedString(@"Add", nil)forState:UIControlStateNormal];
     [self.cancelBtn setTitle:NSLocalizedString(@"cancel", nil) forState:UIControlStateNormal];
    self.delayDate.placeholder=NSLocalizedString(@"Date", nil);
    self.justificationText.placeholder=NSLocalizedString(@"DelayJustification", nil);
    self.uneditableTextFields = @[self.delayDate];
    self.delayDate.delegate = self;
    self.layer.cornerRadius = 10;
    self.clipsToBounds = YES;
    [self addSubview:self.contentView];
    self.contentView.frame = self.bounds;

}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    BOOL shouldEdit = ![self.uneditableTextFields containsObject:textField];
    if (textField == self.delayDate){
        
        [self addDelaydate];
    }
    
    return shouldEdit;
}


-(void)addDelaydate{
    
    
    NSString *lastDate = [self.delegate getLastDate];
    NSLog(@"date = %@",lastDate);
    
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd/MM/yyyy"];
    [formatter setTimeZone: [NSTimeZone timeZoneWithAbbreviation: @"GMT"]];

    NSDate *dueDate = [formatter dateFromString:lastDate];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *datestring = [dateFormatter stringFromDate:dueDate];
    NSLog(@"Formatted Date = %@",datestring);
    NSDate *formattedDate = [dateFormatter dateFromString:datestring];

    
    _endDatePicker = [UIDatePicker new];
    _endDatePicker.datePickerMode = UIDatePickerModeDate;
    _endDatePicker.minimumDate = formattedDate;
    
    _endDatePicker.frame = CGRectMake(0.0, 0.0, _endDatePicker.frame.size.width, 160.0);
    
    [[[LGAlertView alloc] initWithViewAndTitle:NSLocalizedString(@"EndingLabel", nil)
                                       message:NSLocalizedString(@"EndingDatePickerTitle", nil)
                                         style:LGAlertViewStyleActionSheet
                                          view:_endDatePicker
                                  buttonTitles:@[NSLocalizedString(@"Done", nil)]
                             cancelButtonTitle:NSLocalizedString(@"cancel", nil)
                        destructiveButtonTitle:nil
                                      delegate:self] showAnimated:YES completionHandler:nil];

    
}
- (IBAction)submitAction:(id)sender {
    
    [self.delegate addReasonOfDelayDelegateWithDate:_delayDate.text andJustification:_justificationText.text];
    if ([sender isKindOfClass:[UIView class]]) {
        [(UIView*)sender dismissPresentingPopup];
    }
    
}

- (IBAction)deleteAction:(id)sender {
    
    
    [self.delegate deleteDelayWithTaskId:@""];
    if ([sender isKindOfClass:[UIView class]]) {
        [(UIView*)sender dismissPresentingPopup];
    }
    
}

- (IBAction)cancelAction:(id)sender {
    
    if ([sender isKindOfClass:[UIView class]]) {
        [(UIView*)sender dismissPresentingPopup];
    }
    
}

- (void)alertView:(LGAlertView *)alertView clickedButtonAtIndex:(NSUInteger)index title:(nullable NSString *)title {
    NSLog(@"action {title: %@, index: %lu}", title, (long unsigned)index);
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"dd/MM/yyyy"];
                
        NSString *stringFromDate = [formatter stringFromDate:_endDatePicker.date];
        _delayDate.text = stringFromDate;
 
}

- (void)alertViewCancelled:(LGAlertView *)alertView {
    NSLog(@"cancel");
}

- (void)alertViewDestructed:(LGAlertView *)alertView {
    NSLog(@"destructive");
}


@end
