//
//  DelaysPopup.h
//  MCI-Tasks
//
//  Created by Mohammad Maswadeh on 1/24/18.
//  Copyright © 2018 BTIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SZTextView.h"
#import <LGAlertView/LGAlertView.h>
#import "PopupView.h"

//@import PopupKit;

@protocol DelaysPopupDelegate <NSObject>
@optional

-(void)addReasonOfDelayDelegateWithDate:(NSString*)date andJustification:(NSString *)justification;
-(void)deleteDelayWithTaskId:(NSString *)taskId;
-(NSString *)getLastDate;

@end

@interface DelaysPopup : UIView {
    
    id <DelaysPopupDelegate> delegate;
}

@property (strong, nonatomic) id <DelaysPopupDelegate> delegate;

@end
