//
//  LoginpageViewController.h
//  PMO Dashboard
//
//  Created by Blessed Tree IT on 2/20/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SCLAlertView.h"
#import "MBProgressHUD.h"
#import "languageHelper.h"
@interface LoginpageViewController : UIViewController <UITextFieldDelegate>

{

IBOutlet UITextField *Username;
IBOutlet UITextField *password;

}
@property (strong, nonatomic) IBOutlet UITextField *textfieldPass;
@property (strong, nonatomic) IBOutlet UITextField *textfieldUser;
@property (strong, nonatomic) IBOutlet UILabel *passwordLbl;
@property (strong, nonatomic) IBOutlet UILabel *userNameLbl;
@property (weak, nonatomic) IBOutlet UIView *whiteView;

@property (strong, nonatomic) IBOutlet UIButton *loginBtn;
@property (nonatomic, strong) languageHelper * languageSettings;


- (IBAction)loginBtn:(id)sender;

@end
