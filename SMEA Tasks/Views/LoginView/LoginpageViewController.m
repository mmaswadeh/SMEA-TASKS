//
//  LoginpageViewController.m
//  PMO Dashboard
//
//  Created by Blessed Tree IT on 2/20/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import "LoginpageViewController.h"
#import "loginRequest.h"
#import "serialNumber.h"
#import "profileInfo.h"
#import "TasksHolderViewController.h"
#import "MainViewController.h"

@interface LoginpageViewController ()

{
    NSInteger  LangID;
    NSString * serialNo;
    NSString * notificationToken;
    NSUserDefaults *profile;
     NSUserDefaults *loginTokenSaved;   
    profileInfo*UserInfo;
    NSString * loginToken;
}

@end

@implementation LoginpageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.passwordLbl.text=NSLocalizedString(@"Passwordlbl", nil);
    self.userNameLbl.text=NSLocalizedString(@"UserNamelbl",nil);
    [self.loginBtn setTitle:NSLocalizedString(@"Signin", nil) forState:UIControlStateNormal];

    self.textfieldPass.placeholder=NSLocalizedString(@"Password", nil);
    self.textfieldUser.placeholder=NSLocalizedString(@"userName", nil);
    
    
    //MARK : lanaguage check
    // Do any additional setup after loading the view.
    NSArray *languageArray = [NSLocale preferredLanguages];
    NSString *language = [languageArray objectAtIndex:0];
    
    
   NSArray * indexofDash = [language componentsSeparatedByString:@"-"];
    NSString *languageWithoutRegionCode =[indexofDash objectAtIndex:0];

    
    if([languageWithoutRegionCode isEqualToString:@"en"])
    {
        LangID=2;
    }
    else  if([languageWithoutRegionCode isEqualToString:@"ar"])
    {
        LangID=1;
        self.passwordLbl.frame=CGRectMake(self.view.frame.size.width-85, 249, 80, 21);
        self.userNameLbl.frame=CGRectMake(self.view.frame.size.width-90, 174, 88, 21);
        
        self.textfieldPass.frame=CGRectMake(self.userNameLbl.frame.size.width-60, 244, 198, 30);
        self.textfieldUser.frame=CGRectMake(self.userNameLbl.frame.size.width-60, 170, 198, 30);
    }
    else
    { LangID=2;
    }
    

    
    NSString *deviceSpecs =
    [NSString stringWithFormat:@"%@ - %li",
     language, (long)LangID];
     serialNumber *getUDID=[[serialNumber alloc ]init];
    serialNo =[NSString stringWithFormat:@"%@", getUDID.serialNumber];
    NSLog(@"Device Specs --> %@",deviceSpecs);  // Do any additional setup after loading the view.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)loginBtn:(id)sender {
    NSLog(@"Device Specs --> %ld serial number %@",(long)LangID ,serialNo);
    
  notificationToken =@"a088a50a601d7c5dc4bbf4572b5cfb208dc7556809f0f1f8ba2e7bc1de4106a4";

    if ([Username.text isEqualToString:@""]|| [password.text isEqualToString:@""])
    {
        SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
        [alert showError:@"Failed" subTitle:NSLocalizedString(@"LoginFaild", nil) closeButtonTitle:@"Ok" duration:0.0f];
        

    }
    else{
        
         MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [loginRequest newLoginWith:Username.text Passwordwith:password.text Grantwith:@"password" success:^(NSString *emailStr, NSString *surenameStr, NSString *isSuccessStr, NSString *errorStr, NSString* userIdIn, NSString *accessToken, NSString *tokenType) {
       
            if ([isSuccessStr isEqualToString:@"true"]) {
                loginToken=[NSString stringWithFormat:@"%@ %@",accessToken,tokenType];
                loginTokenSaved=[NSUserDefaults standardUserDefaults];
                [loginTokenSaved setObject:loginToken forKey:@"token"];
                profile= [NSUserDefaults standardUserDefaults];
                [profile setObject:@"yes" forKey:@"loggedUser"];
                [profile setInteger:[userIdIn intValue] forKey:@"UserID"];
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"NavigationController"];
            
            [navigationController setViewControllers:@[[storyboard instantiateViewControllerWithIdentifier:@"DashboardHolderViewController"]]];
            
            MainViewController *mainViewController = [storyboard instantiateInitialViewController];
            mainViewController.rootViewController = navigationController;
            
            UIWindow *window = UIApplication.sharedApplication.delegate.window;
            window.rootViewController = mainViewController;
            [UIView transitionWithView:window
                              duration:0.3
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:nil
                            completion:nil];
            
                [hud hideAnimated:YES];

   
            }
            else {
                [hud hideAnimated:YES];
                SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
                [alert showError:@"Failed" subTitle:NSLocalizedString(@"LoginFaild", nil) closeButtonTitle:NSLocalizedString(@"Done", nil) duration:0.0f];
            }
                 }
            
         failure:^(NSError *error) {
            
            [hud hideAnimated:YES];
            SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
            [alert showError:@"Failed" subTitle:NSLocalizedString(@"LoginFaild", nil) closeButtonTitle:NSLocalizedString(@"Done", nil) duration:0.0f];
        }];

    }

}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
