//
//  LeftMenuViewController.h
//  PMO Dashboard
//
//  Created by Blessed Tree IT on 1/12/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "languageHelper.h"
#import "NetworkOperationManager.h"

@interface LeftMenuViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property (strong, nonatomic) IBOutlet UIImageView *userImage;
@property (strong, nonatomic) IBOutlet UILabel *userName;

@property (strong, nonatomic) IBOutlet UITableView *leftMenuTable;

@property (strong, nonatomic) NSArray *leftmenuData;
@property (strong, nonatomic) NSArray *leftmenuImages;

@property (strong, nonatomic) IBOutlet UIButton *logoutLabelStr;
@property (strong, nonatomic) IBOutlet UIImageView *logoutImage;
@property (nonatomic, strong) languageHelper * languageSettings;


- (IBAction)logout:(id)sender;
@end
