//
//  LeftMenuCustomCell.m
//  PMO
//
//  Created by Blessed Tree IT on 12/26/16.
//  Copyright © 2016 Blessed Tree IT. All rights reserved.
//

#import "LeftMenuCustomCell.h"

@implementation LeftMenuCustomCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
