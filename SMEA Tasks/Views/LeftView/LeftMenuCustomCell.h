//
//  LeftMenuCustomCell.h
//  PMO
//
//  Created by Blessed Tree IT on 12/26/16.
//  Copyright © 2016 Blessed Tree IT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LeftMenuCustomCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *cellName;
@property (weak, nonatomic) IBOutlet UIImageView *cellImage;
@end
