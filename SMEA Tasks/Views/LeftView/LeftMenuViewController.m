//
//  LeftMenuViewController.m
//  PMO Dashboard
//
//  Created by Blessed Tree IT on 1/12/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import "LeftMenuViewController.h"
#import "LeftMenuCustomCell.h"
#import "MainViewController.h"
#import "UIViewController+LGSideMenuController.h"


@interface LeftMenuViewController (){
     NSArray* allInitiativeList;
    
}

@end

@implementation LeftMenuViewController
@synthesize leftmenuData,leftMenuTable,leftmenuImages,userImage,userName;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    leftMenuTable.delegate = self;
    leftMenuTable.dataSource = self;

    NSUserDefaults*firstName = [NSUserDefaults standardUserDefaults];
    NSString *firstNameString = [firstName stringForKey:@"FirstName"];
    
    [self.logoutLabelStr  setTitle:NSLocalizedString(@"logout", nil) forState:UIControlStateNormal];
    NSString * language = [[NSString alloc]init];
    
    _languageSettings = [[languageHelper alloc]init];
    language= [_languageSettings checkLanguages];
        if ([language isEqualToString:@"ar"]) {
        
    self.logoutImage.transform=CGAffineTransformMakeScale(-1,1);
        
    }
    //
        leftmenuData = [NSArray arrayWithObjects:NSLocalizedString(@"MyTasksDashboard",nil),
                        NSLocalizedString(@"TasksDashboard",nil),
                       // NSLocalizedString(@"AllTasksDashboard",nil),
                        NSLocalizedString(@"Add Task",nil),
                        NSLocalizedString(@"TasksList",nil),
                        NSLocalizedString(@"logout", nil), nil];
        

        leftmenuImages = [NSArray arrayWithObjects:@"chart-pie",@"chart-pie"
                          //,@"dashboard"
                          ,@"plus",@"clipboard",@"menuItem4", nil];
        
    
    userImage.layer.cornerRadius = userImage.frame.size.height / 2;
    userImage.layer.masksToBounds = true;

    userName.text=[NSString stringWithFormat:@"%@",firstNameString];
    //userName.text=@"User";

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return leftmenuData.count; //3;
}




- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"leftMenuCell";
    LeftMenuCustomCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil)
    {
        cell = [[LeftMenuCustomCell alloc]
                initWithStyle:UITableViewCellStyleDefault reuseIdentifier: CellIdentifier];
        
    }
    NSString *cellName = [leftmenuData objectAtIndex:indexPath.row];
    NSString *cellImage = [leftmenuImages objectAtIndex:indexPath.row];
    
    cell.cellName.text = cellName;
    NSString * language = [[NSString alloc]init];
    
    _languageSettings = [[languageHelper alloc]init];
    language= [_languageSettings checkLanguages];
    if ([language isEqualToString:@"ar"]) {
      leftMenuTable.frame = CGRectMake(-30.0,190, leftMenuTable.frame.size.width, leftMenuTable.frame.size.height);
     cell.contentView.transform = CGAffineTransformMakeScale(-1,1);

        cell.cellName.transform = CGAffineTransformMakeScale(-1,1);
        cell.cellName.textAlignment = NSTextAlignmentRight;
        
    }

    cell.cellImage.image = [UIImage imageNamed:cellImage];
    
        return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
 
    if (indexPath.row == 0 )
    {
        NSLog(@"Dashboard");
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"NavigationController"];
        
        [navigationController setViewControllers:@[[storyboard instantiateViewControllerWithIdentifier:@"MyTasksDashboardViewController"]]];
        
        [[NetworkOperationManager sharedObjectLogin]setPmoBool:YES];
        MainViewController *mainViewController = [storyboard instantiateInitialViewController];
        mainViewController.rootViewController = navigationController;
        
        UIWindow *window = UIApplication.sharedApplication.delegate.window;
        window.rootViewController = mainViewController;
        
        [UIView transitionWithView:window
                          duration:0.3
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:nil
                        completion:nil];
    }
   else if (indexPath.row == 1 )

    {
        NSLog(@"Home");
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"NavigationController"];
        
        [navigationController setViewControllers:@[[storyboard instantiateViewControllerWithIdentifier:@"TasksDashboardViewController"]]];
        
          [[NetworkOperationManager sharedObjectLogin]setPmoBool:YES];
        MainViewController *mainViewController = [storyboard instantiateInitialViewController];
        mainViewController.rootViewController = navigationController;
        
        UIWindow *window = UIApplication.sharedApplication.delegate.window;
        window.rootViewController = mainViewController;
        
        [UIView transitionWithView:window
                          duration:0.3
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:nil
                        completion:nil];
    }
    
//   else if (indexPath.row == 2 )
//
//   {
//       NSLog(@"Home");
//       UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//       UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"NavigationController"];
//
//       [navigationController setViewControllers:@[[storyboard instantiateViewControllerWithIdentifier:@"AllTasksDashboardViewController"]]];
//
//       [[NetworkOperationManager sharedObjectLogin]setPmoBool:YES];
//       MainViewController *mainViewController = [storyboard instantiateInitialViewController];
//       mainViewController.rootViewController = navigationController;
//
//       UIWindow *window = UIApplication.sharedApplication.delegate.window;
//       window.rootViewController = mainViewController;
//
//       [UIView transitionWithView:window
//                         duration:0.3
//                          options:UIViewAnimationOptionTransitionCrossDissolve
//                       animations:nil
//                       completion:nil];
//   }
//
    
    
      else  if (indexPath.row == 2 )
            
        {


            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"NavigationController"];
            
            [navigationController setViewControllers:@[[storyboard instantiateViewControllerWithIdentifier:@"AddTaskViewController"]]];
            
            [[NetworkOperationManager sharedObjectLogin]setPmoBool:YES];
            MainViewController *mainViewController = [storyboard instantiateInitialViewController];
            mainViewController.rootViewController = navigationController;
            
            UIWindow *window = UIApplication.sharedApplication.delegate.window;
            window.rootViewController = mainViewController;
            
            [UIView transitionWithView:window
                              duration:0.3
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:nil
                            completion:nil];
            
        }
      else  if (indexPath.row == 3 )
          
      {
          [[NetworkOperationManager sharedObjectLogin]setPmoBool:YES];
          
          
          UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
          UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"NavigationController"];
          
          [navigationController setViewControllers:@[[storyboard instantiateViewControllerWithIdentifier:@"TasksHolderViewController"]]];
          
          [[NetworkOperationManager sharedObjectLogin]setPmoBool:YES];
          MainViewController *mainViewController = [storyboard instantiateInitialViewController];
          mainViewController.rootViewController = navigationController;
          
          UIWindow *window = UIApplication.sharedApplication.delegate.window;
          window.rootViewController = mainViewController;
          
          [UIView transitionWithView:window
                            duration:0.3
                             options:UIViewAnimationOptionTransitionCrossDissolve
                          animations:nil
                          completion:nil];
          
      }
    else if(indexPath.row == 4){
        NSLog(@"Logout");
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"loggedUser"];
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"NavigationController"];
        [navigationController setViewControllers:@[[storyboard instantiateViewControllerWithIdentifier:@"LoginpageViewController"]]];
        UIWindow *window = UIApplication.sharedApplication.delegate.window;
        window.rootViewController = navigationController;
        [UIView transitionWithView:window
                          duration:0.3
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:nil
                        completion:nil];
        
        
        
}
}

- (IBAction)logout:(id)sender
{
    NSLog(@"Logout");
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"loggedUser"];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"NavigationController"];
    [navigationController setViewControllers:@[[storyboard instantiateViewControllerWithIdentifier:@"LoginpageViewController"]]];
    UIWindow *window = UIApplication.sharedApplication.delegate.window;
    window.rootViewController = navigationController;
    [UIView transitionWithView:window
                      duration:0.3
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:nil
                    completion:nil];
    
    
    
}



@end
