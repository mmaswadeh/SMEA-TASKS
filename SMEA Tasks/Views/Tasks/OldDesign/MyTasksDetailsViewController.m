//
//  MyTasksDetailsViewController.m
//  MCI-Tasks
//
//  Created by Mohammad Maswadeh on 1/23/18.
//  Copyright © 2018 BTIT. All rights reserved.
//

#import "MyTasksDetailsViewController.h"
#import "Tasks.h"
#import "EditTaskViewController.h"
#import "TasksResquest.h"
#import "MainViewController.h"
#import "ProgressChart.h"
#import "PMOCircleChart.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "IDMPhoto.h"
#import "IDMPhotoBrowser.h"
#import "SCLAlertView.h"
#import "StatusCell.h"
#import "DescriptionCell.h"
#import "DelaysCell.h"
#import "Delays.h"
#import "CluoserCell.h"
#import "DelaysPopup.h"
#import "UpdateDelayPopup.h"
#import "MBProgressHUD.h"
#import "TaskDetailsRequest.h"

@interface MyTasksDetailsViewController () <DelaysPopupDelegate,UpdateDelayPopupDelegate>

@end

@implementation MyTasksDetailsViewController
@synthesize myTaskDetailsDelegate;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.myTaskDetailsDelegate sendDateToPopup:@"Test"];
    self.tableView.delegate=self;
    self.tableView.dataSource=self;
    self.tableView.estimatedRowHeight = 70.0;
    self.view.autoresizesSubviews = YES;
    
    if (_percent ==100) {
        [_EditBtn  setUserInteractionEnabled:NO];
        [_EditBtn setEnabled:false];
      [_EditBtn setBackgroundColor:[UIColor lightGrayColor]];
    }
    
    self.navigationItem.title =NSLocalizedString(@"Details", nil);
    
    [_EditBtn setTitle:NSLocalizedString(@"UpdateProgress", nil) forState:UIControlStateNormal];
    [_DeleteBtn setTitle:NSLocalizedString(@"ReasonOfDelayes", nil) forState:UIControlStateNormal];
    
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapOnImage:)];
    [self.myTaskDetailsImage addGestureRecognizer:tapGestureRecognizer];
    self.myTaskDetailsImage.userInteractionEnabled = YES;
    
    
    
    _backgroundView.layer.cornerRadius = 5.0; // set as you want.
    _backgroundView.layer.borderColor = [UIColor lightGrayColor].CGColor; // set color as you want.
    _backgroundView.layer.borderWidth = 0.5;
    
 
    PMOCircleChart  *circleChartView;
    
    circleChartView = [[PMOCircleChart alloc]initWithFrame:CGRectMake(0.0,0.0,40.0,40.0)
                                                     total:@100
                                                   current:@(_percent)
                                                 clockwise:YES
                                                    shadow:YES
                                               shadowColor:[UIColor grayColor]];
    
    
    circleChartView.backgroundColor = [UIColor clearColor];
    
    [circleChartView setStrokeColor:_color];
    [circleChartView strokeChart];
    
    [self.chartView addSubview:circleChartView];
    
        
    [self.myTaskDetailsImage sd_setImageWithURL:[NSURL URLWithString:_filesUrl] placeholderImage:[UIImage imageNamed:@"No Image"]];
    
    
    // Do any additional setup after loading the view.
}

- (IBAction)didTapOnImage:(id)sender {
    
    if (_filesUrl) {
        
        NSArray *photosURL = @[[NSURL URLWithString:_filesUrl]];
        
        // Create an array to store IDMPhoto objects
        
        
        // Or use this constructor to receive an NSArray of IDMPhoto objects from your NSURL objects
        NSArray *photos = [IDMPhoto photosWithURLs:photosURL];
        
        IDMPhotoBrowser *browser = [[IDMPhotoBrowser alloc] initWithPhotos:photos animatedFromView:self.view];
        browser.displayArrowButton = NO;
        browser.displayActionButton = NO;
        
        [self presentViewController:browser animated:YES completion:nil];
    }
    
    
    
    
}

-(void)alertView:(LGAlertView *)alertView clickedButtonAtIndex:(NSUInteger)index title:(nullable NSString *)title {
    
    if (index == 0 ) {
        
        
        [TasksResquest deleteTasksWithId:_myTaskId success:^(NSDictionary *comments) {
            
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"NavigationController"];
            
            [navigationController setViewControllers:@[[storyboard instantiateViewControllerWithIdentifier:@"TasksHolderViewController"]]];
            
            MainViewController *mainViewController = [storyboard instantiateInitialViewController];
            mainViewController.rootViewController = navigationController;
            
            UIWindow *window = UIApplication.sharedApplication.delegate.window;
            window.rootViewController = mainViewController;
            [UIView transitionWithView:window
                              duration:0.3
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:nil
                            completion:nil];
            
            
        } failure:^(NSError *error) {
            
            NSLog(@"Error = %@",error);
            
        }];
    }
    
    else {
        
        NSLog(@"No");
        
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)deleteAction:(id)sender {

    DelaysPopup *customPopupView = [[DelaysPopup alloc] init];
    customPopupView.frame = CGRectMake(0.0, 0.0, 300.0, 250.0);
    customPopupView.delegate=self;
    
    PopupView *popup = [PopupView popupViewWithContentView:customPopupView];
    [popup show];

    
}

-(void)deleteButton {
    
    [TasksResquest deleteTasksWithId:_myTaskId success:^(NSDictionary *comments) {
        
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"NavigationController"];
        
        [navigationController setViewControllers:@[[storyboard instantiateViewControllerWithIdentifier:@"TasksHolderViewController"]]];
        
        MainViewController *mainViewController = [storyboard instantiateInitialViewController];
        mainViewController.rootViewController = navigationController;
        
        UIWindow *window = UIApplication.sharedApplication.delegate.window;
        window.rootViewController = mainViewController;
        [UIView transitionWithView:window
                          duration:0.3
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:nil
                        completion:nil];
        
        
    } failure:^(NSError *error) {
        
        NSLog(@"Error = %@",error);
        
    }];
}

- (IBAction)editAction:(id)sender {
    
    UpdateProgressPopup *customPopupView = [[UpdateProgressPopup alloc] init];
    customPopupView.frame = CGRectMake(0.0, 10.0, 300.0, 350.0);
    customPopupView.delegate=self;
    
    PopupView *popup = [PopupView popupViewWithContentView:customPopupView];
    
    [popup show];
    
    NSLog(@"Did Press on update");
}

-(void)updateDelayDelegateWithJustification:(NSString *)justification {
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSInteger userId = [userDefaults integerForKey:@"UserID"];
    
    [TasksResquest updateTaskDelayWithTaskId:_delayId andJustification:justification andUserid:userId success:^(NSDictionary *response) {

        [self updateTaskFinishedWithTaskId:_myTaskId];

    } failure:^(NSError *error) {
        

    }];
}

-(NSString *)getLastDate {

    NSString *last;

    if ([_lastDelayDate isEqualToString:@""]) {
        last = _endingDate;
    }
    else {
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"MM/dd/yyyy"];
        [formatter setTimeZone: [NSTimeZone timeZoneWithAbbreviation: @"GMT"]];
        
        NSDate *dueDate = [formatter dateFromString:_lastDelayDate];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd/MM/yyyy"];
        NSString *datestring = [dateFormatter stringFromDate:dueDate];
        NSLog(@"Formatted Date = %@",datestring);
        
        last = datestring;
    }
    return last;
}

-(NSInteger)getProgressIndex {
    
    return _percent;
}
-(void)updateProgressDelegateWithValue:(NSString*)value andClosureType:(NSInteger)closureType andTarget:(NSString*)target {
    
    
   
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSInteger userId = [userDefaults integerForKey:@"UserID"];
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:_tableView animated:YES];
    if ([value isEqualToString:@"100"]) {
        
        
        if (closureType == 0   && [target isEqualToString:@""]){
            
           
                [hud hideAnimated:YES];
                SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
                [alert showError:@"Failed" subTitle:NSLocalizedString(@"mustnotBeEmpty", nil) closeButtonTitle:NSLocalizedString(@"Done", nil) duration:0.0f];
           
            
            
        }
        else if(closureType == 640 ){
            
            [TasksResquest editProgressAndClosureWithTaskId:_myTaskId andProgress:value andUserid:userId andClosureType:[NSString stringWithFormat:@"%ld",(long)closureType] andTranferTargetName:target success:^(NSDictionary *response) {
                [hud hideAnimated:YES];
                
                [self updateTaskFinishedWithTaskId:_myTaskId];
                
            } failure:^(NSError *error) {
                [hud hideAnimated:YES];
                
            }];
            
        }
        else {
            
            
            
            [TasksResquest editProgressAndClosureWithTaskId:_myTaskId andProgress:value andUserid:userId andClosureType:[NSString stringWithFormat:@"%ld",(long)closureType] andTranferTargetName:target success:^(NSDictionary *response) {
                [hud hideAnimated:YES];
                
                [self updateTaskFinishedWithTaskId:_myTaskId];
                
            } failure:^(NSError *error) {
                [hud hideAnimated:YES];
                
            }];
            
            
        }
    }
    else
    if (closureType == -1 || [target isEqualToString:@""]) {
        
        [TasksResquest editProgressAndClosureWithTaskId:_myTaskId andProgress:value andUserid:userId andClosureType:@"" andTranferTargetName:@"" success:^(NSDictionary *response) {
            
            [self updateTaskFinishedWithTaskId:_myTaskId];
            [hud hideAnimated:YES];

        } failure:^(NSError *error) {
            
            [hud hideAnimated:YES];

        }];
        
    }
    
    else {
        
        
        
        [TasksResquest editProgressAndClosureWithTaskId:_myTaskId andProgress:value andUserid:userId andClosureType:[NSString stringWithFormat:@"%ld",(long)closureType] andTranferTargetName:target success:^(NSDictionary *response) {
            [hud hideAnimated:YES];

            [self updateTaskFinishedWithTaskId:_myTaskId];

        } failure:^(NSError *error) {
            [hud hideAnimated:YES];

        }];
        
        
    }
}

-(void)addReasonOfDelayDelegateWithDate:(NSString*)date andJustification:(NSString *)justification {
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSInteger userId = [userDefaults integerForKey:@"UserID"];
    

    [TasksResquest saveTaskDelayWithTaskId:_myTaskId andJustification:justification andUserid:userId andNewEndDate:date success:^(NSDictionary *response) {

        [self updateTaskFinishedWithTaskId:_myTaskId];

    } failure:^(NSError *error) {
        

    }];
}

-(void)deleteDelayWithTaskId:(NSString *)taskId {
    

    [TasksResquest deleteDelayWithId:_delayId success:^(NSDictionary *response) {
        
        [self updateTaskFinishedWithTaskId:_myTaskId];
        [self.tableView reloadData];
    } failure:^(NSError *error) {

    }];
    

}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 4;
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    
    if (section==0){
        return 1;
        
    }
    if (section==1){
        return 1;
        
    }
    if (section==2){
        return 1;
    }
    else
        return self.delaysArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *mainCell;
    
    if (indexPath.row==0 && indexPath.section==0) {
        StatusCell* cell=[tableView dequeueReusableCellWithIdentifier:@"headerNotes"];
        [cell.titleLabel setText:[_myTaskTitle uppercaseString]];
        
        cell.startDateLabel.text = [NSString stringWithFormat:@"%@ : %@ %@ : %@",NSLocalizedString(@"From", nil),_startingDate,NSLocalizedString(@"To", nil),_endingDate];
        cell.assignToLabel.text = _assigneeUserName;
        cell.taskStatusView.backgroundColor = _color;
        cell.statusName.text = _myTaskStatusName;
        cell.agencyLabel.text=_agencyName;
        cell.percentageLabel.text =[NSString stringWithFormat:@"(%ld%@",(long)_percent,@"%)"];
        cell.sourceName.text =[NSString stringWithFormat:@"Source Name:%@",_sourceName];
        cell.meetingNo.text=[NSString stringWithFormat:@"Meeting No.:%@",_MeetingNumberLbl];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setTimeZone: [NSTimeZone timeZoneWithAbbreviation: @"GMT"]];
        [dateFormat setDateFormat:@"dd/MM/yyyy"];
        
        NSLog(@"Last Day = %@",_lastDelayDate);
        NSLog(@"Ending date = %@",_endingDate);
        NSDate *dueDate;
        if ([_lastDelayDate isEqualToString:@""]) {
            
            dueDate = [dateFormat dateFromString:_endingDate];

        }
        
        else {
            
          dueDate = [dateFormat dateFromString:_lastDelayDate];

        }
        NSDate *today = [NSDate date];
        
        if (_percent < 100) {
          
            NSCalendar *calendar = [NSCalendar currentCalendar];
            NSInteger comps = (NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear);

            NSDateComponents *date1Components = [calendar components:comps
                                                            fromDate: dueDate];
            NSDateComponents *date2Components = [calendar components:comps
                                                            fromDate: today];

            dueDate = [calendar dateFromComponents:date1Components];
            today = [calendar dateFromComponents:date2Components];

            NSComparisonResult result = [dueDate compare:today];
            if (result == NSOrderedAscending) {
     

            } else if (result == NSOrderedDescending) {

                [self.DeleteBtn setEnabled:NO];
                [self.DeleteBtn setBackgroundColor:[UIColor lightGrayColor]];
                
                
            }  else {
                //the same
                [self.DeleteBtn setEnabled:NO];
                [self.DeleteBtn setBackgroundColor:[UIColor lightGrayColor]];
            }
            
            
            
//            if ([dueDate compare:today] == NSOrderedDescending) {
//
//
//                [self.DeleteBtn setEnabled:NO];
//                [self.DeleteBtn setBackgroundColor:[UIColor lightGrayColor]];
//            }
//
//            if ([dueDate compare:today] == NSOrderedSame) {
//
//
//                [self.DeleteBtn setEnabled:NO];
//                [self.DeleteBtn setBackgroundColor:[UIColor lightGrayColor]];
//            }
//
        }
       
        mainCell=cell;
        
    }
    
    if (indexPath.row==0 && indexPath.section==1) {
        DescriptionCell* cell=[tableView dequeueReusableCellWithIdentifier:@"description"];
        
        cell.descriptionLabel.text = _myTaskDescription;
        
        if ([_myTaskDescription isEqualToString:@""]) {
            
            [cell.descriptionLabel setText:[[NSString stringWithFormat:@"%@",NSLocalizedString(@"NoDescription", nil)] uppercaseString]];
            cell.descriptionLabel.textAlignment = NSTextAlignmentCenter;
            
        }
        mainCell=cell;
    }
    
    if (indexPath.row==0 && indexPath.section==2) {
        CluoserCell* cell=[tableView dequeueReusableCellWithIdentifier:@"ClouserMessage"];
        cell.cluoserType.text=_ClouserType;
        cell.cluoserName.text=_ClouserMessage;
        
        mainCell=cell;
    }
    else  if (indexPath.section==3) {
        DelaysCell* cell=[tableView dequeueReusableCellWithIdentifier:@"delays"];
        Delays* item =[self.delaysArr objectAtIndex:indexPath.row];
        cell.delayDate.text=item.NewEndDate;
        cell.resoneText.text=item.Justification;
        mainCell=cell;
        
    }
    
    
    
    
    return mainCell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    int x = 0 ;
    
    if  (indexPath.section == 0 ) {
        x = 275;
    }
    
    
    else if (indexPath.section == 1) {
        
        x= UITableViewAutomaticDimension;
    }
    
    else  if (indexPath.section ==2){
        
        
        if([self.ClouserMessage isEqualToString:@""] && [self.ClouserType isEqualToString:@""])
        {
            x=0;
            
            
        }
        else{
            x= UITableViewAutomaticDimension;

        }
        
    }
    else  if (indexPath.section ==3){
        
        x= 125;
        
        
    }
    return x;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    if (indexPath.section == 3) {
        
        Delays* item =[self.delaysArr objectAtIndex:indexPath.row];
        _delayId = item.Id;
        UpdateDelayPopup *customPopupView = [[UpdateDelayPopup alloc] init];
        customPopupView.frame = CGRectMake(0.0, 0.0, 300.0, 300.0);
        customPopupView.delegate=self;
        
        PopupView *popup = [PopupView popupViewWithContentView:customPopupView];
        
        [popup show];
        
    }
}

-(void)updateTaskFinishedWithTaskId:(NSInteger)finishedTaskId {
    
    [TaskDetailsRequest getTaskDetailsWithID:finishedTaskId sucess:^(Tasks *tasksitem) {
        
        _ClouserType=tasksitem.Clousertype;
        _ClouserMessage=tasksitem.ClouserMessage;
        _delaysArr=tasksitem.Delays;
        _myTaskTitle = tasksitem.title;
        _myTaskDescription = tasksitem.taskDescription;
        _startingDate = tasksitem.startingDate;
        _endingDate = tasksitem.endingDate;
        _assigneeUserName = tasksitem.assigneeUserName;
        _myTaskId = tasksitem.taskId;
        _filesUrl = tasksitem.filesUrl;
        _percent = tasksitem.completePercentage;
        _color = tasksitem.taskColor;
        _userID = tasksitem.AssigneeUserId;
        _myTaskStatusName = tasksitem.statusText;
        _agencyName=tasksitem.Agency.name;
        _agencyId = tasksitem.Agency.agencyId;
        _sourceId=tasksitem.Sources.sourceId;
        _sourceName=tasksitem.Sources.sourceName;
        _MeetingNumberLbl=tasksitem.MeetingNumber;
        
        if (_percent ==100) {
            [_EditBtn  setUserInteractionEnabled:NO];
            [_EditBtn setEnabled:false];
            [_EditBtn setBackgroundColor:[UIColor lightGrayColor]];
        }
        [self.tableView reloadData];

    } failure:^(NSError *error) {
        
    
    }];
}

@end
