//
//  EditTaskViewController.h
//  PMO-BTIT
//
//  Created by Mohammad Maswadeh on 8/6/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SZTextView.h"
#import <LGAlertView/LGAlertView.h>
#import "PMCalendar.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "IDMPhoto.h"
#import "IDMPhotoBrowser.h"

@interface EditTaskViewController : UIViewController <UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,LGAlertViewDelegate,PMCalendarControllerDelegate,UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *taskImage;
@property (weak, nonatomic) IBOutlet UITextField *taskTitleTF;
@property (weak, nonatomic) IBOutlet SZTextView *taskDescreption;
@property (weak, nonatomic) IBOutlet UITextField *startDateTF;
@property (weak, nonatomic) IBOutlet UITextField *endDateTF;
@property (weak, nonatomic) IBOutlet UITextField *assigneeTF;
@property (weak, nonatomic) IBOutlet UITextField *agencyTF;
@property (weak, nonatomic) IBOutlet UITextField *sourceTF;
@property (weak, nonatomic) IBOutlet UITextField *meetingNumberTF;

@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;



@property (strong, nonatomic) IBOutlet UIDatePicker *startDatePicker;
@property (strong, nonatomic) IBOutlet UIDatePicker *endDatePicker;
@property (strong,nonatomic)  NSArray *agencyArray;

@property (strong,nonatomic)  NSArray *idsAgency;
@property (strong,nonatomic)  NSArray *usersArray;
@property (strong,nonatomic)  NSArray *ids;
@property (nonatomic, assign) NSInteger userId;
@property (nonatomic, assign) NSInteger assigneeId;

@property (nonatomic, assign) NSInteger agencyId;

@property (nonatomic, assign) NSInteger taskId;
@property (nonatomic, strong) NSString *imagePath;
@property (weak, nonatomic) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet UIButton *SaveBtn;
@property (weak, nonatomic) IBOutlet UILabel *selectImageLabel;

@property (weak, nonatomic) IBOutlet UIButton *editButton;
- (IBAction)editImageAction:(id)sender;

- (IBAction)discardAction:(id)sender;

- (IBAction)editTask:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *zoomImage;
- (IBAction)zoomAction:(id)sender;

@property (nonatomic, strong) NSString *taskTitle;
@property (nonatomic, strong) NSString *assigneeUserName;
@property (nonatomic, strong) NSString *filesUrl;
@property (nonatomic, strong) NSString *startingDate;
@property (nonatomic, strong) NSString *endingDate;
@property (nonatomic, strong) NSString *taskDescription;
@property (nonatomic, strong) NSString *agencyName;

@property (strong,nonatomic)  NSArray *idsSource;
@property (strong,nonatomic)  NSArray *sourcesArray;

@property (nonatomic, assign) NSInteger sourceId;
@property (nonatomic, strong) NSString * sourceName;
@property (nonatomic, strong) NSString * MeetingNumberLbl;
- (IBAction)deleteAction:(id)sender;



@end
