//
//  MyTasksViewController.h
//  MCI-Tasks
//
//  Created by Mohammad Maswadeh on 1/23/18.
//  Copyright © 2018 BTIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIViewController+LGSideMenuController.h"
#import "languageHelper.h"
#import "CAPSPageMenu.h"


@protocol MyTasksViewDelegate <NSObject>

@optional

- (void)didPressMyTask:(NSInteger)TaskID andMyTaskArray:(NSArray*)taskArr;

@end


@interface MyTasksViewController : UIViewController < UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate,CAPSPageMenuDelegate>

@property (weak, nonatomic) IBOutlet UITableView *myTasksTableView;

@property (nonatomic, strong) languageHelper * languageSettings;

@property (strong,nonatomic)  NSArray *myTasksArray;
@property(nonatomic,assign)   NSInteger  MYStatusLkpIdtask;
@property (strong,nonatomic)  NSMutableArray *filteredContentList;
@property(weak,nonatomic)   id <MyTasksViewDelegate> myTaskDelegate;


@property (strong, nonatomic)   NSMutableArray *searchResult;

@end
