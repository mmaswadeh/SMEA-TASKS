//
//  OutboxViewController.h
//  PMO-BTIT
//
//  Created by Mohammad Maswadeh on 8/21/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DBManager.h"

@interface OutboxViewController : UIViewController <UITableViewDelegate,UITableViewDataSource>


@property (nonatomic, strong) DBManager *dbManager;
@property (nonatomic, strong) NSArray *arrTaskInfo;
@property (weak, nonatomic) IBOutlet UITableView *outboxTable;

@property (nonatomic, assign) NSInteger offlineTaskId;
@property (nonatomic, assign) NSInteger offlineUserId;
@property (nonatomic, strong) NSString *offlineTitle;
@property (nonatomic, strong) NSString *offlineDescription;
@property (nonatomic, strong) NSString *offlineStartingDate;
@property (nonatomic, strong) NSString *offlineEndingDate;
@property (nonatomic, strong) NSString * offlineAssignee;
@property (nonatomic, assign) NSInteger offlineAgencyID;
@property (nonatomic, assign) NSInteger offlineSourceID;
@property (nonatomic, strong) NSString *offlinePath;

@property (strong,nonatomic)  NSArray *reversedArray;
-(void)DeleteRecord:(NSInteger )taskId;
-(void)loadData;

@end
