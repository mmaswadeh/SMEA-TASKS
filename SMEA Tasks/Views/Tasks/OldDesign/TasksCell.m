//
//  TasksCell.m
//  PMO-BTIT
//
//  Created by Mohammad Maswadeh on 8/3/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import "TasksCell.h"
#import "ProgressChart.h"

@implementation TasksCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


+(id)customTaskCell
{
    
    TasksCell *customView = [[[NSBundle mainBundle] loadNibNamed:@"TasksCell" owner:nil options:nil] firstObject];
    
    // make sure customView is not nil or the wrong class!
    if ([customView isKindOfClass:[TasksCell class]])
    {
        return customView;
        
        
    }
    else
        return nil;
    
}


-(void)setValuesOfTasksList:(Tasks*)taskData
{
   // self.taskTitleLbl.text = taskData.title;
    [self.taskTitleLbl setText:[taskData.title uppercaseString]];

    self.taskDateLbl.text = [NSString stringWithFormat:@"%@ - %@",taskData.startingDate,taskData.endingDate];
    self.taskAssigneeLbl.text = taskData.assigneeUserName;
    self.taskStatusView.backgroundColor = taskData.taskColor;
    self.statusName.text = taskData.statusText;
    self.agencyLabel.text=taskData.Agency.name;
    self.percentageLabel.text=[NSString stringWithFormat:@"%li %@",(long)taskData.completePercentage,@"%"];
    
    
//    NSURL *url = [NSURL URLWithString:taskData.filesUrl];
//    
//    NSData *data = [NSData dataWithContentsOfURL:url];
//    UIImage *image = [UIImage imageWithData:data];
//    
//    [self.taskImage setImage:image];
}

- (IBAction)uploadAction:(id)sender {
}
@end
