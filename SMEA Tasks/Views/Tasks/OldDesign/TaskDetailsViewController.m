//
//  TaskDetailsViewController.m
//  PMO-BTIT
//
//  Created by Mohammad Maswadeh on 8/8/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import "TaskDetailsViewController.h"
#import "Tasks.h"
#import "EditTaskViewController.h"
#import "TasksResquest.h"
#import "MainViewController.h"
#import "ProgressChart.h"
#import "PMOCircleChart.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "IDMPhoto.h"
#import "IDMPhotoBrowser.h"
#import "SCLAlertView.h"
#import "StatusCell.h"
#import "DescriptionCell.h"
#import "DelaysCell.h"
#import "Delays.h"
#import "CluoserCell.h"

@interface TaskDetailsViewController ()

@end

@implementation TaskDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.delegate=self;
    self.tableView.dataSource=self;
    self.tableView.estimatedRowHeight = 70.0;

    self.navigationItem.title =NSLocalizedString(@"Details", nil);

    [_EditBtn setTitle:NSLocalizedString(@"Edit", nil) forState:UIControlStateNormal];
    [_DeleteBtn setTitle:NSLocalizedString(@"delete", nil) forState:UIControlStateNormal];

    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapOnImage:)];
    [self.detailsImage addGestureRecognizer:tapGestureRecognizer];
    self.detailsImage.userInteractionEnabled = YES;
    
    
    
    _backgroundView.layer.cornerRadius = 5.0; // set as you want.
    _backgroundView.layer.borderColor = [UIColor lightGrayColor].CGColor; // set color as you want.
    _backgroundView.layer.borderWidth = 0.5;


    
  //  [self.titleLabel setText:[_taskTitle uppercaseString]];
//    self.descriptionLabel.text = _taskDescription;
//
//    if ([_taskDescription isEqualToString:@""]) {
//
//        [self.descriptionLabel setText:[[NSString stringWithFormat:@"%@",NSLocalizedString(@"NoDescription", nil)] uppercaseString]];
//        self.descriptionLabel.textAlignment = NSTextAlignmentCenter;
//
//    }
//    self.startDateLabel.text = [NSString stringWithFormat:@"%@ : %@ %@ : %@",NSLocalizedString(@"From", nil),_startingDate,NSLocalizedString(@"To", nil),_endingDate];
//    self.assignToLabel.text = _assigneeUserName;
//    self.taskStatusView.backgroundColor = _color;
//    self.statusName.text = _taskStatusName;
    
    
    PMOCircleChart  *circleChartView;
    
    circleChartView = [[PMOCircleChart alloc]initWithFrame:CGRectMake(0.0,0.0,40.0,40.0)
                                                     total:@100
                                                   current:@(_percent)
                                                 clockwise:YES
                                                    shadow:YES
                                               shadowColor:[UIColor grayColor]];
    
    
    circleChartView.backgroundColor = [UIColor clearColor];
    
    [circleChartView setStrokeColor:_color];
    [circleChartView strokeChart];
    
    [self.chartView addSubview:circleChartView];
    

   // [self.detailsImage sd_setImageWithURL:[NSURL URLWithString:_filesUrl]];

    [self.detailsImage sd_setImageWithURL:[NSURL URLWithString:_filesUrl] placeholderImage:[UIImage imageNamed:@"No Image"]];
    
    
       // Do any additional setup after loading the view.
}

- (IBAction)didTapOnImage:(id)sender {
    
    if (_filesUrl) {
        
        NSArray *photosURL = @[[NSURL URLWithString:_filesUrl]];
        
        // Create an array to store IDMPhoto objects
        
        
        // Or use this constructor to receive an NSArray of IDMPhoto objects from your NSURL objects
        NSArray *photos = [IDMPhoto photosWithURLs:photosURL];
        
        IDMPhotoBrowser *browser = [[IDMPhotoBrowser alloc] initWithPhotos:photos animatedFromView:self.view];
        browser.displayArrowButton = NO;
        browser.displayActionButton = NO;
        
        [self presentViewController:browser animated:YES completion:nil];
    }
    
   
    

}

-(void)alertView:(LGAlertView *)alertView clickedButtonAtIndex:(NSUInteger)index title:(nullable NSString *)title {
    
    if (index == 0 ) {
        

            [TasksResquest deleteTasksWithId:_taskId success:^(NSDictionary *comments) {
        
        
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"NavigationController"];
        
                [navigationController setViewControllers:@[[storyboard instantiateViewControllerWithIdentifier:@"TasksHolderViewController"]]];
        
                MainViewController *mainViewController = [storyboard instantiateInitialViewController];
                mainViewController.rootViewController = navigationController;
        
                UIWindow *window = UIApplication.sharedApplication.delegate.window;
                window.rootViewController = mainViewController;
                [UIView transitionWithView:window
                                  duration:0.3
                                   options:UIViewAnimationOptionTransitionCrossDissolve
                                animations:nil
                                completion:nil];
                
                
            } failure:^(NSError *error) {
                
                NSLog(@"Error = %@",error);
                
            }];
    }
    
    else {
        
        NSLog(@"No");
        
    }
     
 }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)deleteAction:(id)sender {
    
    SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
    
    [alert addButton:@"Delete" target:self selector:@selector(deleteButton)];
    [alert showError:NSLocalizedString(@"Warning", nil) subTitle:NSLocalizedString(@"DeleteTask", nil) closeButtonTitle:@"Cancel" duration:0.0f];
    
    
//    LGAlertView *alertView = [[LGAlertView alloc] initWithTitle:NSLocalizedString(@"Warning", nil)
//                                                        message:NSLocalizedString(@"DeleteTask", nil)
//                                                          style:LGAlertViewStyleAlert
//                                                   buttonTitles:@[NSLocalizedString(@"Ok", nil)]
//                                              cancelButtonTitle:NSLocalizedString(@"cancel", nil)
//                                         destructiveButtonTitle:nil
//                                                       delegate:self];
//
//
//
//    alertView.windowLevel = LGAlertViewWindowLevelBelowStatusBar;
//    [alertView showAnimated:YES completionHandler:nil];
    
    

}

-(void)deleteButton {
    
    [TasksResquest deleteTasksWithId:_taskId success:^(NSDictionary *comments) {
        
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"NavigationController"];
        
        [navigationController setViewControllers:@[[storyboard instantiateViewControllerWithIdentifier:@"TasksHolderViewController"]]];
        
        MainViewController *mainViewController = [storyboard instantiateInitialViewController];
        mainViewController.rootViewController = navigationController;
        
        UIWindow *window = UIApplication.sharedApplication.delegate.window;
        window.rootViewController = mainViewController;
        [UIView transitionWithView:window
                          duration:0.3
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:nil
                        completion:nil];
        
        
    } failure:^(NSError *error) {
        
        NSLog(@"Error = %@",error);
        
    }];
}

- (IBAction)editAction:(id)sender {
    
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    EditTaskViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"EditTaskViewController"];
    
    
    controller.taskTitle = _taskTitle;
    controller.taskDescription = _taskDescription;
    controller.startingDate = _startingDate;
    controller.endingDate = _endingDate;
    controller.assigneeUserName = _assigneeUserName;
    controller.taskId = _taskId;
    controller.filesUrl = _filesUrl;
    controller.userId = _userId;
    controller.assigneeId=_userID;
    controller.agencyId = _agencyId;
    controller.agencyName = _agencyName;
    controller.sourceName=_sourceName;
    controller.sourceId=_sourceId;
    controller.MeetingNumberLbl=_MeetingNumberLbl;
    [self.navigationController pushViewController:controller animated:YES];
    

}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 4;
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    
    if (section==0){
        return 1;
        
    }
    if (section==1){
        return 1;
        
    }
    if (section==2){
        return 1;
        
    }
    else
    return self.delaysArr.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *mainCell;
    
    if (indexPath.row==0 && indexPath.section==0) {
        StatusCell* cell=[tableView dequeueReusableCellWithIdentifier:@"headerNotes"];
          [cell.titleLabel setText:[_taskTitle uppercaseString]];
        
        cell.startDateLabel.text = [NSString stringWithFormat:@"%@ : %@ %@ : %@",NSLocalizedString(@"From", nil),_startingDate,NSLocalizedString(@"To", nil),_endingDate];
        cell.assignToLabel.text = _assigneeUserName;
        cell.taskStatusView.backgroundColor = _color;
        cell.statusName.text = _taskStatusName;
        cell.agencyLabel.text=_agencyName;
        cell.percentageLabel.text =[NSString stringWithFormat:@"(%ld%@",(long)_percent,@"%)"];
        cell.sourceName.text =[NSString stringWithFormat:@"Source Name:%@",_sourceName];
        cell.meetingNo.text=[NSString stringWithFormat:@"Meeting No.:%@",_MeetingNumberLbl];
        
        mainCell=cell;
        
    }
    
    if (indexPath.row==0 && indexPath.section==1) {
        DescriptionCell* cell=[tableView dequeueReusableCellWithIdentifier:@"description"];
        
       cell.descriptionLabel.text = _taskDescription;
       
        if ([_taskDescription isEqualToString:@""]) {

            [cell.descriptionLabel setText:[[NSString stringWithFormat:@"%@",NSLocalizedString(@"NoDescription", nil)] uppercaseString]];
            cell.descriptionLabel.textAlignment = NSTextAlignmentCenter;

        }
        mainCell=cell;
    }
    
    if (indexPath.row==0 && indexPath.section==2) {
        CluoserCell* cell=[tableView dequeueReusableCellWithIdentifier:@"ClouserMessage"];
        cell.cluoserType.text=_ClouserType;
        cell.cluoserName.text=_ClouserMessage;
        
        mainCell=cell;
    }
  else  if (indexPath.section==3) {
        DelaysCell* cell=[tableView dequeueReusableCellWithIdentifier:@"delays"];
      Delays* item =[self.delaysArr objectAtIndex:indexPath.row];
      cell.delayDate.text=item.NewEndDate;
      cell.resoneText.text=item.Justification;
      mainCell=cell;
      
    }
   
    
   
 
    return mainCell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    int x = 0 ;
    
    if  (indexPath.section == 0 ) {
        x = 275;
    }
    
    
    else if (indexPath.section == 1) {
        
    x= UITableViewAutomaticDimension;
    }
    
    else  if (indexPath.section ==2){
        
        
        if([self.ClouserMessage isEqualToString:@""] && [self.ClouserType isEqualToString:@""])
        {
            x=0;
            
            
        }
        else{
            x= 60;
            
        }
    
    }
    else  if (indexPath.section ==3){
        
        x= 125;
        
        
    }
    return x;
}
@end
