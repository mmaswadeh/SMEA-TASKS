//
//  EditTaskViewController.m
//  PMO-BTIT
//
//  Created by Mohammad Maswadeh on 8/6/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import "EditTaskViewController.h"
#import "TasksResquest.h"
#import "MBProgressHUD.h"
#import "SCLAlertView.h"
#import "MainViewController.h"
#import "IQUIView+Hierarchy.h"
//#import "EXPhotoViewer.h"

@interface EditTaskViewController ()
{
    int tag;
    NSDate* selectedDate;

}

@property (nonatomic)     NSString            *recordedAudioFileName;
@property (nonatomic)     NSURL               *recordedAudioURL;
@property (nonatomic)     NSString            *recordAudioURLString;
@property (nonatomic)     NSInteger           currentAudioPlayingIndex;
@property (nonatomic, strong) PMCalendarController *startPmCC;
@property (nonatomic, strong) PMCalendarController *endPmCC;
@property (nonatomic, strong) NSArray *uneditableTextFields;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewCamera;


@end

@implementation EditTaskViewController
@synthesize startPmCC,endPmCC;

- (void)viewDidLoad {
    [super viewDidLoad];
    

    _backgroundView.layer.cornerRadius = 5.0; // set as you want.
    _backgroundView.layer.borderColor = [UIColor lightGrayColor].CGColor; // set color as you want.
    _backgroundView.layer.borderWidth = 0.5;
    
    [_SaveBtn setTitle:NSLocalizedString(@"Save", nil) forState:UIControlStateNormal];
    _selectImageLabel.text =  NSLocalizedString(@"SelectImageLabel", nil);
    
    [_taskTitleTF setPlaceholder:NSLocalizedString(@"TitleLabel", nil)];
    [_startDateTF setPlaceholder:NSLocalizedString(@"StartingLabel", nil)];
    [_endDateTF   setPlaceholder:NSLocalizedString(@"EndingLabel", nil)];
    [_assigneeTF setPlaceholder:NSLocalizedString(@"AssigneeLabel", nil)];
    
    [_agencyTF setPlaceholder:NSLocalizedString(@"AgencyLabel", nil)];
    [_taskDescreption setPlaceholder:NSLocalizedString(@"DescriptionLabel", nil)];
    
    [_sourceTF setPlaceholder:NSLocalizedString(@"TaskSource", nil)];
    [_meetingNumberTF setPlaceholder:NSLocalizedString(@"MeetingNumber", nil)];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(AddPhoto)];
    
    [_taskImage setUserInteractionEnabled:YES];
    
 //   [_taskImage addGestureRecognizer:singleTap];
    
    [self.taskImage sd_setImageWithURL:[NSURL URLWithString:_filesUrl]];

    
    self.taskTitleTF.text = _taskTitle;
    self.taskDescreption.text = _taskDescription;
    self.endDateTF.text = _endingDate;
    self.startDateTF.text = _startingDate;
    self.assigneeTF.text = _assigneeUserName;
    self.agencyTF.text = _agencyName;
    self.sourceTF.text=_sourceName;
    self.meetingNumberTF.text=_MeetingNumberLbl;
    
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    
    [TasksResquest getAssigneeUserlistWithSuccess:^(NSMutableArray *usersList,NSMutableArray *usersID) {
        
        [hud hideAnimated:YES];
        _usersArray = [[NSArray alloc]initWithArray:usersList];
        _ids = [[NSArray alloc]initWithArray:usersID];
        
        
        
    } failure:^(NSError *error) {
        
        NSLog(@"Error = %@",error);
        SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
        [alert showError:@"Failed" subTitle:error.localizedDescription closeButtonTitle:NSLocalizedString(@"Done", nil) duration:0.0f];
        
        NSLog(@"Failed = %@",error);
        
        
    }];
     NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    _agencyArray=[userDefaults objectForKey:@"agencyNames"];
    _idsAgency= [userDefaults objectForKey:@"agencyIds"];
    _userId = [userDefaults integerForKey:@"UserID"];
  _ids = [userDefaults objectForKey:@"assingeeIds"];
    
    _sourcesArray=[userDefaults objectForKey:@"sourcesName"];
    _idsSource= [userDefaults objectForKey:@"sourcesIds"];
    [self configureView];
    
}

-(void)configureView
{
    _startDateTF.delegate = self;
    _endDateTF.delegate = self;
    _assigneeTF.delegate = self;
    _agencyTF.delegate     = self;
    
    _sourceTF.delegate = self;
    
    self.uneditableTextFields = @[self.startDateTF,
                                  self.endDateTF,
                                  self.assigneeTF];
    
    
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    BOOL shouldEdit = ![self.uneditableTextFields containsObject:textField];
    
  //  if (textField.isAskingCanBecomeFirstResponder == NO)
 //   {
        if (textField == self.startDateTF){
            
            [self addDueDate];
        }
        else if (textField == self.endDateTF){
            [self addEndDate];
        }
        else if(textField == self.assigneeTF){
            [self addAssingee];
        }
        else if (textField == self.agencyTF)
        {
            [self addAgency];
        }
        else if (textField == self.sourceTF){
            [self addSource];
        }
  //  }
    
    return shouldEdit;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)AddPhoto
{
    LGAlertView *alertView = [[LGAlertView alloc] initWithTitle:NSLocalizedString(@"TitleLabel", nil)
                                                        message:NSLocalizedString(@"PhotoSource", nil)
                                                          style:LGAlertViewStyleActionSheet
                                                   buttonTitles:@[NSLocalizedString(@"Camera", nil),
                                                                  NSLocalizedString(@"Photo", nil)]
                                              cancelButtonTitle:NSLocalizedString(@"cancel", nil)
                                         destructiveButtonTitle:nil
                                                       delegate:self];
    
    
    tag = 0;
    
    alertView.windowLevel = LGAlertViewWindowLevelBelowStatusBar;
    [alertView showAnimated:YES completionHandler:nil];
    
    
    
}


#pragma mark - LGAlertViewDelegate

- (void)alertView:(LGAlertView *)alertView clickedButtonAtIndex:(NSUInteger)index title:(nullable NSString *)title {
    NSLog(@"action {title: %@, index: %lu}", title, (long unsigned)index);
    
    
    if (tag == 0) {
        
        if ([title isEqualToString:@"Camera"]|| [title isEqualToString:@"الكاميرا"]) {
            
            [self startCameraPickerFromViewController:self usingDelegate:self];
            
        }
        
        else if ([title isEqualToString:@"Photo Gallery"] || [title isEqualToString:@"الاستديو"]) {

            [self startLibraryPickerFromViewController:self usingDelegate:self];
            
        }
        
        
    }
    
    else if (tag == 1 ) {
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"dd/MM/yyyy"];
        
        //Optionally for time zone conversions
        
        NSString *stringFromDate = [formatter stringFromDate:_startDatePicker.date];
        _startDateTF.text = stringFromDate;
        
        
    }
    
    else if (tag == 2 ) {
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"dd/MM/yyyy"];
        
        //Optionally for time zone conversions
        
        NSString *stringFromDate = [formatter stringFromDate:_endDatePicker.date];
        _endDateTF.text = stringFromDate;
        
    }
    
    else if (tag == 3 ) {
        NSLog(@"Tag = 3 ");
        
        _assigneeTF.text = title;
        _assigneeId = [[_ids objectAtIndex:index] integerValue];//index;

    }
    else if (tag == 4 ) {
        NSLog(@"Tag = 4 ");
        
        _agencyTF.text = title;
         _agencyId = [[_idsAgency objectAtIndex:index] integerValue];
        
    }
    else if (tag == 5 ) {
        NSLog(@"Tag = 5 ");
        
        _sourceTF.text = title;
        _sourceId = [[_idsSource objectAtIndex:index] integerValue];
        
    }
}

- (void)alertViewCancelled:(LGAlertView *)alertView {
    NSLog(@"cancel");
}

- (void)alertViewDestructed:(LGAlertView *)alertView {
    NSLog(@"destructive");
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (BOOL)startCameraPickerFromViewController:(UIViewController*)controller usingDelegate:(id<UIImagePickerControllerDelegate>)delegateObject
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        picker.allowsEditing = YES;
        picker.delegate = self;
        [controller presentViewController:picker animated:YES completion:nil];
    }
    return YES;
}

- (BOOL)startLibraryPickerFromViewController:(UIViewController*)controller usingDelegate:(id<UIImagePickerControllerDelegate>)delegateObject
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
    {
        UIImagePickerController *picker1 = [[UIImagePickerController alloc]init];
        picker1.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        picker1.allowsEditing = YES;
        picker1.delegate = self;
        [controller presentViewController:picker1 animated:YES completion:nil];
        
    }
    return YES;
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage * img = [info objectForKey:@"UIImagePickerControllerEditedImage"];
    
    // _imagePath = [info objectForKey:UIImagePickerControllerMediaURL];
    
    NSDateFormatter *dateformat = [[NSDateFormatter alloc] init];
    [dateformat setDateFormat:@"yyyy_MM_dd_HH_mm_ss"];
    
    NSDate *date = [[NSDate alloc] initWithTimeIntervalSinceNow:1];
    //NSString *startDateformate = [dateformat stringFromDate:start];
    _recordedAudioFileName = [NSString stringWithFormat:@"%@", [dateformat stringFromDate:date]];
    
    // sets the path for audio file
    NSArray *pathComponents = [NSArray arrayWithObjects:
                               [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject],
                               [NSString stringWithFormat:@"%@.png", [self recordedAudioFileName]],
                               nil];
    _recordedAudioURL = [NSURL fileURLWithPathComponents:pathComponents];
    _imagePath =[NSString stringWithFormat:@"%@",_recordedAudioURL];
    
    
    [self useImage:img];
    [[picker parentViewController] dismissModalViewControllerAnimated:NO];
    UINavigationController* navController = self.navigationController;
    UIViewController* controller = [navController.viewControllers objectAtIndex:0];
    [controller dismissViewControllerAnimated:YES completion:nil];
    
}



-(void)useImage:(UIImage *)theImage
{
    NSData *addImageData = UIImagePNGRepresentation(theImage);
    
    // _taskImage = [UIImage imageWithData:addImageData];
    [_taskImage setImage:[UIImage imageWithData:addImageData]];
    
    [_imageViewCamera setHidden:YES];
    [_editButton setHidden:NO];
    [_zoomImage setHidden:NO];
}


-(void)addDueDate{
    tag = 1;
    
        _startDatePicker = [UIDatePicker new];
        _startDatePicker.datePickerMode = UIDatePickerModeDate;
        _startDatePicker.minimumDate = [NSDate date];

        _startDatePicker.frame = CGRectMake(0.0, 0.0, _startDatePicker.frame.size.width, 160.0);
    
    [[[LGAlertView alloc] initWithViewAndTitle:NSLocalizedString(@"StartingLabel", nil)
                                       message:NSLocalizedString(@"StartingDatePickerTitle", nil)
                                         style:LGAlertViewStyleActionSheet
                                          view:_startDatePicker
                                  buttonTitles:@[NSLocalizedString(@"Done", nil)]
                             cancelButtonTitle:NSLocalizedString(@"cancel", nil)
                        destructiveButtonTitle:nil
                                      delegate:self] showAnimated:YES completionHandler:nil];
    
    

}


-(void)addEndDate{
    
    tag = 2;
    

        _endDatePicker = [UIDatePicker new];
        _endDatePicker.datePickerMode = UIDatePickerModeDate;
        _endDatePicker.minimumDate = _startDatePicker.date;
        _endDatePicker.frame = CGRectMake(0.0, 0.0, _endDatePicker.frame.size.width, 160.0);
    
    [[[LGAlertView alloc] initWithViewAndTitle:NSLocalizedString(@"EndingLabel", nil)
                                       message:NSLocalizedString(@"EndingDatePickerTitle", nil)
                                         style:LGAlertViewStyleActionSheet
                                          view:_endDatePicker
                                  buttonTitles:@[NSLocalizedString(@"Done", nil)]
                             cancelButtonTitle:NSLocalizedString(@"cancel", nil)
                        destructiveButtonTitle:nil
                                      delegate:self] showAnimated:YES completionHandler:nil];


    
    
}


- (IBAction)editImageAction:(id)sender {
    
    LGAlertView *alertView = [[LGAlertView alloc] initWithTitle:NSLocalizedString(@"TitleLabel", nil)
                                                        message:NSLocalizedString(@"PhotoSource", nil)
                                                          style:LGAlertViewStyleActionSheet
                                                   buttonTitles:@[NSLocalizedString(@"Camera", nil),
                                                                  NSLocalizedString(@"Photo", nil),NSLocalizedString(@"", nil)]
                                              cancelButtonTitle:NSLocalizedString(@"cancel", nil)
                                         destructiveButtonTitle:nil
                                                       delegate:self];
    
    
    tag = 0;
    
    alertView.windowLevel = LGAlertViewWindowLevelBelowStatusBar;
    [alertView showAnimated:YES completionHandler:nil];
    
}

- (IBAction)discardAction:(id)sender {
    
    SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
    
    [alert addButton:@"Discard" target:self selector:@selector(discardButton)];
    [alert showError:@"Discard Changes ?" subTitle:@"You will lose all of your changes to new task if you discard" closeButtonTitle:@"Cancel" duration:0.0f];
    
}



-(void)discardButton {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}
- (IBAction)editTask:(id)sender {
    
    NSData *imageData = UIImagePNGRepresentation(_taskImage.image);
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
//    [TasksResquest addTaskWithComment:_taskDescreption.text andTitle:_taskTitleTF.text andUserid:_userId andTaskId:_taskId andImagePath:_imagePath andImageData:imageData andStartingDate:_startDateTF.text andEndingDate:_endDateTF.text andAgencyid:_agencyId success:^(NSDictionary *comments) {
 [TasksResquest addTaskWithComment:_taskDescreption.text andTitle:_taskTitleTF.text andAssigneeid:_assigneeId andTaskId:_taskId andImagePath:_imagePath andImageData:imageData andStartingDate:_startDateTF.text andEndingDate:_endDateTF.text andAgencyid:_agencyId andUserid:_userId andSourceId:_sourceId     andMeetingNumber:[_meetingNumberTF.text integerValue]  success:^(NSDictionary *comments) {


        [hud hideAnimated:YES];

        SCLAlertView *editAlert = [[SCLAlertView alloc] initWithNewWindow];
        
        [editAlert showSuccess:@"Success" subTitle:@"task has been edited successfully" closeButtonTitle:@"Ok" duration:0.0f];
        
        [editAlert alertIsDismissed:^{
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"NavigationController"];
            
            [navigationController setViewControllers:@[[storyboard instantiateViewControllerWithIdentifier:@"TasksHolderViewController"]]];
            
            MainViewController *mainViewController = [storyboard instantiateInitialViewController];
            mainViewController.rootViewController = navigationController;
            
            UIWindow *window = UIApplication.sharedApplication.delegate.window;
            window.rootViewController = mainViewController;
            [UIView transitionWithView:window
                              duration:0.3
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:nil
                            completion:nil];
            
            NSLog(@"SCLAlertView dismissed!");
        }];
        
      
        

        
    } failure:^(NSError *error) {
        [hud hideAnimated:YES];

        NSLog(@"Error = %@",error);
        
        
    }];
    

    
}

-(void)addAssingee{
    LGAlertView *alertView = [[LGAlertView alloc] initWithTitle:NSLocalizedString(@"AssigneeList", nil)
                                                        message:NSLocalizedString(@"ChooseAssignee", nil)
                                                          style:LGAlertViewStyleAlert
                                                   buttonTitles:_usersArray
                                              cancelButtonTitle:NSLocalizedString(@"cancel", nil)
                                         destructiveButtonTitle:nil
                                                       delegate:self];
    
    tag = 3;
    
    alertView.windowLevel = LGAlertViewWindowLevelBelowStatusBar;
    [alertView showAnimated:YES completionHandler:nil];
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (IBAction)zoomAction:(id)sender {
    
    NSArray *photos = [NSArray arrayWithObject:[IDMPhoto photoWithImage:_taskImage.image]];
    
    IDMPhotoBrowser *browser = [[IDMPhotoBrowser alloc] initWithPhotos:photos animatedFromView:self.view];
    browser.displayArrowButton = NO;
    browser.displayActionButton = NO;
    
    [self presentViewController:browser animated:YES completion:nil];
//    [EXPhotoViewer showImageFrom:_taskImage];

    NSLog(@"Zooom");

}

-(void)addAgency
{
    LGAlertView *alertView = [[LGAlertView alloc] initWithTitle:NSLocalizedString(@"AgencyList", nil)
                                                        message:NSLocalizedString(@"ChooseAgency", nil)
                                                          style:LGAlertViewStyleAlert
                                                   buttonTitles:_agencyArray
                                              cancelButtonTitle:NSLocalizedString(@"cancel", nil)
                                         destructiveButtonTitle:nil
                                                       delegate:self];
    
    tag = 4;
    
    alertView.windowLevel = LGAlertViewWindowLevelBelowStatusBar;
    [alertView showAnimated:YES completionHandler:nil];
    
}
- (IBAction)deleteAction:(id)sender {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Warning", nil) message:NSLocalizedString(@"deleteImagemsg", nil) preferredStyle:UIAlertControllerStyleAlert];

    [alert.view setTintColor:[UIColor blackColor]];

    UIAlertAction *ok = [UIAlertAction actionWithTitle:NSLocalizedString(@"Yes", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                         {

                             UIImage * img = [UIImage imageNamed:@"img_no_image"];

                             [self useImage:img];

                             NSData *imageData = UIImagePNGRepresentation(_taskImage.image);

                             MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];

//                           [TasksResquest addTaskWithComment:_taskDescreption.text andTitle:_taskTitleTF.text andAssigneeid:_assigneeId andTaskId:_taskId andImagePath:_imagePath andImageData:imageData andStartingDate:_startDateTF.text andEndingDate:_endDateTF.text andAgencyid:_agencyId andUserid:_userId success:^(NSDictionary *comments) {

                             [TasksResquest deleteTaskImageWithId:_taskId success:^(NSDictionary *comments) {
                                 
                                 [hud hideAnimated:YES];


                             } failure:^(NSError *error) {

                                 NSLog(@"Error = %@",error);
                                 [hud hideAnimated:YES];


                             }];

                         }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDestructive handler:nil];
    [alert addAction:ok];
    [alert addAction:cancel];

    [self presentViewController:alert animated:YES completion:nil];
//

}
-(void)addSource
{
    LGAlertView *alertView = [[LGAlertView alloc] initWithTitle:NSLocalizedString(@"Sources List", nil)
                                                        message:NSLocalizedString(@"", nil)
                                                          style:LGAlertViewStyleAlert
                                                   buttonTitles:_sourcesArray
                                              cancelButtonTitle:NSLocalizedString(@"cancel", nil)
                                         destructiveButtonTitle:nil
                                                       delegate:self];
    
    tag = 5;
    
    alertView.windowLevel = LGAlertViewWindowLevelBelowStatusBar;
    [alertView showAnimated:YES completionHandler:nil];
    
}


@end
