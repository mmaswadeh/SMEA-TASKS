//
//  TasksCell.h
//  PMO-BTIT
//
//  Created by Mohammad Maswadeh on 8/3/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Tasks.h"
#import "UIImage+FX.h"
#import "FXImageView.h"

@interface TasksCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *taskTitleLbl;
@property (weak, nonatomic) IBOutlet UILabel *taskAssigneeLbl;

@property (weak, nonatomic) IBOutlet UILabel *taskDateLbl;
@property (weak, nonatomic) IBOutlet UIView *taskStatusView;

-(void)setValuesOfTasksList:(Tasks*)taskData;
+(id)customTaskCell;
@property (weak, nonatomic) IBOutlet UIButton *uploadTask;
- (IBAction)uploadAction:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *statusName;
@property (weak, nonatomic) IBOutlet UILabel *agencyLabel;
@property (weak, nonatomic) IBOutlet UILabel *percentageLabel;

@end
