//
//  MyTasksDetailsViewController.h
//  MCI-Tasks
//
//  Created by Mohammad Maswadeh on 1/23/18.
//  Copyright © 2018 BTIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <LGAlertView/LGAlertView.h>
#import "UIImage+FX.h"
#import "FXImageView.h"
#import "UpdateProgressPopup.h"

@protocol MyTaskDetailsViewDelegate <NSObject>

@optional

-(void)didUpdateTheProgressTask:(NSInteger)TaskId;
-(void)sendDateToPopup:(NSString *)delayDate;

@end


@interface MyTasksDetailsViewController : UIViewController <LGAlertViewDelegate,UITableViewDelegate,UITableViewDataSource,UpdateProgressPopupDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *myTaskDetailsImage;

@property (weak, nonatomic) IBOutlet UIView *backgroundView;

@property (weak, nonatomic) IBOutlet UIView *chartView;
@property (strong,nonatomic)  NSArray *usersArray;
@property (strong,nonatomic)  NSArray *ids;
@property (nonatomic, assign) NSInteger userId;
@property (nonatomic, assign) NSInteger agencyId;
@property (nonatomic, assign) NSInteger delayId;

@property (nonatomic, assign) NSInteger myTaskId;
@property (nonatomic, strong) NSString *imagePath;
@property (nonatomic, assign) NSInteger percent;
@property (nonatomic, assign) NSInteger userID;

@property (nonatomic, strong) UIColor *color;
@property (nonatomic, strong) NSString *myTaskTitle;
@property (nonatomic, strong) NSString *assigneeUserName;
@property (nonatomic, strong) NSString *filesUrl;
@property (nonatomic, strong) NSString *startingDate;
@property (nonatomic, strong) NSString *endingDate;
@property (nonatomic, strong) NSString *myTaskDescription;
@property (nonatomic, strong) NSString *myTaskStatusName;
@property (weak, nonatomic) IBOutlet UIButton *DeleteBtn;
@property (weak, nonatomic) IBOutlet UIButton *EditBtn;
@property (nonatomic, strong) NSString *agencyName;
@property (nonatomic, strong) NSArray  *delaysArr;
@property (nonatomic, strong) NSString *ClouserType;
@property (nonatomic, strong) NSString *ClouserMessage;
@property (nonatomic, strong) NSString *lastDelayDate;
@property (nonatomic, assign) NSInteger sourceId;
@property (nonatomic, strong) NSString * sourceName;
@property (nonatomic, strong) NSString * MeetingNumberLbl;
- (IBAction)deleteAction:(id)sender;
- (IBAction)editAction:(id)sender;

@property (weak, nonatomic) IBOutlet UISlider *currentTimeSlider;
@property (weak, nonatomic) IBOutlet UIButton *playButton;
@property (weak, nonatomic) IBOutlet UILabel *duration;
@property (weak, nonatomic) IBOutlet UILabel *timeElapsed;

@property(weak,nonatomic) id <MyTaskDetailsViewDelegate> myTaskDetailsDelegate;


@property BOOL isPaused;
@property BOOL scrubbing;

@property NSTimer *timer;

@property (weak, nonatomic) IBOutlet UITableView *tableView;


@end
