//
//  OutboxViewController.m
//  PMO-BTIT
//
//  Created by Mohammad Maswadeh on 8/21/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import "OutboxViewController.h"
#import "TasksCell.h"
#import "TasksResquest.h"
#import "MainViewController.h"
#import "MBProgressHUD.h"

@interface OutboxViewController ()

@end

@implementation OutboxViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
    self.outboxTable.delegate = self;
    self.outboxTable.dataSource = self;
    
    // Initialize the dbManager property.
    self.dbManager = [[DBManager alloc] initWithDatabaseFilename:@"TasksDB.sqlite"];
    [self loadData];
    
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)loadData{
    // Form the query.
    NSString *query = @"select * from OfflineTasks";
    
    // Get the results.
    if (self.arrTaskInfo != nil) {
        self.arrTaskInfo = nil;
        

    }
    _reversedArray = [[[self.dbManager loadDataFromDB:query] reverseObjectEnumerator] allObjects];

    self.arrTaskInfo = [[NSArray alloc] initWithArray:_reversedArray];
    
    // Reload the table view.
    [self.outboxTable reloadData];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 144;
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.arrTaskInfo.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    // Dequeue the cell.
    static NSString *simpleTableIdentifier = @"TasksCell";

    TasksCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    cell.uploadTask.tag = indexPath.row;

    [cell.uploadTask addTarget:self action:@selector(uploadTask:) forControlEvents:UIControlEventTouchUpInside];
    

    cell.taskTitleLbl.text = [NSString stringWithFormat:@"%@", [[self.arrTaskInfo objectAtIndex:indexPath.row] objectAtIndex:1]];
    cell.taskDateLbl.text = [NSString stringWithFormat:@"%@ - %@",[[self.arrTaskInfo objectAtIndex:indexPath.row] objectAtIndex:5],[[self.arrTaskInfo objectAtIndex:indexPath.row] objectAtIndex:6]];
    
    cell.taskAssigneeLbl.text = [NSString stringWithFormat:@"%@",[[self.arrTaskInfo objectAtIndex:indexPath.row]objectAtIndex:7]];
    
    
    
    NSLog(@"%@",[NSString stringWithFormat:@"%@",[[self.arrTaskInfo objectAtIndex:indexPath.row]objectAtIndex:0]]
          );

    
    return cell;
}

- (IBAction)uploadTask:(UIButton *)button
{
    
    _offlineTaskId = [[[self.arrTaskInfo objectAtIndex:button.tag] objectAtIndex:0]integerValue];
    _offlineTitle = [[self.arrTaskInfo objectAtIndex:button.tag] objectAtIndex:1];
    _offlineDescription = [[self.arrTaskInfo objectAtIndex:button.tag] objectAtIndex:2];
    _offlineUserId = [[[self.arrTaskInfo objectAtIndex:button.tag] objectAtIndex:3]integerValue];
    _offlinePath = [[self.arrTaskInfo objectAtIndex:button.tag] objectAtIndex:4];

    _offlineStartingDate = [[self.arrTaskInfo objectAtIndex:button.tag] objectAtIndex:5];
    _offlineEndingDate = [[self.arrTaskInfo objectAtIndex:button.tag] objectAtIndex:6];

    _offlineAssignee = [[self.arrTaskInfo objectAtIndex:button.tag] objectAtIndex:7];
    _offlineAgencyID =[[[self.arrTaskInfo objectAtIndex:button.tag]objectAtIndex:8]integerValue];
   
    
   // [self uploadRequestWithComment:_offlineDescription andTitle:_offlineTitle andUserid:_offlineUserId andTaskId:_offlineTaskId andImagePath:_offlinePath andImageData:nil andStartingDate:_offlineStartingDate andEndingDate:_offlineEndingDate];
    
    [self uploadRequestWithComment:_offlineDescription andTitle:_offlineTitle andAssigneeid:_offlineUserId andAgencyid:_offlineAgencyID andTaskId:_offlineTaskId andImagePath:_offlinePath andImageData:nil andStartingDate:_offlineStartingDate andEndingDate:_offlineEndingDate andUserid:1];
    
    
}

-(void)uploadRequestWithComment:(NSString*)comment andTitle:(NSString*)title andAssigneeid:(NSInteger)assigneeId andAgencyid:(NSInteger)agencyId andTaskId:(NSInteger)taskId andImagePath:(NSString *)imagePath andImageData:(NSData*)imageData andStartingDate:(NSString *)startDate andEndingDate:(NSString *)endDate andUserid:(NSInteger)userId {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:
                      [NSString stringWithFormat:@"%@",imagePath]];
    
    
    NSError* error = nil;
    NSData* data = [NSData dataWithContentsOfFile:path  options:0 error:&error];
    
    NSLog(@"Data read from %@ with error: %@", imagePath, error);
    
    MBProgressHUD *hud2 = [MBProgressHUD showHUDAddedTo:self.view animated:YES];

//    [TasksResquest addTaskWithComment:comment andTitle:title andUserid:userId andTaskId:0 andImagePath:imagePath andImageData:data andStartingDate:startDate andEndingDate:endDate andAgencyid:1 success:^(NSDictionary *comments) {
        
        
        [TasksResquest addTaskWithComment:comment andTitle:title andAssigneeid:userId andTaskId:0 andImagePath:imagePath andImageData:imageData andStartingDate:startDate andEndingDate:endDate andAgencyid:1 andUserid:userId andSourceId:1 andMeetingNumber:1 success:^(NSDictionary *comments) {
        
        [hud2 hideAnimated:YES];
        [self DeleteRecord:taskId];
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"NavigationController"];
        
        [navigationController setViewControllers:@[[storyboard instantiateViewControllerWithIdentifier:@"TasksHolderViewController"]]];
        
        MainViewController *mainViewController = [storyboard instantiateInitialViewController];
        mainViewController.rootViewController = navigationController;
        
        UIWindow *window = UIApplication.sharedApplication.delegate.window;
        window.rootViewController = mainViewController;
        [UIView transitionWithView:window
                          duration:0.3
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:nil
                        completion:nil];
        
        
        
        
        
    } failure:^(NSError *error) {
        
        NSLog(@"Error = %@",error);
        
    }];
    
}


-(void)DeleteRecord:(NSInteger )taskId
{
        // Delete the selected record.
        // Find the record ID.
         self.dbManager = [[DBManager alloc] initWithDatabaseFilename:@"TasksDB.sqlite"];

        // Prepare the query.
        NSString *query = [NSString stringWithFormat:@"delete from OfflineTasks where TaskId=%ld", (long)taskId];
        
        // Execute the query.
        [self.dbManager executeQuery:query];
        
        // Reload the table view.
    

       [self.outboxTable reloadData];

}

-(UIImage *)compressImage:(UIImage *)image{
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    float maxHeight = 300.0;
    float maxWidth = 400.0;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    float compressionQuality = 0.5;//50 percent compression
    if (actualHeight > maxHeight || actualWidth > maxWidth){
        if(imgRatio < maxRatio){
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio){
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }
        else{
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
    }
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [image drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    NSData *imageData = UIImageJPEGRepresentation(img, compressionQuality);
    UIGraphicsEndImageContext();
    return [UIImage imageWithData:imageData];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
