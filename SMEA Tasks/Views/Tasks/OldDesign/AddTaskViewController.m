//
//  AddTaskViewController.m
//  PMO-BTIT
//
//  Created by Mohammad Maswadeh on 8/3/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

/* Set this to YES to reproduce the playback issue */

#define IMPEDE_PLAYBACK NO

#import "AddTaskViewController.h"
#import "TasksResquest.h"
#import "MBProgressHUD.h"
#import "SCLAlertView.h"
#import "MainViewController.h"
#import "PMCalendar.h"
#import "DBManager.h"
#import "IQUIView+Hierarchy.h"
#import "IDMPhoto.h"
#import "IDMPhotoBrowser.h"

@interface AddTaskViewController ()
{
    int tag;
    NSDate* selectedDate;

}

@property (nonatomic)     NSString            *imageFileName;
@property (nonatomic)     NSString            *savedImagePath;
@property (nonatomic)     NSURL               *imageURL;
@property (nonatomic, strong) PMCalendarController *startPmCC;
@property (nonatomic, strong) PMCalendarController *endPmCC;
@property (nonatomic, strong) DBManager       *dbManager;
@property (nonatomic, strong) NSArray         *uneditableTextFields;
@property (nonatomic)     AVAudioPlayer       *player;
@property (nonatomic)     AVAudioRecorder     *recorder;
@property (nonatomic)     NSString            *recordedAudioFileName;
@property (nonatomic)     NSURL               *recordedAudioURL;

- (IBAction)recordingForTouchDown:(id)sender;
- (IBAction)endRecording:(id)sender;



@end

@implementation AddTaskViewController
@synthesize startPmCC,endPmCC;
@synthesize player, recorder, recordedAudioFileName, recordedAudioURL;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title =NSLocalizedString(@"NewTask", nil);
    self.edgesForExtendedLayout = UIRectEdgeNone;

    
    UIBarButtonItem * leftButton= [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu"] style:UIBarButtonItemStylePlain target:self action:@selector(ShowLeftView:)];
        
    self.navigationItem.leftBarButtonItem = leftButton;
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    
    [_SaveBtn setTitle:NSLocalizedString(@"Save", nil) forState:UIControlStateNormal];
    [_HoldToRecordBtn setTitle:NSLocalizedString(@"HoldToRecord", nil) forState:UIControlStateNormal];
    _selectImageLabel.text =  NSLocalizedString(@"SelectImageLabel", nil);
    [_taskTitleTF setPlaceholder:NSLocalizedString(@"TitleLabel", nil)];
    [_startDateTF setPlaceholder:NSLocalizedString(@"StartingLabel", nil)];
    [_endDateTF   setPlaceholder:NSLocalizedString(@"EndingLabel", nil)];
    [_assigneeTF setPlaceholder:NSLocalizedString(@"AssigneeLabel", nil)];
    [_agencyTF setPlaceholder:NSLocalizedString(@"AgencyLabel", nil)];
    [_sourceTF setPlaceholder:NSLocalizedString(@"TaskSource", nil)];
    [_meetingNumberTF setPlaceholder:NSLocalizedString(@"MeetingNumber", nil)];

    [_taskDescreption setPlaceholder:NSLocalizedString(@"DescriptionLabel", nil)];

    

    _backgroundView.layer.cornerRadius = 5.0; // set as you want.
    _backgroundView.layer.borderColor = [UIColor lightGrayColor].CGColor; // set color as you want.
    _backgroundView.layer.borderWidth = 0.5;

    
    self.dbManager = [[DBManager alloc] initWithDatabaseFilename:@"TasksDB.sqlite"];

    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(AddPhoto)];

    [_cameraImageView setUserInteractionEnabled:YES];
    
    [_cameraImageView addGestureRecognizer:singleTap];

    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    _usersArray = [userDefaults objectForKey:@"assingeeNames"];
    _ids = [userDefaults objectForKey:@"assingeeIds"];
    _agencyArray=[userDefaults objectForKey:@"agencyNames"];
    _idsAgency= [userDefaults objectForKey:@"agencyIds"];
    _userId = [userDefaults integerForKey:@"UserID"];
    _sourcesArray=[userDefaults objectForKey:@"sourcesName"];
    _idsSource= [userDefaults objectForKey:@"sourcesIds"];

 
    [self configureView];
    
}

-(IBAction)ShowLeftView:(id)sender
{
    
    NSString * language = [[NSString alloc]init];
    
    _languageSettings = [[languageHelper alloc]init];
    language= [_languageSettings checkLanguages];
    if ([language isEqualToString:@"ar"]) {
        
        [self.sideMenuController showRightViewAnimated:YES completionHandler:nil];}
    else{
        [self.sideMenuController showLeftViewAnimated:YES completionHandler:nil];
    }
}



-(void)configureView
{
    _startDateTF.delegate  = self;
    _endDateTF.delegate    = self;
    _assigneeTF.delegate   = self;
    _agencyTF.delegate     = self;
    _taskTitleTF.delegate  = self;
    _sourceTF.delegate = self;
   // _meetingNumberTF.delegate = self;
    
    self.uneditableTextFields = @[self.startDateTF,
                                  self.endDateTF,
                                  self.assigneeTF,
                                  self.agencyTF,
                                  self.sourceTF];
    

}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    BOOL shouldEdit = ![self.uneditableTextFields containsObject:textField];
    
  //  if (textField.isAskingCanBecomeFirstResponder == NO)
 //   {
        if (textField == self.startDateTF){
            
            [self addDueDate];
        }
        else if (textField == self.endDateTF){
            [self addEndDate];
        }
        else if(textField == self.assigneeTF){
            [self addAssingee];
        }
    else if (textField == self.agencyTF)
    {
        [self addAgency];
    }
    
    else if (textField == self.sourceTF){
        [self addSource];
    }
    
  //  }
    
    return shouldEdit;
}

- (BOOL)textField:(UITextField *) textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSUInteger Length = 0;
    
    if (textField == self.taskTitleTF){
        
        
        NSUInteger oldLength = [textField.text length];
        NSUInteger replacementLength = [string length];
        NSUInteger rangeLength = range.length;
        
        NSUInteger newLength = oldLength - rangeLength + replacementLength;
        
        BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
        
        Length = newLength <= 80 || returnKey;
        
    }
    return Length;
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)AddPhoto
{
  
   
        LGAlertView *alertView = [[LGAlertView alloc] initWithTitle:NSLocalizedString(@"TitleLabel", nil)
                                                            message:NSLocalizedString(@"PhotoSource", nil)
                                                              style:LGAlertViewStyleActionSheet
                                                       buttonTitles:@[NSLocalizedString(@"Camera", nil),
                                                                      NSLocalizedString(@"Photo", nil),NSLocalizedString(@"", nil)]
                                                  cancelButtonTitle:NSLocalizedString(@"cancel", nil)
                                             destructiveButtonTitle:nil
                                                           delegate:self];
        
        
        tag = 0;
        
        alertView.windowLevel = LGAlertViewWindowLevelBelowStatusBar;
        [alertView showAnimated:YES completionHandler:nil];

    
   
}

#pragma mark - LGAlertViewDelegate

- (void)alertView:(LGAlertView *)alertView clickedButtonAtIndex:(NSUInteger)index title:(nullable NSString *)title {
    NSLog(@"action {title: %@, index: %lu}", title, (long unsigned)index);
    
    if (tag == 0) {
        
        if ([title isEqualToString:@"Camera"] ||[title isEqualToString:@"الكاميرا"]) {
            
            [self startCameraPickerFromViewController:self usingDelegate:self];
            
        }
        
        else if ([title isEqualToString:@"Photo Gallery"] || [title isEqualToString:@"الاستديو"]) {
            
            [self startLibraryPickerFromViewController:self usingDelegate:self];
            
        }
        

    }
    
    else if (tag == 1 ) {
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"dd/MM/yyyy"];
        
        //Optionally for time zone conversions
        
        NSString *stringFromDate = [formatter stringFromDate:_startDatePicker.date];
        _startDateTF.text = stringFromDate;
        
        
    }
    
    else if (tag == 2 ) {
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"dd/MM/yyyy"];
        
        //Optionally for time zone conversions
        
        NSString *stringFromDate = [formatter stringFromDate:_endDatePicker.date];
        _endDateTF.text = stringFromDate;
        
    }
    

    
    else if (tag == 3 ) {
        NSLog(@"Tag = 3 ");
        
        _assigneeTF.text = title;
        _assigneId = [[_ids objectAtIndex:index] integerValue];
        
    }
    
    else if (tag == 4 ) {
        NSLog(@"Tag = 4 ");
        
        _agencyTF.text = title;
      _agencyId = [[_idsAgency objectAtIndex:index] integerValue];
        
    }
    
    
    else if (tag == 5 ) {
        NSLog(@"Tag = 4 ");
        
        _sourceTF.text = title;
        _sourceId = [[_idsSource objectAtIndex:index] integerValue];
        
    }
}

- (void)alertViewCancelled:(LGAlertView *)alertView {
    NSLog(@"cancel");
}

- (void)alertViewDestructed:(LGAlertView *)alertView {
    NSLog(@"destructive");
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (BOOL)startCameraPickerFromViewController:(UIViewController*)controller usingDelegate:(id<UIImagePickerControllerDelegate>)delegateObject
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        picker.allowsEditing = NO;
        picker.delegate = self;
        [controller presentViewController:picker animated:YES completion:nil];
    }
    return YES;
}

- (BOOL)startLibraryPickerFromViewController:(UIViewController*)controller usingDelegate:(id<UIImagePickerControllerDelegate>)delegateObject
{

    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
    {
        UIImagePickerController *picker1 = [[UIImagePickerController alloc]init];
        picker1.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        picker1.allowsEditing = YES;
        picker1.delegate = self;
        [controller presentViewController:picker1 animated:YES completion:nil];
        
    }
    return YES;
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage * img = [info objectForKey:UIImagePickerControllerOriginalImage];

    
    [self useImage:img];
    
    [[picker parentViewController] dismissModalViewControllerAnimated:NO];
    UINavigationController* navController = self.navigationController;
    UIViewController* controller = [navController.viewControllers objectAtIndex:0];
    [controller dismissViewControllerAnimated:YES completion:nil];

}

-(void)useImage:(UIImage *)theImage
{
    UIImage *compressedImage = [self compressImage:theImage];
    NSData *addImageData = UIImagePNGRepresentation(compressedImage);
    
    [_taskImage setImage:[UIImage imageWithData:addImageData]];
    [_cameraImageView setHidden:YES];
    [_editButton setHidden:NO];
    [_zoomButton setHidden:NO];
    [_deleteBtn setHidden:NO];

    if (theImage != nil)
    {
        
        int r = arc4random() % 1000;
        NSString *random = [NSString stringWithFormat:@"%d",r];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                             NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent:
                          [NSString stringWithFormat:@"%@",random]];
        NSData* data = UIImagePNGRepresentation(theImage);
        [data writeToFile:path atomically:YES];
        _savedImagePath = [NSString stringWithFormat:@"%@",random];
        
    }

    
}

- (IBAction)saveTaskAction:(id)sender {
    
    if ([_taskTitleTF.text isEqualToString:@""] || [_assigneeTF.text isEqualToString:@""])
        
    {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Warning", nil)
            message:NSLocalizedString(@"ValidationError", nil)
            preferredStyle:UIAlertControllerStyleAlert];
        
        
        //We add buttons to the alert controller by creating UIAlertActions:
        UIAlertAction *actionOk = [UIAlertAction actionWithTitle:NSLocalizedString(@"Ok", nil)
                                                           style:UIAlertActionStyleDestructive
                                                         handler:nil]; //You can use a block here to handle a press on this button
        
        
        [alertController addAction:actionOk];
        [self presentViewController:alertController animated:YES completion:nil];
        
    }
    
    else
    {
        MBProgressHUD *hud2 = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        // NSData *myData = [NSData dataWithContentsOfFile:_imagePath];
        
        UIImage *compressedImage = [self compressImage:_taskImage.image];
        
        NSData *imageData = UIImagePNGRepresentation(compressedImage);
        
//        [TasksResquest addTaskWithComment:_taskDescreption.text andTitle:_taskTitleTF.text andUserid:_userId andTaskId:0 andImagePath:_imagePath andImageData:imageData andStartingDate:_startDateTF.text andEndingDate:_endDateTF.text andAgencyid:_agencyId success:^(NSDictionary *comments) {
//            [hud2 hideAnimated:YES];
        
        [TasksResquest addTaskWithComment:_taskDescreption.text andTitle:_taskTitleTF.text andAssigneeid:_assigneId andTaskId:0 andImagePath:_imagePath andImageData:imageData andStartingDate:_startDateTF.text andEndingDate:_endDateTF.text  andAgencyid:_agencyId andUserid:_userId andSourceId:_sourceId andMeetingNumber:[_meetingNumberTF.text integerValue] success:^(NSDictionary *comments) {
            
            [hud2 hideAnimated:YES];

            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"NavigationController"];
            
            [navigationController setViewControllers:@[[storyboard instantiateViewControllerWithIdentifier:@"TasksHolderViewController"]]];
            
            MainViewController *mainViewController = [storyboard instantiateInitialViewController];
            mainViewController.rootViewController = navigationController;
            
            UIWindow *window = UIApplication.sharedApplication.delegate.window;
            window.rootViewController = mainViewController;
            [UIView transitionWithView:window
                              duration:0.3
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:nil
                            completion:nil];
            
        } failure:^(NSError *error) {
            
            NSLog(@"Error = %@",error);
            
            NSString *query;
            if (_savedImagePath) {
                query = [NSString stringWithFormat:@"insert into OfflineTasks values(null, '%@', '%@', %ld,'%@','%@','%@','%@','%@')", _taskTitleTF.text, _taskDescreption.text, (long)_userId, _savedImagePath , _startDateTF.text, _endDateTF.text, _assigneeTF.text, _idsAgency];

            }
            
            else
            {
                
                _savedImagePath = @"";
                query = [NSString stringWithFormat:@"insert into OfflineTasks values(null, '%@', '%@', %ld,'%@','%@','%@','%@','%@')", _taskTitleTF.text, _taskDescreption.text, (long)_userId, _savedImagePath , _startDateTF.text, _endDateTF.text, _assigneeTF.text, _idsAgency];

            }
            // Execute the query.
            [self.dbManager executeQuery:query];
            
            // If the query was successfully executed then pop the view controller.
            if (self.dbManager.affectedRows != 0) {
                NSLog(@"Query was executed successfully. Affected rows = %d", self.dbManager.affectedRows);
                
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"NavigationController"];
                
                [navigationController setViewControllers:@[[storyboard instantiateViewControllerWithIdentifier:@"TasksHolderViewController"]]];
                
                MainViewController *mainViewController = [storyboard instantiateInitialViewController];
                mainViewController.rootViewController = navigationController;
                
                UIWindow *window = UIApplication.sharedApplication.delegate.window;
                window.rootViewController = mainViewController;
                [UIView transitionWithView:window
                                  duration:0.3
                                   options:UIViewAnimationOptionTransitionCrossDissolve
                                animations:nil
                                completion:nil];
                
            }
            else{
                NSLog(@"Could not execute the query.");
            }
            
            
            [hud2 hideAnimated:YES];
            
        }];
        
        
    }
    

    
}

- (IBAction)discardTask:(id)sender {
    
    SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
    
    [alert addButton:NSLocalizedString(@"Discard", nil) target:self selector:@selector(discardButton)];

    [alert showError:NSLocalizedString(@"discardTitle", nil) subTitle:NSLocalizedString(@"discardmsg", nil) closeButtonTitle:NSLocalizedString(@"cancel", nil) duration:0.0f];
}

- (IBAction)editAction:(id)sender {
    
    LGAlertView *alertView = [[LGAlertView alloc] initWithTitle:NSLocalizedString(@"TitleLabel", nil)
                                                        message:NSLocalizedString(@"PhotoSource", nil)
                                                          style:LGAlertViewStyleActionSheet
                                                   buttonTitles:@[NSLocalizedString(@"Camera", nil),
                                                                  NSLocalizedString(@"Photo", nil),NSLocalizedString(@"", nil)]
                                              cancelButtonTitle:NSLocalizedString(@"cancel", nil)
                                         destructiveButtonTitle:nil
                                                       delegate:self];
    
    
    tag = 0;
    
    alertView.windowLevel = LGAlertViewWindowLevelBelowStatusBar;
    [alertView showAnimated:YES completionHandler:nil];

    
    
}

- (IBAction)zoomAction:(id)sender {
    
    // Or use this constructor to receive an NSArray of IDMPhoto objects from your NSURL objects
    NSArray *photos = [NSArray arrayWithObject:[IDMPhoto photoWithImage:_taskImage.image]];
    
    IDMPhotoBrowser *browser = [[IDMPhotoBrowser alloc] initWithPhotos:photos animatedFromView:self.view];
    browser.displayArrowButton = NO;
    browser.displayActionButton = NO;
    
    [self presentViewController:browser animated:YES completion:nil];
    
//[EXPhotoViewer showImageFrom:_taskImage];

}

- (IBAction)deleteAction:(id)sender {
    
    
    UIImage * img = [UIImage imageNamed:@"img_no_image"];
    
    [self useImage:img];
}

-(void)discardButton {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}
-(void)addDueDate{

    tag = 1;

    _startDatePicker = [UIDatePicker new];
    _startDatePicker.datePickerMode = UIDatePickerModeDate;
    //_startDatePicker.minimumDate = [NSDate date];
    _startDatePicker.maximumDate = [_endDatePicker date];

    _startDatePicker.frame = CGRectMake(0.0, 0.0, _startDatePicker.frame.size.width, 160.0);
    
    [[[LGAlertView alloc] initWithViewAndTitle:NSLocalizedString(@"StartingLabel", nil)
                                       message:NSLocalizedString(@"StartingDatePickerTitle", nil)
                                         style:LGAlertViewStyleActionSheet
                                          view:_startDatePicker
                                  buttonTitles:@[NSLocalizedString(@"Done", nil)]
                             cancelButtonTitle:NSLocalizedString(@"cancel", nil)
                        destructiveButtonTitle:nil
                                      delegate:self] showAnimated:YES completionHandler:nil];
    


}


-(void)addEndDate {
    
    tag = 2;

    _endDatePicker = [UIDatePicker new];
    _endDatePicker.datePickerMode = UIDatePickerModeDate;
    _endDatePicker.minimumDate = _startDatePicker.date;
    
    _endDatePicker.frame = CGRectMake(0.0, 0.0, _endDatePicker.frame.size.width, 160.0);
    
    [[[LGAlertView alloc] initWithViewAndTitle:NSLocalizedString(@"EndingLabel", nil)
                                       message:NSLocalizedString(@"EndingDatePickerTitle", nil)
                                         style:LGAlertViewStyleActionSheet
                                          view:_endDatePicker
                                  buttonTitles:@[NSLocalizedString(@"Done", nil)]
                             cancelButtonTitle:NSLocalizedString(@"cancel", nil)
                        destructiveButtonTitle:nil
                                      delegate:self] showAnimated:YES completionHandler:nil];

    

}


-(UIImage *)compressImage:(UIImage *)image{
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    float maxHeight = 1000.0;
    float maxWidth = 1000.0;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    float compressionQuality = 0.8;//50 percent compression
    if (actualHeight > maxHeight || actualWidth > maxWidth){
        if(imgRatio < maxRatio){
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio){
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }
        else{
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
    }
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [image drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    NSData *imageData = UIImageJPEGRepresentation(img, compressionQuality);
    UIGraphicsEndImageContext();
    return [UIImage imageWithData:imageData];
}


-(void)addAssingee
{
    LGAlertView *alertView = [[LGAlertView alloc] initWithTitle:NSLocalizedString(@"AssigneeList", nil)
                                                        message:NSLocalizedString(@"ChooseAssignee", nil)
                                                          style:LGAlertViewStyleAlert
                                                   buttonTitles:_usersArray
                                              cancelButtonTitle:NSLocalizedString(@"cancel", nil)
                                         destructiveButtonTitle:nil
                                                       delegate:self];
    
    tag = 3;
    
    alertView.windowLevel = LGAlertViewWindowLevelBelowStatusBar;
    [alertView showAnimated:YES completionHandler:nil];

}

-(void)addAgency
{
    LGAlertView *alertView = [[LGAlertView alloc] initWithTitle:NSLocalizedString(@"AgencyList", nil)
                                                        message:NSLocalizedString(@"ChooseAgency", nil)
                                                          style:LGAlertViewStyleAlert
                                                   buttonTitles:_agencyArray
                                              cancelButtonTitle:NSLocalizedString(@"cancel", nil)
                                         destructiveButtonTitle:nil
                                                       delegate:self];
    
    tag = 4;
    
    alertView.windowLevel = LGAlertViewWindowLevelBelowStatusBar;
    [alertView showAnimated:YES completionHandler:nil];
    
}

-(void)addSource
{
    LGAlertView *alertView = [[LGAlertView alloc] initWithTitle:NSLocalizedString(@"Sources List", nil)
                                                        message:NSLocalizedString(@"", nil)
                                                          style:LGAlertViewStyleAlert
                                                   buttonTitles:_sourcesArray
                                              cancelButtonTitle:NSLocalizedString(@"cancel", nil)
                                         destructiveButtonTitle:nil
                                                       delegate:self];
    
    tag = 5;
    
    alertView.windowLevel = LGAlertViewWindowLevelBelowStatusBar;
    [alertView showAnimated:YES completionHandler:nil];
    
}



@end
