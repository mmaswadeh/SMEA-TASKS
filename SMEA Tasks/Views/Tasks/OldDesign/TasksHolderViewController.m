//
//  TasksHolderViewController.m
//  PMO-BTIT
//
//  Created by Mohammad Maswadeh on 8/21/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import "TasksHolderViewController.h"
#import "AddTaskViewController.h"
#import "TasksViewController.h"
#import "OutboxViewController.h"
#import "TaskDetailsViewController.h"
#import "Tasks.h"
#import "MainViewController.h"
#import "MyTasksViewController.h"
#import "MyTasksDetailsViewController.h"
#import "TaskDetailsRequest.h"
#import "Delays.h"
#import "MBProgressHUD.h"
#import "TasksResquest.h"
#import "SCLAlertView.h"


@interface TasksHolderViewController () <TaskViewDelegate,MyTasksViewDelegate,MyTaskDetailsViewDelegate>
//
{

  NSInteger  StatusLkpIdIS;
      NSInteger  StatusLkpIdIS2;
    NSInteger  myTasksPieFilterId;
    NSInteger  tasksPieFilterId;
    NSInteger myTasksAgencyFilter;
    NSInteger tasksAgencyFilter;
    NSInteger filteredAgencyId;


}
@property (nonatomic) CAPSPageMenu *pageMenu;
//@property (weak, nonatomic) IBOutlet UIView *holderView;
@property (weak, nonatomic) IBOutlet UIView *layoutView;


@end

@implementation TasksHolderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title =NSLocalizedString(@"TasksList", nil);
    
    
    UIBarButtonItem * leftButton= [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu"] style:UIBarButtonItemStylePlain target:self action:@selector(ShowLeftView:)];
    
    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc]
                                    initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                                    target:self action:@selector(AddTask:)];
    
    self.navigationItem.leftBarButtonItem = leftButton;
    self.navigationItem.rightBarButtonItem = rightButton;
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    _filteredChartList1=[[NSMutableArray alloc]init];
    _filteredChartList2=[[NSMutableArray alloc]init];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];

 
    StatusLkpIdIS =(long)[userDefaults  integerForKey:@"statusIDFilter"];
    StatusLkpIdIS2 =(long)[userDefaults  integerForKey:@"statusIDFilter2"];
    myTasksPieFilterId =(long)[userDefaults  integerForKey:@"myTasksPieFilterId"];
    tasksPieFilterId =(long)[userDefaults  integerForKey:@"tasksPieFilterId"];
    myTasksAgencyFilter =(long)[userDefaults  integerForKey:@"myTasksAgencyFilter"];
    tasksAgencyFilter =(long)[userDefaults  integerForKey:@"tasksAgencyFilter"];
    filteredAgencyId = (long)[userDefaults  integerForKey:@"agencyFilteredId"];
    
    if (StatusLkpIdIS ||StatusLkpIdIS2 ) {
        //[self loadViews];
        [self readFromFilter];
    }
    
    else if (tasksPieFilterId || myTasksPieFilterId) {
        [self readFromPieFilter];

    }
    
    else if (tasksAgencyFilter || myTasksAgencyFilter)
        
    {
        [self readAgencyFilter];
    }
    else{
        [self loadViews];
    }
    
    // Do any additional setup after loading the view.
}



-(IBAction)ShowLeftView:(id)sender
{

    NSString * language = [[NSString alloc]init];
    
    _languageSettings = [[languageHelper alloc]init];
    language= [_languageSettings checkLanguages];
    if ([language isEqualToString:@"ar"]) {
        
        [self.sideMenuController showRightViewAnimated:YES completionHandler:nil];}
    else{
        [self.sideMenuController showLeftViewAnimated:YES completionHandler:nil];
    }
}

-(void) loadViews {
    
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    TasksViewController *vcTasksView = [storyboard instantiateViewControllerWithIdentifier:@"TasksViewController"];
    vcTasksView.tasksArray=_filteredChartList1;
    
    vcTasksView.taskDelegate = self;
    vcTasksView.title = NSLocalizedString(@"Tasks", nil);
    
    
    MyTasksViewController *vcMyTasks = [storyboard instantiateViewControllerWithIdentifier:@"MyTasksViewController"];
    
    
    vcMyTasks.myTasksArray = _filteredChartList2;
    vcMyTasks.myTaskDelegate = self;
    vcMyTasks.title = NSLocalizedString(@"MyTasks", nil);
    
    OutboxViewController *vcOutboxView = [storyboard instantiateViewControllerWithIdentifier:@"OutboxViewController"];
    vcOutboxView.title = NSLocalizedString(@"Outbox", nil);
    
    NSArray *controllerArray = @[vcTasksView, vcMyTasks, vcOutboxView];
    NSDictionary *parameters = @{
                                 CAPSPageMenuOptionScrollMenuBackgroundColor: [UIColor colorWithRed:69.0/255.0 green:154.0/255.0 blue:165.0/255.0 alpha:1.0],
                                 CAPSPageMenuOptionSelectedMenuItemLabelColor:[UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0],
                                 CAPSPageMenuOptionViewBackgroundColor: [UIColor colorWithRed:69.0/255.0 green:154.0/255.0 blue:165.0/255.0 alpha:1.0],
                                 CAPSPageMenuOptionSelectionIndicatorColor: [UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0],
                                 CAPSPageMenuOptionBottomMenuHairlineColor: [UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0],
                                 CAPSPageMenuOptionMenuItemFont: [UIFont fontWithName:@"HelveticaNeue-bold" size:14.0],
                                 CAPSPageMenuOptionMenuHeight: @(50.0),
                                 CAPSPageMenuOptionMenuItemWidth: @(self.view.frame.size.width/[controllerArray count]),
                                 CAPSPageMenuOptionCenterMenuItems: @(YES)
                
                                 };
    
   
    _pageMenu = [[CAPSPageMenu alloc] initWithViewControllers:controllerArray frame:CGRectMake(0.0, 0.0, self.view.frame.size.width, self.view.frame.size.height) options:parameters];
    
        [self.view addSubview:_pageMenu.view];

}

-(void)readAgencyFilter {
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    NSInteger userId = [userDefaults integerForKey:@"UserID"];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [TasksResquest getAllTasksWithUserId:userId Success:^(NSMutableArray *assinedByMe, NSMutableArray *assinedToMe) {
        
        // _tasksArray = [[tasksList reverseObjectEnumerator] allObjects];
        _tasksArray1 = [NSArray arrayWithArray:assinedToMe];
        _tasksArray2 = [NSArray arrayWithArray:assinedByMe];
        
        
        if (myTasksAgencyFilter>0) {
            for (Tasks *myObject in self.tasksArray1) {
                
                //Tasks *myObject = (Tasks *)tempEndDate;
                
                if (myObject.taskStatus ==myTasksAgencyFilter && myObject.Agency.agencyId ==filteredAgencyId) {
                    
                    [_filteredChartList2 addObject:myObject];
                }
                
                
            }
        }
        
        if (tasksAgencyFilter>0) {
            for (Tasks *myObject in self.tasksArray2) {
                
                //Tasks *myObject = (Tasks *)tempEndDate;
                
                if (myObject.taskStatus ==tasksAgencyFilter && myObject.Agency.agencyId ==filteredAgencyId) {
                    [_filteredChartList1 addObject:myObject];
                }
                
                
            }
        }
        
        [self loadViews];
        if(myTasksAgencyFilter > 0){
            
            [_pageMenu moveToPage:1];
            
        }
        else if (tasksAgencyFilter > 0){
            
            [_pageMenu moveToPage:0];
        }
        
        
        [userDefaults removeObjectForKey:@"tasksAgencyFilter"];
        [userDefaults removeObjectForKey:@"myTasksAgencyFilter"];
        [userDefaults removeObjectForKey:@"agencyFilteredId"];

        [hud hideAnimated:YES];
        
        
    } failure:^(NSError *error) {
        [hud hideAnimated:YES];
        NSLog(@"Error = %@",error);
        SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
        [alert showError:@"Failed" subTitle:error.localizedDescription closeButtonTitle:NSLocalizedString(@"Done", nil) duration:0.0f];
        
        
        NSLog(@"Failed = %@",error);
        
        
    }];
    
}

-(void)readFromFilter{
    
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];

    NSInteger userId = [userDefaults integerForKey:@"UserID"];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [TasksResquest getAllTasksWithUserId:userId Success:^(NSMutableArray *assinedByMe, NSMutableArray *assinedToMe) {
    
        // _tasksArray = [[tasksList reverseObjectEnumerator] allObjects];
        _tasksArray1 = [NSArray arrayWithArray:assinedByMe];
        _tasksArray2 = [NSArray arrayWithArray:assinedToMe];

        
        if (StatusLkpIdIS>0) {
            for (Tasks *myObject in self.tasksArray1) {
                
                //Tasks *myObject = (Tasks *)tempEndDate;
                
                if (myObject.TaskPerfomanceStatus ==StatusLkpIdIS) {
                    [_filteredChartList1 addObject:myObject];
                }
                
                
            }
        }
        
        if (StatusLkpIdIS2>0) {
            for (Tasks *myObject in self.tasksArray2) {
                
                //Tasks *myObject = (Tasks *)tempEndDate;
                
                if (myObject.TaskPerfomanceStatus ==StatusLkpIdIS2) {
                    [_filteredChartList2 addObject:myObject];
                }
                
                
            }
        }
        
         [self loadViews];
        if(StatusLkpIdIS > 0){
            
            [_pageMenu moveToPage:0];
            
        }
        else if (StatusLkpIdIS2 > 0){
            
            [_pageMenu moveToPage:1];
        }
        
        
        [userDefaults removeObjectForKey:@"statusIDFilter"];
        [userDefaults removeObjectForKey:@"statusIDFilter2"];
        [hud hideAnimated:YES];
        
        
    } failure:^(NSError *error) {
       [hud hideAnimated:YES];
        NSLog(@"Error = %@",error);
        SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
        [alert showError:@"Failed" subTitle:error.localizedDescription closeButtonTitle:NSLocalizedString(@"Done", nil) duration:0.0f];
        
        
        NSLog(@"Failed = %@",error);
        
        
    }];

}

-(void)readFromPieFilter{
    
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    NSInteger userId = [userDefaults integerForKey:@"UserID"];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [TasksResquest getAllTasksWithUserId:userId Success:^(NSMutableArray *assinedByMe, NSMutableArray *assinedToMe) {
        
        // _tasksArray = [[tasksList reverseObjectEnumerator] allObjects];
        _tasksArray1 = [NSArray arrayWithArray:assinedToMe];
        _tasksArray2 = [NSArray arrayWithArray:assinedByMe];
        
        
        if (myTasksPieFilterId>0) {
            for (Tasks *myObject in self.tasksArray1) {
                
                //Tasks *myObject = (Tasks *)tempEndDate;
                
                if (myObject.taskStatus ==myTasksPieFilterId) {
                    
                    [_filteredChartList2 addObject:myObject];
                }
                
                
            }
        }
        
        if (tasksPieFilterId>0) {
            for (Tasks *myObject in self.tasksArray2) {
                
                //Tasks *myObject = (Tasks *)tempEndDate;
                
                if (myObject.taskStatus ==tasksPieFilterId) {
                    [_filteredChartList1 addObject:myObject];
                }
                
                
            }
        }
        
        [self loadViews];
        if(myTasksPieFilterId > 0){
            
            [_pageMenu moveToPage:1];
            
        }
        else if (tasksPieFilterId > 0){
            
            [_pageMenu moveToPage:0];
        }
        
                
        [userDefaults removeObjectForKey:@"myTasksPieFilterId"];
        [userDefaults removeObjectForKey:@"tasksPieFilterId"];
        [hud hideAnimated:YES];
        
        
    } failure:^(NSError *error) {
        [hud hideAnimated:YES];
        NSLog(@"Error = %@",error);
        SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
        [alert showError:@"Failed" subTitle:error.localizedDescription closeButtonTitle:NSLocalizedString(@"Done", nil) duration:0.0f];
        
        
        NSLog(@"Failed = %@",error);
        
        
    }];
    
}

-(IBAction)AddTask:(id)sender
{
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AddTaskViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"AddTaskViewController"];
    
    [self.navigationController pushViewController:controller animated:YES];
}

- (void)didPressMyTask:(NSInteger)TaskID andMyTaskArray:(NSArray*)taskArr {
    
    [TaskDetailsRequest getTaskDetailsWithID:TaskID sucess:^(Tasks *tasksitem) {
        
        UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        MyTasksDetailsViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"MyTasksDetailsViewController"];
        
        controller.ClouserType=tasksitem.Clousertype;
        controller.ClouserMessage=tasksitem.ClouserMessage;
        controller.delaysArr=tasksitem.Delays;
        controller.myTaskTitle = tasksitem.title;
        controller.myTaskDescription = tasksitem.taskDescription;
        controller.startingDate = tasksitem.startingDate;
        controller.endingDate = tasksitem.endingDate;
        controller.assigneeUserName = tasksitem.assigneeUserName;
        controller.myTaskId = tasksitem.taskId;
        controller.filesUrl = tasksitem.filesUrl;
        controller.percent = tasksitem.completePercentage;
        controller.color = tasksitem.taskColor;
        controller.userID = tasksitem.AssigneeUserId;
        controller.myTaskStatusName = tasksitem.statusText;
        controller.agencyName=tasksitem.Agency.name;
        controller.agencyId = tasksitem.Agency.agencyId;
        controller.sourceId=tasksitem.Sources.sourceId;
        controller.sourceName=tasksitem.Sources.sourceName;
        controller.MeetingNumberLbl=tasksitem.MeetingNumber;
        Delays *delayObject = [tasksitem.Delays lastObject];
        
        
        controller.lastDelayDate = [self extractStringValue:delayObject.NewEndDate];
        NSLog(@"String = %@",[self extractStringValue:delayObject.NewEndDate]);
        
        [self.navigationController pushViewController:controller animated:YES];
        
        
        
    } failure:^(NSError * error) {
        NSLog(@"ERROR : %@",error);
    }];
    
    
}

//-(NSString *)extractStringValue:(id)value{
//    
//    NSString *string = @"";
//    
//    if (value != nil && ![value isEqual:[NSNull null]]) {
//        string = value;
//    }
//    
//    return string;
//}


- (void)didPressTask:(NSInteger)TaskID andTaskArray:(NSArray*)taskArr;
{
    
   
    [TaskDetailsRequest getTaskDetailsWithID:TaskID sucess:^(Tasks *tasksitem) {
   
        UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        TaskDetailsViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"TaskDetailsViewController"];
        
        controller.ClouserType=tasksitem.Clousertype;
        controller.ClouserMessage=tasksitem.ClouserMessage;
        controller.delaysArr=tasksitem.Delays;
        controller.taskTitle = tasksitem.title;
        controller.taskDescription = tasksitem.taskDescription;
        controller.startingDate = tasksitem.startingDate;
        controller.endingDate = tasksitem.endingDate;
        controller.assigneeUserName = tasksitem.assigneeUserName;
        controller.taskId = tasksitem.taskId;
        controller.filesUrl = tasksitem.filesUrl;
        controller.percent = tasksitem.completePercentage;
        controller.color = tasksitem.taskColor;
        controller.userID = tasksitem.AssigneeUserId;
        controller.taskStatusName = tasksitem.statusText;
        controller.agencyName=tasksitem.Agency.name;
        controller.agencyId = tasksitem.Agency.agencyId;
        controller.sourceName= tasksitem.Sources.sourceName;
        controller.sourceId=tasksitem.Sources.sourceId;
        controller.MeetingNumberLbl=tasksitem.MeetingNumber;
        
        [self.navigationController pushViewController:controller animated:YES];
        
        
        
    } failure:^(NSError * error) {
        NSLog(@"ERROR : %@",error);
    }];
    
    
    //tasksss
    
    
}

- (void)didUpdateTheProgressTask:(NSInteger)TaskId {
    
    NSLog(@"TaskId");
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
