//
//  TasksViewController.m
//  PMO-BTIT
//
//  Created by Mohammad Maswadeh on 8/3/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import "TasksViewController.h"
#import "AddTaskViewController.h"
#import "TasksCell.h"
#import "TasksResquest.h"
#import "MBProgressHUD.h"
#import "SCLAlertView.h"
#import "Tasks.h"
#import "EditTaskViewController.h"
#import "TaskDetailsViewController.h"

@interface TasksViewController (){
    
    BOOL isSearching;
   
    
}
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet UISearchController *searchBarController;
@end

@implementation TasksViewController
- (void)viewDidLoad {
    [super viewDidLoad];
       self.filteredContentList = [[NSMutableArray alloc] init];
    
  
  
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(handleRefresh) forControlEvents:UIControlEventValueChanged];
    //[self.mytable addSubview:refreshControl];
    self.taskTableView.refreshControl = refreshControl;
    
    //self.navigationItem.title =NSLocalizedString(@"Tasks", nil);
    
    self.taskTableView.delegate = self;
    self.taskTableView.dataSource = self;
    
//    UIBarButtonItem * leftButton= [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu"] style:UIBarButtonItemStylePlain target:self action:@selector(ShowLeftView:)];
//    
//    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc]
//                                    initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
//                                    target:self action:@selector(AddTask:)];
//    
//    self.navigationItem.leftBarButtonItem = leftButton;
//    self.navigationItem.rightBarButtonItem = rightButton;
//    
//    [self.navigationController.navigationBar setTitleTextAttributes:
//     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    
    if ([_tasksArray  count]>0) {
        
        NSLog(@"filters array%@",_tasksArray);
        [self.taskTableView reloadData];
    }
    else{
        
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:_taskTableView animated:YES];
        
        
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        NSInteger userId = [userDefaults integerForKey:@"UserID"];
        
        [TasksResquest getAllTasksWithUserId:userId Success:^(NSMutableArray *assinedByMe, NSMutableArray *assinedToMe) {
            
            [hud hideAnimated:YES];
            
            //            _tasksArray = [[tasksList reverseObjectEnumerator] allObjects];
            _tasksArray = [NSArray arrayWithArray:assinedByMe];
            
            
            [self.taskTableView reloadData];
            
            
            NSLog(@"Tasks array = %@",_tasksArray);
            
            
            
        } failure:^(NSError *error) {
            
            NSLog(@"Error = %@",error);
            SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
            [alert showError:@"Failed" subTitle:error.localizedDescription closeButtonTitle:NSLocalizedString(@"Done", nil) duration:0.0f];
            [hud hideAnimated:YES];
            
            NSLog(@"Failed = %@",error);
            
            
        }];
        
    }
    // Do any additional setup after loading the view.
}

-(void)viewWillDisappear:(BOOL)animated {
    
    [self.searchBar resignFirstResponder];
    //[_searchBarController setActive:NO];
}

//-(void)viewDidDisappear:(BOOL)animated {
//    [_searchBarController setActive:NO];
//    
//}
//-(void)viewWillLayoutSubviews {
//    [_searchBarController setActive:NO];
//    
//}

-(void)handleRefresh
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSInteger userId = [userDefaults integerForKey:@"UserID"];
    
    [TasksResquest getAllTasksWithUserId:userId Success:^(NSMutableArray *assinedByMe, NSMutableArray *assinedToMe) {
        
        
        _tasksArray = [NSArray arrayWithArray:assinedByMe];
        
        [self.taskTableView reloadData];
        
        [self.taskTableView.refreshControl endRefreshing];
        
        NSLog(@"Tasks array = %@",_tasksArray);
        
        
        
    } failure:^(NSError *error) {
        
        NSLog(@"Error = %@",error);
        SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
        [alert showError:@"Failed" subTitle:error.localizedDescription closeButtonTitle:NSLocalizedString(@"Done", nil) duration:0.0f];
        [self.taskTableView.refreshControl endRefreshing];
        
        NSLog(@"Failed = %@",error);
        
        
    }];
    
    
}
//-(IBAction)ShowLeftView:(id)sender
//{
//    NSString * language = [[NSString alloc]init];
//
//    _languageSettings = [[languageHelper alloc]init];
//    language= [_languageSettings checkLanguages];
//    if ([language isEqualToString:@"ar"]) {
//
//        [self.sideMenuController showRightViewAnimated:YES completionHandler:nil];}
//    else{
//        [self.sideMenuController showLeftViewAnimated:YES completionHandler:nil];
//    }
//}
//
//-(IBAction)AddTask:(id)sender
//{
//
//
//    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    AddTaskViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"AddTaskViewController"];
//
//    [self.navigationController pushViewController:controller animated:YES];
//
//
//}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if ([_searchBarController isActive]) {
        
        // if (isSearching) {
        return [_filteredContentList count];
    }
    else {
        return [_tasksArray count];
    }
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    
    static NSString *simpleTableIdentifier = @"TasksCell";
    TasksCell *taskCell = [TasksCell customTaskCell];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];

  //  [taskCell setValuesOfTasksList:tasksObj];
    cell=taskCell;
    if ([_searchBarController isActive]) {
        
        //  if (isSearching) {
        Tasks *tasksObj=[_filteredContentList objectAtIndex:indexPath.row];
        
        [taskCell setValuesOfTasksList:tasksObj];
        
    }
    else {
        Tasks *tasksObj=[_tasksArray objectAtIndex:indexPath.row];
        
        [taskCell setValuesOfTasksList:tasksObj];
        
        
    }
    
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    //   isSearching = NO;
    
    if (self.taskDelegate) {
        
        if ([_searchBarController isActive]) {
            
            // if (isSearching) {
             Tasks *tasksObj=[_filteredContentList objectAtIndex:indexPath.row];
            [self.taskDelegate didPressTask:tasksObj.taskId andTaskArray:_filteredContentList];
            
        }
        
        else
        {
             Tasks *tasksObj=[_tasksArray objectAtIndex:indexPath.row];
            [self.taskDelegate didPressTask:tasksObj.taskId andTaskArray:_tasksArray];
            
        }
        
    }
    
    
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 144;
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)searchTableList {
    NSString *searchString = _searchBar.text;
    
    for (NSString *tempTitle in self.tasksArray) {
        
        Tasks *myObject = (Tasks *)tempTitle;
        
        
        if ([myObject.title length]>=[searchString length]) {
            NSComparisonResult result = [myObject.title compare:searchString options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch) range:NSMakeRange(0, [searchString length])];
            if (result == NSOrderedSame) {
                
                [_filteredContentList addObject:myObject];
            }
        }
        
    }
    
    for (NSString *tempStartDate in self.tasksArray) {
        
        Tasks *myObject = (Tasks *)tempStartDate;
        
        
        if ([myObject.startingDate length]>=[searchString length]) {
            NSComparisonResult result = [myObject.startingDate compare:searchString options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch) range:NSMakeRange(0, [searchString length])];
            if (result == NSOrderedSame) {
                
                [_filteredContentList addObject:myObject];
            }
        }
        
    }
    
    for (NSString *tempEndDate in self.tasksArray) {
        
        Tasks *myObject = (Tasks *)tempEndDate;
        
        
        if ([myObject.endingDate length]>=[searchString length]) {
            NSComparisonResult result = [myObject.endingDate compare:searchString options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch) range:NSMakeRange(0, [searchString length])];
            if (result == NSOrderedSame) {
                
                [_filteredContentList addObject:myObject];
            }
        }
        
    }
    
    for (NSString *tempAssignee in self.tasksArray) {
        
        Tasks *myObject = (Tasks *)tempAssignee;
        
        
        if ([myObject.assigneeUserName length]>=[searchString length]) {
            NSComparisonResult result = [myObject.assigneeUserName compare:searchString options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch) range:NSMakeRange(0, [searchString length])];
            if (result == NSOrderedSame) {
                
                [_filteredContentList addObject:myObject];
            }
        }
        
        for (NSString *tempAgency in self.tasksArray) {
            
            Tasks *myObject = (Tasks *)tempAgency;
            
            
            if ([myObject.Agency.name length]>=[searchString length]) {
                NSComparisonResult result = [myObject.Agency.name compare:searchString options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch) range:NSMakeRange(0, [searchString length])];
                if (result == NSOrderedSame) {
                    
                    [_filteredContentList addObject:myObject];
                }
            }
            
        }
        
        
        for (NSString *tempStatus in self.tasksArray) {
            
            Tasks *myObject = (Tasks *)tempStatus;
            
            
            if ([myObject.statusText length]>=[searchString length]) {
                NSComparisonResult result = [myObject.statusText compare:searchString options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch) range:NSMakeRange(0, [searchString length])];
                if (result == NSOrderedSame) {
                    
                    [_filteredContentList addObject:myObject];
                }
            }
            
        }
        
        
    }
    
    
    
}


- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    isSearching = YES;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    NSLog(@"Text change - %d",isSearching);
    
    //Remove all objects first.
    [_filteredContentList removeAllObjects];
    
    if([searchText length] != 0) {
        isSearching = YES;
        [self searchTableList];
    }
    else {
        isSearching = NO;
    }
    // [self.tblContentList reloadData];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    NSLog(@"Cancel clicked");
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    NSLog(@"Search Clicked");
    [self searchTableList];
}



@end

