//
//  AddTaskViewController.h
//  PMO-BTIT
//
//  Created by Mohammad Maswadeh on 8/3/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SZTextView.h"
#import <LGAlertView/LGAlertView.h>
#import "PMCalendar.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "UIViewController+LGSideMenuController.h" 
#import "IDMPhoto.h"
#import "IDMPhotoBrowser.h"
#import "languageHelper.h"
//#import "EXPhotoViewer.h"
#import <AVFoundation/AVFoundation.h>

@interface AddTaskViewController : UIViewController <UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,LGAlertViewDelegate,PMCalendarControllerDelegate,UITextFieldDelegate,AVAudioPlayerDelegate>
@property (weak, nonatomic) IBOutlet UIButton *HoldToRecordBtn;
@property (weak, nonatomic) IBOutlet UILabel *selectImageLabel;
@property (nonatomic, strong) languageHelper * languageSettings;

@property (weak, nonatomic) IBOutlet UIButton *SaveBtn;
@property (weak, nonatomic) IBOutlet UIImageView *taskImage;
@property (weak, nonatomic) IBOutlet UITextField *taskTitleTF;
@property (weak, nonatomic) IBOutlet SZTextView *taskDescreption;
@property (weak, nonatomic) IBOutlet UITextField *startDateTF;
@property (weak, nonatomic) IBOutlet UITextField *endDateTF;
@property (weak, nonatomic) IBOutlet UITextField *assigneeTF;
@property (weak, nonatomic) IBOutlet UITextField *agencyTF;
@property (weak, nonatomic) IBOutlet UITextField *sourceTF;
@property (weak, nonatomic) IBOutlet UITextField *meetingNumberTF;

@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;



@property (strong, nonatomic) IBOutlet UIDatePicker *startDatePicker;
@property (strong, nonatomic) IBOutlet UIDatePicker *endDatePicker;
@property (weak, nonatomic) IBOutlet UIButton *editButton;
@property (weak, nonatomic) IBOutlet UIButton *zoomButton;
@property (weak, nonatomic) IBOutlet UIImageView *cameraImageView;
@property (strong,nonatomic)  NSArray *idsAgency;
@property (strong,nonatomic)  NSArray *agencyArray;
@property (strong,nonatomic)  NSArray *idsSource;
@property (strong,nonatomic)  NSArray *sourcesArray;
@property (strong,nonatomic)  NSArray *usersArray;
@property (strong,nonatomic)  NSArray *ids;
@property (nonatomic, assign) NSInteger userId;
@property (nonatomic, assign) NSInteger assigneId;
@property (nonatomic, assign) NSInteger agencyId;
@property (nonatomic, assign) NSInteger sourceId;

@property (nonatomic, strong) NSString *imagePath;
@property (weak, nonatomic) IBOutlet UIView *backgroundView;

- (IBAction)saveTaskAction:(id)sender;
- (IBAction)discardTask:(id)sender;
- (IBAction)editAction:(id)sender;
- (IBAction)zoomAction:(id)sender;
- (IBAction)deleteAction:(id)sender;


@end
