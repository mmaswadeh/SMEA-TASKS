//
//  TasksViewController.h
//  PMO-BTIT
//
//  Created by Mohammad Maswadeh on 8/3/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIViewController+LGSideMenuController.h"
#import "languageHelper.h"
#import "CAPSPageMenu.h"

@protocol TaskViewDelegate <NSObject>

@optional

- (void)didPressTask:(NSInteger)TaskID andTaskArray:(NSArray*)taskArr;

@end


@interface TasksViewController : UIViewController < UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate,CAPSPageMenuDelegate>

@property (weak, nonatomic) IBOutlet UITableView *taskTableView;

@property (nonatomic, strong) languageHelper * languageSettings;

@property (strong,nonatomic)  NSArray *tasksArray;
@property(nonatomic,assign)   NSInteger  StatusLkpIdtask;
@property (strong,nonatomic)  NSMutableArray *filteredContentList;
@property(weak,nonatomic)   id <TaskViewDelegate> taskDelegate;


@property (strong, nonatomic)   NSMutableArray *searchResult;
@end

