//
//  TaskDetailsViewController.h
//  PMO-BTIT
//
//  Created by Mohammad Maswadeh on 8/8/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <LGAlertView/LGAlertView.h>
#import "UIImage+FX.h"
#import "FXImageView.h"

@interface TaskDetailsViewController : UIViewController <LGAlertViewDelegate,UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UIImageView *detailsImage;
//@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
//@property (weak, nonatomic) IBOutlet UITextView *descriptionLabel;
//@property (weak, nonatomic) IBOutlet UILabel *startDateLabel;
//@property (weak, nonatomic) IBOutlet UILabel *endDateLabel;
//@property (weak, nonatomic) IBOutlet UILabel *assignToLabel;
@property (weak, nonatomic) IBOutlet UIView *backgroundView;
//@property (weak, nonatomic) IBOutlet UIView *taskStatusView;
//@property (weak, nonatomic) IBOutlet UILabel *statusName;

@property (weak, nonatomic) IBOutlet UIView *chartView;
@property (strong,nonatomic)  NSArray *usersArray;
@property (strong,nonatomic)  NSArray *ids;
@property (nonatomic, assign) NSInteger userId;
@property (nonatomic, assign) NSInteger agencyId;

@property (nonatomic, assign) NSInteger taskId;
@property (nonatomic, strong) NSString *imagePath;
@property (nonatomic, assign) NSInteger percent;
@property (nonatomic, assign) NSInteger userID;

@property (nonatomic, strong) UIColor *color;


@property (nonatomic, strong) NSString *taskTitle;
@property (nonatomic, strong) NSString *assigneeUserName;
@property (nonatomic, strong) NSString *filesUrl;
@property (nonatomic, strong) NSString *startingDate;
@property (nonatomic, strong) NSString *endingDate;
@property (nonatomic, strong) NSString *taskDescription;
@property (nonatomic, strong) NSString *taskStatusName;
@property (weak, nonatomic) IBOutlet UIButton *DeleteBtn;
@property (weak, nonatomic) IBOutlet UIButton *EditBtn;
@property (nonatomic, strong) NSString *agencyName;
@property (nonatomic, strong) NSArray  *delaysArr;
@property (nonatomic, strong) NSString *ClouserType;
@property (nonatomic, strong) NSString *ClouserMessage;

@property (nonatomic, assign) NSInteger sourceId;
@property (nonatomic, strong) NSString * sourceName;
@property (nonatomic, strong) NSString * MeetingNumberLbl;

- (IBAction)deleteAction:(id)sender;
- (IBAction)editAction:(id)sender;

// Audio Player


@property (weak, nonatomic) IBOutlet UISlider *currentTimeSlider;
@property (weak, nonatomic) IBOutlet UIButton *playButton;
@property (weak, nonatomic) IBOutlet UILabel *duration;
@property (weak, nonatomic) IBOutlet UILabel *timeElapsed;


@property BOOL isPaused;
@property BOOL scrubbing;

@property NSTimer *timer;

@property (weak, nonatomic) IBOutlet UITableView *tableView;






@end
