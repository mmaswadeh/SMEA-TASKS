//
//  TasksHolderViewController.h
//  PMO-BTIT
//
//  Created by Mohammad Maswadeh on 8/21/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CAPSPageMenu.h"
#import "UIViewController+LGSideMenuController.h"
#import "languageHelper.h"


@interface TasksHolderViewController : UIViewController
@property (strong,nonatomic)  NSArray *tasksArray1;
@property (strong,nonatomic)  NSArray *tasksArray2;
@property (nonatomic, strong) languageHelper * languageSettings;
//@property (nonatomic,strong)   NSInteger * StatusLkpIdIS;
@property (strong,nonatomic)  NSMutableArray *filteredChartList1;
@property (strong,nonatomic)  NSMutableArray *filteredChartList2;
@end

