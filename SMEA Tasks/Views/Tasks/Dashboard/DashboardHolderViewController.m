//
//  DashboardHolderViewController.m
//  MCI-Tasks
//
//  Created by Mohammad Maswadeh on 1/29/18.
//  Copyright © 2018 BTIT. All rights reserved.
//

#import "DashboardHolderViewController.h"
#import "AddTaskViewController.h"
#import "Tasks.h"
#import "MainViewController.h"
#import "MyTasksDashboardViewController.h"
#import "TasksDashboardViewController.h"
#import "UIViewController+LGSideMenuController.h"

@interface DashboardHolderViewController ()

@property (nonatomic) CAPSPageMenu *pageMenu;

@end

@implementation DashboardHolderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIBarButtonItem * leftButton= [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu"] style:UIBarButtonItemStylePlain target:self action:@selector(ShowLeftView:)];
    
    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc]
                                    initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                                    target:self action:@selector(AddTask:)];
    
    self.navigationItem.leftBarButtonItem = leftButton;
    self.navigationItem.rightBarButtonItem = rightButton;
    self.navigationItem.title = NSLocalizedString(@"ExecutiveHub", nil);
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    MyTasksDashboardViewController *controller2 = [storyboard instantiateViewControllerWithIdentifier:@"MyTasksDashboardViewController"];
    controller2.title = NSLocalizedString(@"MyTasks", nil);
    
    TasksDashboardViewController *controller1 = [storyboard instantiateViewControllerWithIdentifier:@"TasksDashboardViewController"];
    controller1.title = NSLocalizedString(@"Tasks", nil);
    

    
    NSArray *controllerArray = @[controller1, controller2];
    NSDictionary *parameters = @{
                                 CAPSPageMenuOptionScrollMenuBackgroundColor: [UIColor colorWithRed:69.0/255.0 green:154.0/255.0 blue:165.0/255.0 alpha:1.0],
                                 CAPSPageMenuOptionSelectedMenuItemLabelColor:[UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0],
                                 CAPSPageMenuOptionViewBackgroundColor: [UIColor colorWithRed:69.0/255.0 green:154.0/255.0 blue:165.0/255.0 alpha:1.0],
                                 CAPSPageMenuOptionSelectionIndicatorColor: [UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0],
                                 CAPSPageMenuOptionBottomMenuHairlineColor: [UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0],
                                 CAPSPageMenuOptionMenuItemFont: [UIFont fontWithName:@"HelveticaNeue-bold" size:14.0],
                                 CAPSPageMenuOptionMenuHeight: @(50.0),
                                 CAPSPageMenuOptionMenuItemWidth: @(self.view.frame.size.width/[controllerArray count]),
                                 CAPSPageMenuOptionCenterMenuItems: @(YES)
                                 
                                 };
    
    _pageMenu = [[CAPSPageMenu alloc] initWithViewControllers:controllerArray frame:CGRectMake(0.0, 0.0, self.layoutView.frame.size.width, self.layoutView.frame.size.height) options:parameters];
    [self.layoutView addSubview:_pageMenu.view];
    
    
    
    
    // Do any additional setup after loading the view.
}

-(IBAction)ShowLeftView:(id)sender
{
    
    NSString * language = [[NSString alloc]init];
    
    _languageSettings = [[languageHelper alloc]init];
    language= [_languageSettings checkLanguages];
    if ([language isEqualToString:@"ar"]) {
        
        [self.sideMenuController showRightViewAnimated:YES completionHandler:nil];}
    else{
        [self.sideMenuController showLeftViewAnimated:YES completionHandler:nil];
    }
}

-(IBAction)AddTask:(id)sender
{
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AddTaskViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"AddTaskViewController"];
    
    [self.navigationController pushViewController:controller animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
