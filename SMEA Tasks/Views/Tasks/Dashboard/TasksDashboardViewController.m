//
//  TasksDashboardViewController.m
//  MCI-Tasks
//
//  Created by Mohammad Maswadeh on 1/29/18.
//  Copyright © 2018 BTIT. All rights reserved.
//

#import "TasksDashboardViewController.h"
#import "MBProgressHUD.h"
#import "Dashboard.h"
#import "PMOPieChart.h"
#import "legendHelper.h"
#import "DynamicLegend.h"
#import "TasksViewController.h"
#import "TasksHolderViewController.h"
#import "MainViewController.h"
#import "AddTaskViewController.h"
#import "CellDashboardView2.h"
#import "chartData.h"
#import "PieChartVIew.h"
#import "ProgressBarChartCollectionView.h"
#import "HeaderCollection.h"
#import "AgencyCollectionView.h"

@interface TasksDashboardViewController ()<allEntityCellDelegate,PerformancePieChartDelegate> {

    NSArray * toMeDataItems;
    NSArray * toMeStatusData;
    NSArray * toMeProgressByAgency;
      NSArray * toMeProgressBySources;
    NSMutableArray *items;
}

@end

@implementation TasksDashboardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIBarButtonItem * leftButton= [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu"] style:UIBarButtonItemStylePlain target:self action:@selector(ShowLeftView:)];
    
    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc]
                                    initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                                    target:self action:@selector(AddTask:)];
    
    self.navigationItem.leftBarButtonItem = leftButton;
//    self.navigationItem.rightBarButtonItem = rightButton;

    
    _filteredChartList =[[NSMutableArray alloc]init];
    _cellNames = [NSArray arrayWithObjects:NSLocalizedString(@"New",nil),
                    NSLocalizedString(@"open",nil),
                    NSLocalizedString(@"Completed",nil),
                    NSLocalizedString(@"Closed and considered as Goal",nil),
                    NSLocalizedString(@"Closed and considered as Program",nil),
                    NSLocalizedString(@"Closed and considered as Initiative",nil),
                    NSLocalizedString(@"Closed and considered as Project",nil),
                    NSLocalizedString(@"Closed and considered as Case Study",nil), nil];
    
  //  NSLocalizedString(@"Canceled", nil),
    self.navigationItem.title = NSLocalizedString(@"TasksDashboard",nil);
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
   // [self getData];
    [self retriveData];
    self.tasksDashboardTable.delegate = self;
    self.tasksDashboardTable.dataSource = self;
    self.performanceCollection.delegate = self;
    self.performanceCollection.dataSource = self;
    
    // Do any additional setup after loading the view.
}

-(IBAction)ShowLeftView:(id)sender
{
    
    NSString * language = [[NSString alloc]init];
    
    _languageSettings = [[languageHelper alloc]init];
    language= [_languageSettings checkLanguages];
    if ([language isEqualToString:@"ar"]) {
        
        [self.sideMenuController showRightViewAnimated:YES completionHandler:nil];}
    else{
        [self.sideMenuController showLeftViewAnimated:YES completionHandler:nil];
    }
}

-(IBAction)AddTask:(id)sender
{
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AddTaskViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"AddTaskViewController"];
    
    [self.navigationController pushViewController:controller animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)retriveData {
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSInteger userId = [userDefaults integerForKey:@"UserID"];
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
 [TasksStatusRequest getTasksDashboardWithUserId:userId
                                         Success:^(NSMutableArray *toMeChartData, NSMutableArray *byMeChartData, NSMutableArray *toMeStastus, NSMutableArray *byMeStatus, NSMutableArray *ProgressStatusByAgencyToMe, NSMutableArray *ProgressStatusByAgencyByMe, NSMutableArray *ProgressStatusBySourceByMe, NSMutableArray *ProgressStatusBySourceToMe) {
                                            
        
        toMeDataItems = [NSArray arrayWithArray:byMeChartData];
        toMeStatusData = [NSArray arrayWithArray:byMeStatus];
        toMeProgressByAgency = [NSArray arrayWithArray:ProgressStatusByAgencyToMe];
         toMeProgressBySources= [NSArray arrayWithArray:ProgressStatusBySourceToMe];
        [hud hideAnimated:YES];
       
      //  [self DrawHeaderWithHeaderArray:toMeStatusData];
        
        [self.tasksDashboardTable reloadData];
        [self.performanceCollection reloadData];
        
    
    } failure:^(NSError *error) {
        
        [hud hideAnimated:YES];

    }];
    

}

-(void)DrawHeaderWithHeaderArray:(NSArray *)data {
    
    NSMutableArray *headerNames=[NSMutableArray new];
    NSMutableArray *headerData=[NSMutableArray new];
    
    for (Dashboard *headerObj in data) {
        
        
        [headerNames addObject:headerObj.Status];
        [headerData addObject:@(headerObj.Count)];

        }
        
        
    HeaderCollection * collecCell = [HeaderCollection createCollectionWithNames: headerNames :headerData];
    collecCell.center = CGPointMake(self.collectionHeader.frame.size.width  / 2,
                                    self.collectionHeader.frame.size.height / 2);
    
    
    [self.collectionHeader addSubview:collecCell];

}
#pragma mark - Table view data source



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {

    return 400;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //collectionCell
    
    UITableViewCell *mainCell;
    if (indexPath.row ==0)
    {
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"hubPerformance" forIndexPath:indexPath];
        
        CellDashboardView2 *IssueCellVC = [CellDashboardView2 customView];
        
        
        if (toMeDataItems !=NULL) {
            PieChartVIew * pieChartData=[PieChartVIew PMOPieChart:toMeDataItems];
            pieChartData.PieDelegate = self;
            
            NSMutableArray *items=[NSMutableArray new];
            NSMutableArray *legendItems=[NSMutableArray new];

            for (Dashboard *statusObj in toMeDataItems) {
                
                if (statusObj.Count>0) {

                   [items addObject:[PNPieChartDataItem dataItemWithValue:statusObj.Count color:statusObj.colorIs statusID:statusObj.StatusLkpId]];

                }
                
                [legendItems addObject:[legendHelper dataItemWithColor:statusObj.colorIs description:statusObj.statusText count:[NSString stringWithFormat:@"%li",(long)statusObj.Count]]];

            
                }
            

            DynamicLegend * legendViewCus=[DynamicLegend customView];
            
            [legendViewCus createViewWithArrayOfColorsAndNamesOfLegend:legendItems];
            
            legendViewCus.center = CGPointMake(IssueCellVC.viewLegend.frame.size.width  / 2,
                                               IssueCellVC.viewLegend.frame.size.height / 2);
            

            [IssueCellVC.viewLegend addSubview:legendViewCus];
            
            [IssueCellVC.viewPieChartVC addSubview:pieChartData];
           IssueCellVC.headerLabe2.text= NSLocalizedString(@"Hub Performance",nil);

            [cell addSubview:IssueCellVC];
            IssueCellVC.translatesAutoresizingMaskIntoConstraints = NO;
            
            [self changeViewConstraints:IssueCellVC WithCell:cell];
            
            
        }
        
        mainCell = cell;
        
        
        
    }
    else if (indexPath.row ==1)
    {

        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"agencyCell" forIndexPath:indexPath];
        
        AgencyCollectionView *customView = [AgencyCollectionView createCollectionWithData:toMeProgressByAgency ScreenName:@"tasksAgencyFilter"];
        customView.headerLabel.text=NSLocalizedString(@"Agencies Status", nil);
        //        customView.delegate=self;
        [cell addSubview:customView];
        customView.translatesAutoresizingMaskIntoConstraints = NO;
        
        [self changeViewConstraints:customView WithCell:cell];
        
        mainCell = cell;
        
        
        
    }
    else if (indexPath.row ==2)
    {
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"agencyCell" forIndexPath:indexPath];
        
        AgencyCollectionView *customView = [AgencyCollectionView createCollectionWithData:toMeProgressBySources ScreenName:@"tasksAgencyFilter"];
        customView.headerLabel.text=NSLocalizedString(@"Sources Status", nil);
        //        customView.delegate=self;
        [cell addSubview:customView];
        customView.translatesAutoresizingMaskIntoConstraints = NO;
        
        [self changeViewConstraints:customView WithCell:cell];
        
        mainCell = cell;
        
        
        
    }

    
    return mainCell;
}



- (nonnull __kindof UICollectionViewCell *)collectionView:(nonnull UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
    DashboardCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"DashboardCollectionCell" forIndexPath:indexPath];

    Dashboard *dashObject = [toMeStatusData objectAtIndex:indexPath.row];
    cell.cellNumnber.text = [NSString stringWithFormat:@"%ld",(long)dashObject.Count];
    cell.cellName.text =  [NSString stringWithFormat:@"%@",dashObject.Status];
    return cell;
    
}

- (NSInteger)collectionView:(nonnull UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return [toMeStatusData count];
    
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat picDimension =self.collectionHeader.frame.size.height;
    
    return CGSizeMake(picDimension, picDimension-20);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    Dashboard *dashObject = [toMeStatusData objectAtIndex:indexPath.row];
    
    NSInteger statusId =dashObject.StatusLkpId;
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setInteger:statusId forKey:@"statusIDFilter"];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"NavigationController"];
    
    [navigationController setViewControllers:@[[storyboard instantiateViewControllerWithIdentifier:@"TasksHolderViewController"]]];
    
    MainViewController *mainViewController = [storyboard instantiateInitialViewController];
    mainViewController.rootViewController = navigationController;
    
    UIWindow *window = UIApplication.sharedApplication.delegate.window;
    window.rootViewController = mainViewController;
    
    [UIView transitionWithView:window
                      duration:0.3
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:nil
                    completion:nil];
    
    
}

-(void)didClickOnPieChartWithId:(NSInteger)statusId {
    
    //  Dashboard *statusObj =[byMeDataItems objectAtIndex:statusId];
    //  NSInteger pieStatusId =statusId;
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setInteger:statusId forKey:@"tasksPieFilterId"];
    
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"NavigationController"];
    
    [navigationController setViewControllers:@[[storyboard instantiateViewControllerWithIdentifier:@"TasksHolderViewController"]]];
    
    //  [[NetworkOperationManager sharedObjectLogin]setPmoBool:YES];
    MainViewController *mainViewController = [storyboard instantiateInitialViewController];
    mainViewController.rootViewController = navigationController;
    
    UIWindow *window = UIApplication.sharedApplication.delegate.window;
    window.rootViewController = mainViewController;
    
    [UIView transitionWithView:window
                      duration:0.3
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:nil
                    completion:nil];
    
    
}




-(void)userClickedOnPieIndexItem:(NSInteger)pieIndex
{
    PNPieChartDataItem *statusObj =[items objectAtIndex:pieIndex];
  NSInteger statusId =statusObj.statusID;
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setInteger:statusId forKey:@"statusIDFilter"];
    
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"NavigationController"];
    
    [navigationController setViewControllers:@[[storyboard instantiateViewControllerWithIdentifier:@"TasksHolderViewController"]]];
    
  //  [[NetworkOperationManager sharedObjectLogin]setPmoBool:YES];
    MainViewController *mainViewController = [storyboard instantiateInitialViewController];
    mainViewController.rootViewController = navigationController;
    
    UIWindow *window = UIApplication.sharedApplication.delegate.window;
    window.rootViewController = mainViewController;
    
    [UIView transitionWithView:window
                      duration:0.3
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:nil
                    completion:nil];

}

-(void)changeViewConstraints:(UIView*)view WithCell:(UITableViewCell*)cell
{
    
    //Trailing
    NSLayoutConstraint *trailing =[NSLayoutConstraint
                                   constraintWithItem:view
                                   attribute:NSLayoutAttributeTrailing
                                   relatedBy:NSLayoutRelationEqual
                                   toItem:cell
                                   attribute:NSLayoutAttributeTrailing
                                   multiplier:1.0f
                                   constant:0.f];
    
    //Leading
    
    NSLayoutConstraint *leading = [NSLayoutConstraint
                                   constraintWithItem:view
                                   attribute:NSLayoutAttributeLeading
                                   relatedBy:NSLayoutRelationEqual
                                   toItem:cell
                                   attribute:NSLayoutAttributeLeading
                                   multiplier:1.0f
                                   constant:0.f];
    
    //Bottom
    NSLayoutConstraint *bottom =[NSLayoutConstraint
                                 constraintWithItem:view
                                 attribute:NSLayoutAttributeBottom
                                 relatedBy:NSLayoutRelationEqual
                                 toItem:cell
                                 attribute:NSLayoutAttributeBottom
                                 multiplier:1.0f
                                 constant:0.f];
    
    NSLayoutConstraint *top =[NSLayoutConstraint
                              constraintWithItem:view
                              attribute:NSLayoutAttributeTop
                              relatedBy:NSLayoutRelationEqual
                              toItem:cell
                              attribute:NSLayoutAttributeTop
                              multiplier:1.0f
                              constant:0.f];
    
    //Height to be fixed for SubView same as AdHeight
    NSLayoutConstraint *height = [NSLayoutConstraint
                                  constraintWithItem:view
                                  attribute:NSLayoutAttributeHeight
                                  relatedBy:NSLayoutRelationEqual
                                  toItem:nil
                                  attribute:NSLayoutAttributeNotAnAttribute
                                  multiplier:0
                                  constant:650.f];
    
    [cell addConstraint:trailing];
    [cell addConstraint:bottom];
    [cell addConstraint:top];
    [cell addConstraint:leading];
    [cell addConstraint:height];
    
    
}

-(void)changeViewConstraintWithoutHeight:(UIView*)view WithCell:(UIView*)cell
{
    
    //Trailing
    NSLayoutConstraint *trailing =[NSLayoutConstraint
                                   constraintWithItem:view
                                   attribute:NSLayoutAttributeTrailing
                                   relatedBy:NSLayoutRelationEqual
                                   toItem:cell
                                   attribute:NSLayoutAttributeTrailing
                                   multiplier:1.0f
                                   constant:0.f];
    
    //Leading
    
    NSLayoutConstraint *leading = [NSLayoutConstraint
                                   constraintWithItem:view
                                   attribute:NSLayoutAttributeLeading
                                   relatedBy:NSLayoutRelationEqual
                                   toItem:cell
                                   attribute:NSLayoutAttributeLeading
                                   multiplier:1.0f
                                   constant:0.f];
    
    //Bottom
    NSLayoutConstraint *bottom =[NSLayoutConstraint
                                 constraintWithItem:view
                                 attribute:NSLayoutAttributeBottom
                                 relatedBy:NSLayoutRelationEqual
                                 toItem:cell
                                 attribute:NSLayoutAttributeBottom
                                 multiplier:1.0f
                                 constant:0.f];
    
    NSLayoutConstraint *top =[NSLayoutConstraint
                              constraintWithItem:view
                              attribute:NSLayoutAttributeTop
                              relatedBy:NSLayoutRelationEqual
                              toItem:cell
                              attribute:NSLayoutAttributeTop
                              multiplier:1.0f
                              constant:0.f];
    
    
    [cell addConstraint:trailing];
    [cell addConstraint:bottom];
    [cell addConstraint:top];
    [cell addConstraint:leading];
    
    
}



@end
