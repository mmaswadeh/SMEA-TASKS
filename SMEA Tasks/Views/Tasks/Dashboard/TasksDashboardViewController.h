//
//  TasksDashboardViewController.h
//  MCI-Tasks
//
//  Created by Mohammad Maswadeh on 1/29/18.
//  Copyright © 2018 BTIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DashboardCollectionCell.h"
#import "TasksStatusRequest.h"
#import "PNChartDelegate.h"
#import "UIViewController+LGSideMenuController.h"
#import "languageHelper.h"

@protocol userClickDelegate <NSObject>

@optional

- (void)didPressPieChartItem:(NSInteger)StatusLkpId;

@end
@interface TasksDashboardViewController : UIViewController <UICollectionViewDelegate,UICollectionViewDataSource,PNChartDelegate,UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, weak) id<userClickDelegate> delegateItem;
@property (weak, nonatomic) IBOutlet UICollectionView *performanceCollection;
@property (strong, nonatomic) NSArray *cellNames;
@property (weak, nonatomic) IBOutlet UIView *chartView;
@property (weak, nonatomic) IBOutlet UIView *legendView;
@property (strong,nonatomic)  NSMutableArray *filteredChartList;
@property (nonatomic, strong) languageHelper * languageSettings;
@property (weak, nonatomic) IBOutlet UITableView *tasksDashboardTable;
@property (weak, nonatomic) IBOutlet UIView *collectionHeader;


@end
