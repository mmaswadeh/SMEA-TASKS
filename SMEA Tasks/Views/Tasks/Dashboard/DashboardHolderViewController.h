//
//  DashboardHolderViewController.h
//  MCI-Tasks
//
//  Created by Mohammad Maswadeh on 1/29/18.
//  Copyright © 2018 BTIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CAPSPageMenu.h"
#import "UIViewController+LGSideMenuController.h"
#import "languageHelper.h"


@interface DashboardHolderViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIView *layoutView;
@property (nonatomic, strong) languageHelper * languageSettings;

@end
