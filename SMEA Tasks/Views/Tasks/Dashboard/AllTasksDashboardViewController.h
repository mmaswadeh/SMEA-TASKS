//
//  AllTasksDashboardViewController.h
//  MCI-Tasks
//
//  Created by Mohammad Maswadeh on 2/26/18.
//  Copyright © 2018 BTIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DashboardCollectionCell.h"
#import "UIViewController+LGSideMenuController.h"
#import "TasksStatusRequest.h"
#import "PNChartDelegate.h"
#import "languageHelper.h"

@interface AllTasksDashboardViewController : UIViewController <UICollectionViewDelegate,UICollectionViewDataSource,PNChartDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *performanceCollection;
@property (strong, nonatomic) NSArray *cellNames;
@property (weak, nonatomic) IBOutlet UIView *chartView;
@property (weak, nonatomic) IBOutlet UIView *legendView;
@property (nonatomic, strong) languageHelper* languageSettings;
@property (weak, nonatomic) IBOutlet UITableView *tasksDashboardTable;
@property (weak, nonatomic) IBOutlet UIView *collectionHeader;

@end
