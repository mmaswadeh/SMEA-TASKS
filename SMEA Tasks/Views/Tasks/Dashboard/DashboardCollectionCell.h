//
//  DashboardCollectionCell.h
//  MCI-Tasks
//
//  Created by Mohammad Maswadeh on 1/29/18.
//  Copyright © 2018 BTIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DashboardCollectionCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *cellNumnber;
@property (weak, nonatomic) IBOutlet UILabel *cellName;

@end
