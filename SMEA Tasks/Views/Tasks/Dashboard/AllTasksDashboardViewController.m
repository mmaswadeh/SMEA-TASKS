//
//  AllTasksDashboardViewController.m
//  MCI-Tasks
//
//  Created by Mohammad Maswadeh on 2/26/18.
//  Copyright © 2018 BTIT. All rights reserved.
//

#import "AllTasksDashboardViewController.h"
#import "MBProgressHUD.h"
#import "Dashboard.h"
#import "PMOPieChart.h"
#import "legendHelper.h"
#import "DynamicLegend.h"
#import "TasksHolderViewController.h"
#import "TasksViewController.h"
#import "MainViewController.h"
#import "AddTaskViewController.h"
#import "HeaderCollection.h"
#import "CellDashboardView2.h"
#import "PMOPieChart.h"
#import "PieChartVIew.h"
#import "ProgressBarChartCollectionView.h"
#import "AgencyCollectionView.h"


@interface AllTasksDashboardViewController()<UITableViewDelegate,UITableViewDataSource,allEntityCellDelegate,PerformancePieChartDelegate> {
    
    NSArray * TaskStatusArr;
    NSArray * PerformanceStatusArr;
    NSArray * ProgressStatusByAgencyArr;
    
    NSMutableArray *items;
}

@end

@implementation AllTasksDashboardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIBarButtonItem * leftButton= [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu"] style:UIBarButtonItemStylePlain target:self action:@selector(ShowLeftView:)];
    
    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc]
                                    initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                                    target:self action:@selector(AddTask:)];
    
    self.navigationItem.leftBarButtonItem = leftButton;
    
    self.navigationItem.title = NSLocalizedString(@"AllTasksDashboard",nil);
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    [self retriveData];
    
    
    // Do any additional setup after loading the view.
}

-(IBAction)ShowLeftView:(id)sender
{
    
    NSString * language = [[NSString alloc]init];
    
    _languageSettings = [[languageHelper alloc]init];
    language= [_languageSettings checkLanguages];
    if ([language isEqualToString:@"ar"]) {
        
        [self.sideMenuController showRightViewAnimated:YES completionHandler:nil];}
    else{
        [self.sideMenuController showLeftViewAnimated:YES completionHandler:nil];
    }
}

-(IBAction)AddTask:(id)sender
{
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AddTaskViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"AddTaskViewController"];
    
    [self.navigationController pushViewController:controller animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)retriveData {
    
    self.tasksDashboardTable.delegate = self;
    self.tasksDashboardTable.dataSource = self;
    self.performanceCollection.delegate = self;
    self.performanceCollection.dataSource = self;
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSInteger userId = [userDefaults integerForKey:@"UserID"];
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [TasksStatusRequest getAllTasksDashboardWithUserId:userId Success:^(NSMutableArray *taskStatus, NSMutableArray *performanceStatus, NSMutableArray *progressStatusByAgency) {
        
        TaskStatusArr = [NSArray arrayWithArray:taskStatus];
        PerformanceStatusArr = [NSArray arrayWithArray:performanceStatus];
        ProgressStatusByAgencyArr = [NSArray arrayWithArray:progressStatusByAgency];
        [hud hideAnimated:YES];

        [self.performanceCollection reloadData];
        [self.tasksDashboardTable reloadData];
        
    } failure:^(NSError *error) {
        
        [hud hideAnimated:YES];

    }];

    
    
}


#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    int height = 0;
    if (indexPath.row ==0 && indexPath.section == 0 )
    {
        height = 400;
    }
    
    else {
        
        height = 475;
    }
    return height;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    UITableViewCell *mainCell;
    if (indexPath.row ==0 && indexPath.section == 0 )
    {
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"hubPerformance" forIndexPath:indexPath];
        
        CellDashboardView2 *IssueCellVC = [CellDashboardView2 customView];
        
        if (TaskStatusArr !=NULL) {
            PieChartVIew * pieChartData=[PieChartVIew PMOPieChart:TaskStatusArr];
            pieChartData.PieDelegate = self;
            //pieChartData.delegateItem2=self;
            NSMutableArray *items=[NSMutableArray new];
            NSMutableArray *legendItems=[NSMutableArray new];
            
            for (Dashboard *statusObj in TaskStatusArr) {
                
                if (statusObj.Count>0) {
                    
                    [items addObject:[PNPieChartDataItem dataItemWithValue:statusObj.Count color:statusObj.colorIs statusID:statusObj.StatusLkpId]];
                    
                }
                
                [legendItems addObject:[legendHelper dataItemWithColor:statusObj.colorIs description:statusObj.statusText count:[NSString stringWithFormat:@"%li",(long)statusObj.Count]]];
                
                
            }
            
            
            DynamicLegend * legendViewCus=[DynamicLegend customView];
            
            [legendViewCus createViewWithArrayOfColorsAndNamesOfLegend:legendItems];
            
            legendViewCus.center = CGPointMake(IssueCellVC.viewLegend.frame.size.width  / 2,
                                               IssueCellVC.viewLegend.frame.size.height / 2);
            
            
            [IssueCellVC.viewLegend addSubview:legendViewCus];
            
            [IssueCellVC.viewPieChartVC addSubview:pieChartData];
            IssueCellVC.headerLabe2.text= NSLocalizedString(@"Hub Performance",nil);
            
            [cell addSubview:IssueCellVC];
            IssueCellVC.translatesAutoresizingMaskIntoConstraints = NO;
            
            [self changeViewConstraints:IssueCellVC WithCell:cell];
            
            
        }
        
        mainCell = cell;
        
        
        
    }
    else if (indexPath.row ==1 && indexPath.section == 0)
    {
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"agencyCell" forIndexPath:indexPath];
        
        AgencyCollectionView *customView = [AgencyCollectionView createCollectionWithData:ProgressStatusByAgencyArr ScreenName:@"AllTasksScreen"];
        customView.headerLabel.text=NSLocalizedString(@"Agencies Status", nil);
        //        customView.delegate=self;
        [cell addSubview:customView];
        customView.translatesAutoresizingMaskIntoConstraints = NO;
        
        [self changeViewConstraints:customView WithCell:cell];
        
        mainCell = cell;
        
        
    }
    
    
    return mainCell;
}



- (nonnull __kindof UICollectionViewCell *)collectionView:(nonnull UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
    DashboardCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"DashboardCollectionCell" forIndexPath:indexPath];
    
    
    Dashboard *dashObject = [PerformanceStatusArr objectAtIndex:indexPath.row];
    cell.cellNumnber.text = [NSString stringWithFormat:@"%ld",(long)dashObject.Count];
    cell.cellName.text =  [NSString stringWithFormat:@"%@",dashObject.Status];
    return cell;
    
}

- (NSInteger)collectionView:(nonnull UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return [PerformanceStatusArr count];
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
//    Dashboard *dashObject = [PerformanceStatusArr objectAtIndex:indexPath.row];
//
//    NSInteger statusId =dashObject.StatusLkpId;
//
//    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
//    [userDefaults setInteger:statusId forKey:@"statusIDFilter2"];
//
//
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"NavigationController"];
//
//    [navigationController setViewControllers:@[[storyboard instantiateViewControllerWithIdentifier:@"TasksHolderViewController"]]];
//
//    //  [[NetworkOperationManager sharedObjectLogin]setPmoBool:YES];
//    MainViewController *mainViewController = [storyboard instantiateInitialViewController];
//    mainViewController.rootViewController = navigationController;
//
//    UIWindow *window = UIApplication.sharedApplication.delegate.window;
//    window.rootViewController = mainViewController;
//
//    [UIView transitionWithView:window
//                      duration:0.3
//                       options:UIViewAnimationOptionTransitionCrossDissolve
//                    animations:nil
//                    completion:nil];
//
    
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat picDimension =self.collectionHeader.frame.size.height;
    
    return CGSizeMake(picDimension, picDimension-20);
}

-(void)didClickOnPieChartWithId:(NSInteger)statusId {
    
//    //  Dashboard *statusObj =[TaskStatusArr objectAtIndex:statusId];
//    //  NSInteger pieStatusId =statusId;
//    
//    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
//    [userDefaults setInteger:statusId forKey:@"myTasksPieFilterId"];
//    
//    
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"NavigationController"];
//    
//    [navigationController setViewControllers:@[[storyboard instantiateViewControllerWithIdentifier:@"TasksHolderViewController"]]];
//    
//    //  [[NetworkOperationManager sharedObjectLogin]setPmoBool:YES];
//    MainViewController *mainViewController = [storyboard instantiateInitialViewController];
//    mainViewController.rootViewController = navigationController;
//    
//    UIWindow *window = UIApplication.sharedApplication.delegate.window;
//    window.rootViewController = mainViewController;
//    
//    [UIView transitionWithView:window
//                      duration:0.3
//                       options:UIViewAnimationOptionTransitionCrossDissolve
//                    animations:nil
//                    completion:nil];
    
    
}

-(void)userClickedOnPieIndexItem:(NSInteger)pieIndex
{
//    PNPieChartDataItem *statusObj =[TaskStatusArr objectAtIndex:pieIndex];
//    NSInteger statusId =statusObj.statusID;
//
//    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
//    [userDefaults setInteger:statusId forKey:@"statusIDFilter"];
//
//
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"NavigationController"];
//
//    [navigationController setViewControllers:@[[storyboard instantiateViewControllerWithIdentifier:@"TasksHolderViewController"]]];
//
//    //  [[NetworkOperationManager sharedObjectLogin]setPmoBool:YES];
//    MainViewController *mainViewController = [storyboard instantiateInitialViewController];
//    mainViewController.rootViewController = navigationController;
//
//    UIWindow *window = UIApplication.sharedApplication.delegate.window;
//    window.rootViewController = mainViewController;
//
//    [UIView transitionWithView:window
//                      duration:0.3
//                       options:UIViewAnimationOptionTransitionCrossDissolve
//                    animations:nil
//                    completion:nil];
    
}


-(void)changeViewConstraints:(UIView*)view WithCell:(UITableViewCell*)cell
{
    
    //Trailing
    NSLayoutConstraint *trailing =[NSLayoutConstraint
                                   constraintWithItem:view
                                   attribute:NSLayoutAttributeTrailing
                                   relatedBy:NSLayoutRelationEqual
                                   toItem:cell
                                   attribute:NSLayoutAttributeTrailing
                                   multiplier:1.0f
                                   constant:0.f];
    
    //Leading
    
    NSLayoutConstraint *leading = [NSLayoutConstraint
                                   constraintWithItem:view
                                   attribute:NSLayoutAttributeLeading
                                   relatedBy:NSLayoutRelationEqual
                                   toItem:cell
                                   attribute:NSLayoutAttributeLeading
                                   multiplier:1.0f
                                   constant:0.f];
    
    //Bottom
    NSLayoutConstraint *bottom =[NSLayoutConstraint
                                 constraintWithItem:view
                                 attribute:NSLayoutAttributeBottom
                                 relatedBy:NSLayoutRelationEqual
                                 toItem:cell
                                 attribute:NSLayoutAttributeBottom
                                 multiplier:1.0f
                                 constant:0.f];
    
    NSLayoutConstraint *top =[NSLayoutConstraint
                              constraintWithItem:view
                              attribute:NSLayoutAttributeTop
                              relatedBy:NSLayoutRelationEqual
                              toItem:cell
                              attribute:NSLayoutAttributeTop
                              multiplier:1.0f
                              constant:0.f];
    
    //Height to be fixed for SubView same as AdHeight
    NSLayoutConstraint *height = [NSLayoutConstraint
                                  constraintWithItem:view
                                  attribute:NSLayoutAttributeHeight
                                  relatedBy:NSLayoutRelationEqual
                                  toItem:nil
                                  attribute:NSLayoutAttributeNotAnAttribute
                                  multiplier:0
                                  constant:650.f];
    
    [cell addConstraint:trailing];
    [cell addConstraint:bottom];
    [cell addConstraint:top];
    [cell addConstraint:leading];
    [cell addConstraint:height];
    
    
}

-(void)changeViewConstraintWithoutHeight:(UIView*)view WithCell:(UIView*)cell
{
    
    //Trailing
    NSLayoutConstraint *trailing =[NSLayoutConstraint
                                   constraintWithItem:view
                                   attribute:NSLayoutAttributeTrailing
                                   relatedBy:NSLayoutRelationEqual
                                   toItem:cell
                                   attribute:NSLayoutAttributeTrailing
                                   multiplier:1.0f
                                   constant:0.f];
    
    //Leading
    
    NSLayoutConstraint *leading = [NSLayoutConstraint
                                   constraintWithItem:view
                                   attribute:NSLayoutAttributeLeading
                                   relatedBy:NSLayoutRelationEqual
                                   toItem:cell
                                   attribute:NSLayoutAttributeLeading
                                   multiplier:1.0f
                                   constant:0.f];
    
    //Bottom
    NSLayoutConstraint *bottom =[NSLayoutConstraint
                                 constraintWithItem:view
                                 attribute:NSLayoutAttributeBottom
                                 relatedBy:NSLayoutRelationEqual
                                 toItem:cell
                                 attribute:NSLayoutAttributeBottom
                                 multiplier:1.0f
                                 constant:0.f];
    
    NSLayoutConstraint *top =[NSLayoutConstraint
                              constraintWithItem:view
                              attribute:NSLayoutAttributeTop
                              relatedBy:NSLayoutRelationEqual
                              toItem:cell
                              attribute:NSLayoutAttributeTop
                              multiplier:1.0f
                              constant:0.f];
    
    
    [cell addConstraint:trailing];
    [cell addConstraint:bottom];
    [cell addConstraint:top];
    [cell addConstraint:leading];
    
    
}


@end
