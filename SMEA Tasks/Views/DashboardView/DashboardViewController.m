//
//  DashboardViewController.m
//  MCI-Tasks
//
//  Created by Mohammad Maswadeh on 1/13/18.
//  Copyright © 2018 BTIT. All rights reserved.
//

#import "DashboardViewController.h"
#import "PNPieChart.h"
#import "PNChart.h"
#import "PNChartDelegate.h"
#import "TasksStatusRequest.h"
#import "Dashboard.h"

#import "PMOPieChart.h"
#import "legendHelper.h"
#import "DynamicLegend.h"
#import "MBProgressHUD.h"
#import "AddTaskViewController.h"

#import "DashboardCellTableViewCell.h"
@interface DashboardViewController ()
{
    
    
    NSArray * toMeDataItems;
    NSArray * byMeDataItems;

}
@end

@implementation DashboardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
      MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSInteger userId = [userDefaults integerForKey:@"UserID"];
    
 //   [TasksStatusRequest getTasksDashboardWithUserId:userId Success:^(NSMutableArray *toMeChartData, NSMutableArray *byMeChartData) {
    
    [TasksStatusRequest getTasksDashboardWithUserId:userId  Success:^(NSMutableArray *toMeChartData, NSMutableArray *byMeChartData, NSMutableArray *toMeStastus, NSMutableArray *byMeStatus, NSMutableArray *ProgressStatusByAgencyToMe, NSMutableArray *ProgressStatusByAgencyByMe, NSMutableArray *ProgressStatusBySourceByMe, NSMutableArray *ProgressStatusBySourceToMe) {
        
         toMeDataItems=[NSArray arrayWithArray:toMeChartData];
         byMeDataItems=[NSArray arrayWithArray:byMeChartData];

        self.dashboardTable.delegate=self;
        self.dashboardTable.dataSource=self;
        [self.dashboardTable reloadData];
            [hud hideAnimated:YES];
        
        
        
        

        
    } failure:^(NSError *error) {
        [hud hideAnimated:YES];
        NSLog(@"ERROR DASHBOARD %@",error);
    }];
    
    
    self.navigationItem.title =NSLocalizedString(@"Dashboard", nil);
    
    
    UIBarButtonItem * leftButton= [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu"] style:UIBarButtonItemStylePlain target:self action:@selector(ShowLeftView:)];
    
    self.navigationItem.leftBarButtonItem = leftButton;
    
    
    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc]
                                    initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                                    target:self action:@selector(AddTask:)];
    
    
    self.navigationItem.rightBarButtonItem = rightButton;

  
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    // Do any additional setup after loading the view.
}

-(IBAction)AddTask:(id)sender
{
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AddTaskViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"AddTaskViewController"];
    
    [self.navigationController pushViewController:controller animated:YES];
    
    
}

-(IBAction)ShowLeftView:(id)sender
{
    
    NSString * language = [[NSString alloc]init];
    
    _languageSettings = [[languageHelper alloc]init];
    language= [_languageSettings checkLanguages];
    if ([language isEqualToString:@"ar"]) {
        
        [self.sideMenuController showRightViewAnimated:YES completionHandler:nil];}
    else{
        [self.sideMenuController showLeftViewAnimated:YES completionHandler:nil];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    UITableViewCell *mainCell ;

    if (indexPath.row == 0) {
        
        DashboardCellTableViewCell * cell =[tableView dequeueReusableCellWithIdentifier:@"dashboardCell"];
        PMOPieChart * pieChart;
        NSMutableArray *items=[NSMutableArray new];
        NSMutableArray *legendItems=[NSMutableArray new];
        for (Dashboard *statusObj in toMeDataItems) {
            if (statusObj.Count>0) {
                
                [items addObject:[PNPieChartDataItem dataItemWithValue:statusObj.Count color:statusObj.colorIs  statusID:statusObj.StatusLkpId]];
                
                
            }
            
            [legendItems addObject:[legendHelper dataItemWithColor:statusObj.colorIs description:statusObj.Status count:[NSString stringWithFormat:@"%li",(long)statusObj.Count]]];
        }
        pieChart = [[PMOPieChart alloc] initWithFrame:CGRectMake(0 ,0, 250, 250) items:items];
        pieChart.showAbsoluteValues = YES;
        pieChart.showOnlyValues = NO;
        
        
        DynamicLegend * legendViewCus=[DynamicLegend customView];
        
        [legendViewCus createViewWithArrayOfColorsAndNamesOfLegend:legendItems];
        
        legendViewCus.center = CGPointMake(cell.legendView.frame.size.width  / 2,
                                           cell.legendView.frame.size.height / 2);
        
        [cell.legendView addSubview:legendViewCus];
        
        [cell.pieChartView addSubview:pieChart];
        cell.header.text=@"Assigned to me tasks status";
        mainCell = cell;
    }
    
    else {
        
        DashboardCellTableViewCell * cell =[tableView dequeueReusableCellWithIdentifier:@"dashboardCell"];
        PMOPieChart * pieChart;
        NSMutableArray *items=[NSMutableArray new];
        NSMutableArray *legendItems=[NSMutableArray new];
        for (Dashboard *statusObj in byMeDataItems) {
            if (statusObj.Count>0) {
                
                [items addObject:[PNPieChartDataItem dataItemWithValue:statusObj.Count color:statusObj.colorIs statusID:statusObj.StatusLkpId]];
                
                
            }
            [legendItems addObject:[legendHelper dataItemWithColor:statusObj.colorIs description:statusObj.Status count:[NSString stringWithFormat:@"%li",(long)statusObj.Count]]];
        }
        pieChart = [[PMOPieChart alloc] initWithFrame:CGRectMake(0 ,0, 250, 250) items:items];
        pieChart.showAbsoluteValues = YES;
        pieChart.showOnlyValues = NO;
        
        
        DynamicLegend * legendViewCus=[DynamicLegend customView];
        
        [legendViewCus createViewWithArrayOfColorsAndNamesOfLegend:legendItems];
        
        legendViewCus.center = CGPointMake(cell.legendView.frame.size.width  / 2,
                                           cell.legendView.frame.size.height / 2);
        
        [cell.legendView addSubview:legendViewCus];
        
        [cell.pieChartView addSubview:pieChart];
        cell.header.text=@"Assigned by me task status";
        
        mainCell = cell;
    }

    return mainCell;
    
}

@end
