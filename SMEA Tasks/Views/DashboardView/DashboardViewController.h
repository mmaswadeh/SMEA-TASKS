//
//  DashboardViewController.h
//  MCI-Tasks
//
//  Created by Mohammad Maswadeh on 1/13/18.
//  Copyright © 2018 BTIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIViewController+LGSideMenuController.h"
#import "languageHelper.h"

@interface DashboardViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) languageHelper * languageSettings;
@property (weak, nonatomic) IBOutlet UITableView *dashboardTable;

@end
