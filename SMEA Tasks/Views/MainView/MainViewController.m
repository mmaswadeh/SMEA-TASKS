//
//  MainViewController.m
//  PMO
//
//  Created by Blessed Tree IT on 12/22/16.
//  Copyright © 2016 Blessed Tree IT. All rights reserved.
//

#import "MainViewController.h"

@interface MainViewController ()

@property (assign, nonatomic) NSUInteger type;

@end

@implementation MainViewController


-(void)viewDidLoad
{
    LeftMenuViewController *leftViewController;
    leftViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"LeftMenuViewController"];
    UIBlurEffectStyle regularStyle;
    
    if (UIDevice.currentDevice.systemVersion.floatValue >= 10.0) {
        regularStyle = UIBlurEffectStyleRegular;
    }
    else {
       // regularStyle = UIBlurEffectStyleLight;
    }
    
    NSString * language = [[NSString alloc]init];
    
    _languageSettings = [[languageHelper alloc]init];
    language= [_languageSettings checkLanguages];
    if ([language isEqualToString:@"ar"]) {
        self.rightViewPresentationStyle = LGSideMenuPresentationStyleScaleFromBig;
        self.rightViewController = leftViewController;

    }
    else{
    
        self.leftViewPresentationStyle = LGSideMenuPresentationStyleScaleFromBig;
        self.leftViewController = leftViewController;
}
    
}



- (void)rootViewWillLayoutSubviewsWithSize:(CGSize)size {
    [super rootViewWillLayoutSubviewsWithSize:size];
    
    self.rootView.frame = CGRectMake(0.0, 0.0, size.width, size.height);
}

- (void)leftViewWillLayoutSubviewsWithSize:(CGSize)size {
    [super leftViewWillLayoutSubviewsWithSize:size];
    
    if (self.isLeftViewStatusBarHidden) {
        
    
        self.leftView.frame = CGRectMake(0.0, 0.0, size.width, size.height);
    }
    else {
        
      
        self.leftView.frame = CGRectMake(0.0, 20.0, size.width, size.height-20.0);
    }
}

- (void)rightViewWillLayoutSubviewsWithSize:(CGSize)size {
    [super rightViewWillLayoutSubviewsWithSize:size];
    
    if (!self.isRightViewStatusBarHidden ||
        (self.rightViewAlwaysVisibleOptions & LGSideMenuAlwaysVisibleOnPadLandscape &&
         UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad &&
         UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication.statusBarOrientation))) {
            self.rightView.frame = CGRectMake(0.0, 20.0, size.width, size.height-20.0);
        }
    else {
        self.rightView.frame = CGRectMake(0.0, 0.0, size.width, size.height);
    }
}

- (BOOL)isLeftViewStatusBarHidden {
    if (self.type == 8) {
        return UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication.statusBarOrientation) && UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone;
    }
    
    return super.isLeftViewStatusBarHidden;
}

- (BOOL)isRightViewStatusBarHidden {
    if (self.type == 8) {
        return UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication.statusBarOrientation) && UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone;
    }
    
    return super.isRightViewStatusBarHidden;
}

- (void)dealloc {
    NSLog(@"MainViewController deallocated");
}
@end
