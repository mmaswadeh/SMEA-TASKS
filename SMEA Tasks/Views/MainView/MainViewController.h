//
//  MainViewController.h
//  PMO
//
//  Created by Blessed Tree IT on 12/22/16.
//  Copyright © 2016 Blessed Tree IT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LGSideMenuController.h"
#import "LeftMenuViewController.h"
#import "languageHelper.h"

@interface MainViewController : LGSideMenuController
@property (nonatomic, strong) languageHelper * languageSettings;
@end
