//
//  DelaysCell.h
//  MCI-Tasks
//
//  Created by Blessed Tree IT on 1/10/18.
//  Copyright © 2018 BTIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DelaysCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *delayedToLabel;
@property (weak, nonatomic) IBOutlet UILabel *delayDate;
@property (weak, nonatomic) IBOutlet UILabel *resoneLabel;
@property (weak, nonatomic) IBOutlet UILabel *resoneText;

@end
