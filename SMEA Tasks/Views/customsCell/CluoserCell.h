//
//  CluoserCell.h
//  MCI-Tasks
//
//  Created by Blessed Tree IT on 1/16/18.
//  Copyright © 2018 BTIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CluoserCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *cluoserName;
@property (weak, nonatomic) IBOutlet UILabel *cluoserType;

@end
