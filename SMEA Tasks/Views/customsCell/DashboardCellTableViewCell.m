//
//  DashboardCellTableViewCell.m
//  MCI-Tasks
//
//  Created by Blessed Tree IT on 1/14/18.
//  Copyright © 2018 BTIT. All rights reserved.
//

#import "DashboardCellTableViewCell.h"

@implementation DashboardCellTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
