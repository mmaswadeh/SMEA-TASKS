//
//  DashboardCellTableViewCell.h
//  MCI-Tasks
//
//  Created by Blessed Tree IT on 1/14/18.
//  Copyright © 2018 BTIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DashboardCellTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *pieChartView;
@property (weak, nonatomic) IBOutlet UIView *legendView;
@property (weak, nonatomic) IBOutlet UILabel *header;
@end
