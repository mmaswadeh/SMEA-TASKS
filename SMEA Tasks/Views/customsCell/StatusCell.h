//
//  StatusCell.h
//  MCI-Tasks
//
//  Created by Blessed Tree IT on 1/10/18.
//  Copyright © 2018 BTIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StatusCell : UITableViewCell



@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UITextView *descriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *startDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *endDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *assignToLabel;
@property (weak, nonatomic) IBOutlet UILabel *agencyLabel;
@property (weak, nonatomic) IBOutlet UILabel *percentageLabel;

@property (weak, nonatomic) IBOutlet UIView *taskStatusView;
@property (weak, nonatomic) IBOutlet UILabel *sourceName;

@property (weak, nonatomic) IBOutlet UILabel *statusName;
@property (weak, nonatomic) IBOutlet UILabel *meetingNo;
@end
