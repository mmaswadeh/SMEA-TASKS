//
//  DescriptionCell.h
//  MCI-Tasks
//
//  Created by Blessed Tree IT on 1/10/18.
//  Copyright © 2018 BTIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DescriptionCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;

//@property (weak, nonatomic) IBOutlet UITextView *descriptionLabel;

@end
