//
//  AppDelegate.h
//  SMEA Tasks
//
//  Created by Mohammad Maswadeh on 3/6/18.
//  Copyright © 2018 BTIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong,nonatomic)  NSArray *offlineTasks;
@property (nonatomic, strong) NSArray *arrTaskInfo;
@property (nonatomic, assign) NSInteger tasksCount;

@property (nonatomic, assign) NSInteger offlineTaskId;
@property (nonatomic, assign) NSInteger offlineUserId;
@property (nonatomic, strong) NSString *offlineTitle;
@property (nonatomic, strong) NSString *offlineDescription;
@property (nonatomic, strong) NSString *offlineStartingDate;
@property (nonatomic, strong) NSString *offlineEndingDate;
@property (nonatomic, strong) NSString *offlineAssignee;
@property (nonatomic, strong) NSString *offlinePath;

@end

