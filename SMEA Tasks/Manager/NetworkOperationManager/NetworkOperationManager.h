//
//  NetworkOperationManager.h
//  PMO Dashboard
//
//  Created by Blessed Tree IT on 1/30/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFHTTPSessionManager.h"
#import "ErrorHandler.h"


@interface NetworkOperationManager : AFHTTPSessionManager

//@property (nonatomic, assign) BOOL isPmo;
@property (nonatomic, assign) BOOL isPmoDetails;

@property (nonatomic, assign) BOOL pmoBool;
///

+(NetworkOperationManager*)sharedObject;

+(NetworkOperationManager*)sharedObjectLogin;
+(NetworkOperationManager*)sharedObjectEditComment;
+(NetworkOperationManager*)sharedObjectTasks;

-(void)saveAuthenticationToken:(NSString *)authenticationToken;
-(NSString *)authenticationToken;
-(void)clearAuthenticationToken;


@end
