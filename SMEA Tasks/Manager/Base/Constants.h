//
//  Constants.h
//  PMO Dashboard
//
//  Created by Blessed Tree IT on 1/30/17.
//  Copyright © 2017 Blessed Tree IT. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

// Base Url



#define BASEURL      @"http://dev7.cloudapp.net/sema.Taskapi/"
#define TASKSURL     @"http://dev7.cloudapp.net/sema.apitasks/api/tasks/"   //UserName : vroadmin@moh.local Password:P@ssw0rd
#define LOGINBASEURL @"http://dev7.cloudapp.net/sema.Taskapi/"//http://10.153.157.26/EPM.MobileAPI/"


//Tasks

#define GET_TASKS     @"all-swift-tasks"
#define GET_ASSIGNEE  @"assignees-user"
#define ADD_TASK      @"save-swift-task"
#define DELETE_TASK   @"delete-swift-task"
#define GET_AGGENCIES @"agencies"
#define GET_TASKS_DETAILS @"swift-task"
#define GET_TASKS_STATUS @"get-tasks-status"
#define GET_AGENCY_TASK_STATUS @"get-Agencies-tasks-status"
#define SAVE_MY_TASK @"save-my-task"
#define SAVE_TASK_DELAY @"save-task-Delay"
#define UPDATE_TASK_DELAY @"update-task-Delay"
#define MY_SWIFT_TASK @"my-swift-tasks"
#define GET_CLOSURE_TYPES @"closuretype"
#define GET_MY_TASK_STATUS @"get-my-tasks-status"
#define DELETE_TASK_DELAY @"delete-task-delay"
#define DELETE_TASK_IMAGE   @"delete-swift-task-image"
#define GET_TASK_SOURCES @"TaskSources"

//Main Entitiy
#define GET_ENTITY_DETAILS @"api/Entity/GetEntitiesDetails"
#define GET_ENTITY_DETAILS_WITHID @"api/Entity/GetEntityDetails"
#define GET_ALL_ENTITY @"api/Entity/GetAllEntities"

#define GET_ENTITY_DETAILS_PMO @"api/Entity/GetPmoDashboard"
#define GET_ENTITY_DETAILS_WITHID_PMO @"api/Entity/GetEntityDetails"

// Get Initiative Details
#define GET_INITIATIVE_DETAILS @"api/Instances/GetInstanceDetails"

// Get ALL Entities
#define GET_ALL_ENTITIES @"//api/Objective/GetEntities"
#define GET_ALL_ENTITIES_PMO @"GetEntities.json"

// Main Portfolios

#define GET_PORTFOLIOS_DETAILS @"api/MainPortfiols/GetPortifoiliosDetails"
#define GET_PORTFOLIOS_DETAILS_WITHID @"api/MainPortfiols/GetPortifoilioDetails"

#define GET_PORTFOLIO_LIST @"api/MainPortfiols/GetPortifoilios"
#define GET_PORTFOLIOS @"api/MainPortfiols/GetPortifoilios"

// Projects
#define GET_PROJECT_LIST_FOR_PMO @"api/Project/GetNonNTPProjects"
#define GET_PROJECTS_LIST @"api/Project/GetNTPProjects"
#define GET_PROJECT_DETAILS_WITHID @"api/Project/GetProjectDetails"//project_details.json" //
#define GET_MILESTONE_LIST @"api/Project/GetProjectMileStones"//Projects/Milestone"
#define GET_STAKEHOLDER_LIST @"api/Projects/Stakeholders"
#define POST_REQUEST_MEETING @"api/Projects/RequestMeeting"
#define POST_FEELING_FACE @"api/Projects/ChangeFeeling"


// Risks

#define GET_RISKS_LIST @"api/Risk/GetAllNTPRisks"//GetRisksList"//
#define GET_PMO_RISKS  @"api/Risk/GetAllNonNTPRisks"
// Issue

#define GET_ISSUES_LIST @"api/Issues/GetAllNTPIssues" //GetIsseusList"//
#define GET_PMO_ISSUES  @"api/Issues/GetAllNonNTPIssues"
#define GET_ALL_OBJECTIVES @"api/Object/GetObjectives"

// Comments


#define POST_ADD_COMMENT @"api/Comment/AddComment"
#define POST_VOICE_COMMENT @"api/Comment/AddSoundComment"
#define GET_COMMENTS @"api/Comment/GetComments"
#define UPDATE_COMMENTS @"api/Comment/UpdateComment"

// Login Request

#define POST_LOGIN_USER @"token"

#define DELETE_COMMENT @"api/Comment/DeleteComment"


#define GET_THEME_VIEWS @"api/plan/GetOverviewThemes"//overview_themes.json" //
#define GET_THEME_DETAILS @"api/Object/GetThemeDetails"//theme_details.json"//

#define GET_INSTANCE_DETAILS @"api/Initiative/GetInitiativeDetails"//instance_details.json" //
#define GET_PORTFOLIO_DETAILS @"api/Portfolio/GetPortfolioDetails"//portfolio_details.json"//
#define GET_PROGRAM_DETAILS @"api/Project/GetProgramDetails"//program_details.json"//

#define GET_OBJECTIVE_DETAILS @"api/Object/GetObjectiveDetails"//objective_details.json"//



#define GET_OVER_VIEW_DASHBOARD @"api/Plan/GetActivePlan" //overview_dashboard.json"//
#define GET_OVERVIEW_MATRIX @"api/plan/GetStrategicMatrix"//strategicMatrix.json"//

#define GET_BASIC_INFO @"api/Dashboard/GetBasicInformation"
#define GET_KPI_DETAILS @"api/KPI/GetKPIDetails"//kpi_details.json"//

#define  GET_ALL_INITIATIVES @"api/Initiative/GetAllInitiatives"
#define  GET_ALL_OBJECTIVES_KPIS @"api/KPI/GetAllObjectsKPIs"

//NSInteger const  COMMENT_None = 0;
//NSInteger const  COMMENT_Unit = 1;
//NSInteger const  COMMENT_Portfolio = 2;
//NSInteger const  COMMENT_Project = 3;
//NSInteger const  COMMENT_Issue = 4;
//NSInteger const  COMMENT_Risk = 5;
//NSInteger const  COMMENT_Task = 6;
//NSInteger const  COMMENT_Escalation = 7;
//NSInteger const  COMMENT_AllUnits = 8;
//NSInteger const  COMMENT_Summary = 9;
//NSInteger const  COMMENT_SupportNeeded = 10;
//NSInteger const  COMMENT_Plan = 11;
//NSInteger const  COMMENT_Tracking = 12;
//NSInteger const  COMMENT_Object = 13;
//NSInteger const  COMMENT_Initiative = 14;
//NSInteger const  COMMENT_KPI = 15;
//NSInteger const  COMMENT_StrategyPortfolio = 16;
//NSInteger const  COMMENT_StrategyProject = 17;
//NSInteger const  COMMENT_Entity = 18;


#define  COMMENT_None  0
#define  COMMENT_Unit  1
#define  COMMENT_Portfolio  2
#define  COMMENT_Project  3
#define  COMMENT_Issue 4
#define  COMMENT_Risk  5
#define  COMMENT_Task  6
#define  COMMENT_Escalation  7
#define  COMMENT_AllUnits  8
#define  COMMENT_Summary  9
#define  COMMENT_SupportNeeded  10
#define  COMMENT_Plan  11
#define  COMMENT_Tracking  12
#define  COMMENT_Object  13
#define  COMMENT_Initiative  14
#define  COMMENT_KPI  15
#define  COMMENT_StrategyPortfolio  16
#define  COMMENT_StrategyProject  17
#define  COMMENT_Entity  18


#define  OBJECTIVES_ITEM          0
#define  KPIS_ITEM                1
#define  PROJECTS_ITEM            2
#define  INITIATIVES_ITEM         3
#define  PROGRESS_ITEM            4

#define PLAN_TYPE 11
#define TRACKING_TYPE 12
#define OBJECT_TYPE 13
#define INITIATIVE_TYPE 14
#define KPI_TYPE 15
#define STRATEGYPORTFOLIO_TYPE 16
#define STRATEGYPROJECT_TYPE 17
#define ENTITY_TYPE 18

#define PMO_TYPE 1
#define VRO_TYPE 12
#define PROJECT_TYPE 3

#endif /* Constants_h */
